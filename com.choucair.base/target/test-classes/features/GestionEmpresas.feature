#Author: jacevedor@choucairtesting.com


@tag
Feature: Gestión de empresas

  @RegistroEmpresaExitoso
  Scenario: Registrar Empresa
    Given Abrir Navegador
		And Autenticar y verificar
		|Correo																				|password	|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$|
		And Ingreso opcion nueva Empresa
		And Ingreso datos de la nueva empresa
		|RUC					|RazonSocial										|DIRECCION																		|DEPARTAMENTO	|PROVINCIA|DISTRITO|GRUPO_ECONOMICO	|BROKER															|CONTACTO							|CORREO_CONTACTO							|TELEFONO_CONTACTO|CONTACTO_BROKER										|CORREO_BROKER				|TELEFONO_BROKER|PROCEDENCIA|Eps			|	
		|123412342136	|Choucair testing nueva empresa	|Paseo de la República 3211, San Isidro 15047 |Lima					|Lima			|San Luis|SIN GRUPO				|JLT PERU CORREDORES DE SEGUROS S.A.|Jhon Edinson Acevedo	|jacevedor@choucairtesting.com|3154439086				|JLT PERU CORREDORES DE SEGUROS S.A	|jeacevedo92@gmail.com|3154439086			|EPS				|Pacifico	|
    Then Verifico Registro Exitoso
    
   @Fun006
	Scenario: Registrar empresa con campos obligatorios incompletos.
    Given Abrir Navegador
		And Autenticar y verificar
		|Correo																				|password	|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$|
		And Ingreso opcion nueva Empresa
		And Ingreso datos incompletos de la nueva empresa
		|RUC					|RazonSocial										|	
		|123412342134	|Choucair testing nueva empresa	|
		Then Verifico campos incompletos  
    
  @Fun007
	Scenario: Registrar empresa con RUC/Razón Social existente.
    Given Abrir Navegador
		And Autenticar y verificar
		|Correo																				|password	|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$|
		And Ingreso opcion nueva Empresa
		And Ingreso datos de la nueva empresa
		|RUC					|RazonSocial										|DIRECCION																		|DEPARTAMENTO	|PROVINCIA|DISTRITO|GRUPO_ECONOMICO	|BROKER															|CONTACTO							|CORREO_CONTACTO							|TELEFONO_CONTACTO|CONTACTO_BROKER										|CORREO_BROKER				|TELEFONO_BROKER|PROCEDENCIA|Eps			|	
		|123412342134	|Choucair testing nueva empresa	|Paseo de la República 3211, San Isidro 15047 |Lima					|Lima			|San Luis|SIN GRUPO				|JLT PERU CORREDORES DE SEGUROS S.A.|Jhon Edinson Acevedo	|jacevedor@choucairtesting.com|3154439086				|JLT PERU CORREDORES DE SEGUROS S.A	|jeacevedo92@gmail.com|3154439086			|EPS				|Pacifico	|
		Then Verifico Empresa Existente
		
		
		
		
		
		
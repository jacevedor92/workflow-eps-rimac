package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://rimacsegurosperu.sharepoint.com/sites/WorkflowSalud")
public class LoginPage extends PageObject {
	
	@FindBy(xpath = "//div[@class='placeholderContainer']/input")
	public WebElementFacade txtUserName;
	
	@FindBy(id = "idSIButton9")
	public WebElementFacade BtnContinuar;
	
	@FindBy(xpath = "//div[@class='placeholderContainer']/input")
	public WebElementFacade txtPassword;
	
//	@FindBy(id = "idSIButton9")
//	public WebElementFacade btnIniciarSesion;
//		
//	@FindBy(id = "idSIButton9")
//	public WebElementFacade btnSesionActiva;
	
	@FindBy(id = "KmsiCheckboxField")
	public WebElementFacade ckbManterActiva;
	
	

	
	public void UserName(String DatoPrueba) {		
		txtUserName.sendKeys(DatoPrueba);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void Continuar() {		
		BtnContinuar.click();
	}
	
	public void Password(String DatoPrueba) {
		  try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		txtPassword.sendKeys(DatoPrueba);
	}
	
	public void IniciarSesion() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		BtnContinuar.submit();
	}
	
	public void SesionActiva() {
		//ckbManterActiva.click();
		if (getDriver().findElements(By.id("idSIButton9")).size()!=0) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			BtnContinuar.submit();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
		
	}
	
}
	

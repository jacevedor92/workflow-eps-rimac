package com.choucair.formacion.pageobjects;


import java.util.List;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

//@DefaultUrl("")
public class TramitesBusquedaPage extends PageObject {
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div/div[1]/div[1]/div/button")
	public WebElementFacade btnIniciarTramite;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div/div[1]/div[1]/div/ul/li[1]/a")
	public WebElementFacade optVentaNueva;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div/div[1]/div[1]/div/ul/li[1]/a")
	public WebElementFacade optRenovacion;
	
	@FindBy(xpath = "//*[@id=\"headingOne\"]/h4/a")
	public WebElementFacade slctBuscador;
	
	@FindBy(xpath = "//*[@id='collapseOne']/div/div/div/div[1]/div/input")
	public WebElementFacade txtNumeroTramite;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[2]/div/input")
	public WebElementFacade txtRucCliente;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[3]/div/div/button")
	public WebElementFacade txtRazonSocialCliente;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[4]/div/div/button")
	public WebElementFacade cmbBroker;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[5]/div/div/button")
	public WebElementFacade cmbGrupoEconomico;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[6]/div/div/button")
	public WebElementFacade cmbEstado;
	
	@FindBy(xpath = "//*[@id=\"datetimepicker1\"]/div/div[1]/div/input")
	public WebElementFacade txtFechaDesde;
	
	@FindBy(xpath = "//*[@id=\"datetimepicker2\"]/div/div/div/input")
	public WebElementFacade txtFechaHasta;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[6]/div/div/button")
	public WebElementFacade cmbPrioridad;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[6]/div/div/button")
	public WebElementFacade cmbTipoTramite;
	
	@FindBy(xpath = "//*[@id=\"datetimepicker2\"]/div/div/div/input")
	public WebElementFacade btnBuscar;
	
	@FindBy(xpath = "//*[@id=\"datetimepicker2\"]/div/div/div/input")
	public WebElementFacade btnLimpiar;
	
	
	//tramite buscado el ojito
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div/div[4]/div/div/div/div[1]/div/a[1]/img")
	public WebElementFacade btnTramitePendiente;
	
	public void VentaNueva() {
		optVentaNueva.click();
	}
	
	public void Renovacion() {
		optRenovacion.click();
	}
	
	public void Buscador() {
		slctBuscador.click();
	}
	
	//numero tramite
	public void NumeroTramite(String text) {
		txtNumeroTramite.sendKeys(text);
	}
	
	//ruc cliente
	public void RucCliente(String text) {
		txtRucCliente.sendKeys(text);
	}
	
	//razon social
	public void RazonSocialCliente(String text) {
		txtRazonSocialCliente.sendKeys(text);
	}
	
	//broker
	public void Broker(String text) {
		cmbBroker.selectByVisibleText(text);
	}
	
	//tipo entidad predecesora
	public void TipoEntidadPredecesora(String text) {
		cmbGrupoEconomico.selectByVisibleText(text);
	}
	//Categoria
	public void Categoria(String text) {
		cmbEstado.selectByVisibleText(text);
	}
		
	//FechaActualizacionDesde
	public void FechaActualizacionDesde(String text) {
		cmbGrupoEconomico.selectByVisibleText(text);
	}
		
	//Fecha hasta actualizacion 
	public void FechaActualizacionHasta(String text) {
		cmbGrupoEconomico.selectByVisibleText(text);
	}

	//Fecha hasta actualizacion 
	public void Buscar() {
		btnBuscar.click();
	}
	
	public void Limpiar() {
		cmbGrupoEconomico.click();
	}
	
	public void SelectTramite() {
		btnTramitePendiente.click();
	}
	
	public void SeleccionarTramite(String NumTramite) {
		this.Buscador();
		this.NumeroTramite(NumTramite);
		this.Buscar();
		this.SelectTramite();		
	}
	
	
	
}
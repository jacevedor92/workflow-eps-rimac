package com.choucair.formacion.pageobjects;

/**
 * Estructura básica de clase para Object RnGestionarClientesRenovarPage
 *
 */

import java.util.List;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("")
public class RnGestionarClientesRenovarPage extends PageObject {
	
	@FindBy(xpath = "//*[@id=\"mytable\"]/tbody/tr/td[5]/a/img")
	public WebElementFacade btnContacto;
	
	@FindBy(xpath = "//*[@id=\"mytable\"]/tbody/tr/td[9]/div/label/input")
	public WebElementFacade ckbRenovar;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[8]/div[3]/div/button[2]")
	public WebElementFacade btnIniciarProceso;	
	
	@FindBy(xpath = "//*[@id=\"modalMensajeConfirmacion\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnConfirmacion;
	
	@FindBy(id = "mensajeExito")
	public WebElementFacade lblConfirmacion;
	
	@FindBy(xpath = "//*[@id=\"modalMensajeExito\"]/div/div/div[2]/div[3]/button")
	public WebElementFacade btnAceptar;
	
	
	public void Contacto(String data) {
		btnContacto.click();
	}
	
	public void Renovacion() {
		ckbRenovar.click();
	}
	
	public void IniciarProceso() {
		btnIniciarProceso.click();
	}
	
	public void Confirmacion() {
		btnConfirmacion.click();
	}
	
	public void Aceptar() {
		btnConfirmacion.click();
	}

	public void validacionGestionClientes() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mensajeExito")));
		
		String strMensaje = lblConfirmacion.getText();
				
		assertThat(strMensaje, containsString("Se ha enviado la carta de NO RENOVACIÓN y puede proceder con la generación de la propuesta"));
		
		btnAceptar.click();		
		
	}
	
	
}
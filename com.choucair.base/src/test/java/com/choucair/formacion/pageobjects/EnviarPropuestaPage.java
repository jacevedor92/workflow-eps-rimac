package com.choucair.formacion.pageobjects;

/**
 * Estructura básica de clase para Object EnviarPropuestaPage
 *
 */

import java.util.List;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("")
public class EnviarPropuestaPage extends PageObject {
	
	
	@FindBy(xpath = "//*[@id=\"step-1\"]/div[5]/div/button")
	public WebElementFacade btnEnviarPropuesta;
	
	@FindBy(xpath = "//*[@id=\"datetimepicker1\"]/div/div/div/input")
	public WebElementFacade txtFechaDespacho;
	
	
	@FindBy(xpath = "//*[@id=\"datetimepicker1\"]/div/div[2]/div/div[2]/div[2]/div[5]/div[7]")
	public WebElementFacade txtFechaDia;
	
	@FindBy(xpath = "//*[@id=\"datetimepicker1\"]/div/div[2]/div/div[3]/div[2]/div/ul/li[87]")
	public WebElementFacade txtFechaHora;	
	
	@FindBy(xpath = "//*[@id=\"chbox0\"]")
	public WebElementFacade cknNoEnviar;
	
	@FindBy(id = "cmbEmpresa")
	public WebElementFacade cmbEmpresa;
	
	@FindBy(xpath = "//*[@id=\"modalPopupDespacho\"]/div/div/div[2]/div[1]/div/div[2]/div[7]/div[1]/div[5]/div/input")
	public WebElementFacade txtDireccion;
	
	@FindBy(xpath = "//*[@id=\"modalPopupDespacho\"]/div/div/div[2]/div[1]/div/div[2]/div[7]/div/div[5]/div/input")
	public WebElementFacade txtContacto;
	
	@FindBy(xpath = "//*[@id=\"modalPopupDespacho\"]/div/div/div[2]/div[1]/div/div[2]/div[5]/div/button")
	public WebElementFacade btnDireccionDespacho;
	
	@FindBy(xpath = "//*[@id=\"modalPopupDespacho\"]/div/div/div[2]/div[1]/div/div[2]/div[7]/div/div[6]/div/input")
	public WebElementFacade txtEmailContacto;
		
	@FindBy(xpath = "//*[@id=\"modalPopupDespacho\"]/div/div/div[2]/div[1]/div/div[2]/div[7]/div/div[7]/div/input")
	public WebElementFacade txtTelefonoContacto;
	
	@FindBy(xpath = "//*[@id=\"modalPopupDespacho\"]/div/div/div[2]/div[2]/button[2]")
	public WebElementFacade btnEnviarPropuestFormal;
	
	@FindBy(xpath = "//*[@id=\"modalMensajeConfirmacionTipo\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnConfirmacionEnvioPropuesta;//5 seg espera
	
	
	@FindBy(xpath = "//*[@id=\"modalMensajeExito\"]/div/div/div[2]/div[3]/button")
	public WebElementFacade btnAceptar;//5 seg espera
	
	
	@FindBy(xpath = "//*[@id=\"step-1\"]/div[2]/div/button[2]")
	public WebElementFacade btnEnviarPropuestFormalRenovacion;//5 seg espera
	
	
	//mensajes de confirmacion para validacion
	//*[@id="mensajeConfirmacionTipo"]
	@FindBy(id = "mensajeConfirmacionTipo")
	public WebElementFacade Lblconfirmacion;
	//Se enviará el trámite a configurar los planes
	
	
	@FindBy(id = "mensajeExito")
	public WebElementFacade LblconfirmacionExito;
	//Se ha iniciado el concurso regular.
	//Se ha enviado la propuesta formal y se espera el ingreso del resultado del concurso.
	//Se requiere de información y documentación adicional para proceder con la implementación.
	//Se ha enviado el trámite al área de configuración de planes
	
	
	//metodo por mejorar
	//fecha y hora quemada
	public void FechaDespacho(String data) {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalPopupDespacho")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		txtFechaDespacho.click();
		btnEnviarPropuestFormal.sendKeys("");
		
		txtFechaDespacho.click();
		txtFechaDia.click();		
		txtFechaDespacho.clear();
		txtFechaDespacho.sendKeys(data);
		
	}
	
	public void Contacto(String data) {
				
		txtEmailContacto.click();
		txtContacto.click();
		txtContacto.sendKeys(data);
	}
	
	public void Email(String data) {
		txtEmailContacto.click();
		txtEmailContacto.sendKeys(data);
	}
	
	public void TelefonoContacto(String data) {
		txtTelefonoContacto.click();
		txtTelefonoContacto.sendKeys(data);
	}
	
	public void Enviar() {
		btnEnviarPropuestFormal.click();
	}
	
	public void Confirmar() {
		btnConfirmacionEnvioPropuesta.click();
	}

	public void Aceptar() {
		btnAceptar.click();
		
	}

	public void EnviarPropuesta() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"step-1\"]/div[5]/div/button")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		btnEnviarPropuesta.click();
	}
	
	public void EnviarPropuestaRenovacion() {
	
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"step-1\"]/div[2]/div/button[2]")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		btnEnviarPropuestFormalRenovacion.click();
	}

	public void validacion() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalMensajeConfirmacionTipo")));
		
		Confirmar();
				
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalMensajeExito")));
		
		String strMensaje = LblconfirmacionExito.getText();
		assertThat(strMensaje, containsString("Se ha enviado la propuesta formal y se espera el ingreso del resultado del concurso"));
		
		Aceptar();
		
	}
	
}
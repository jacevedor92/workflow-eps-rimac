package com.choucair.formacion.pageobjects;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

import java.sql.Driver;
import java.util.List;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;

/**
 * Estructura básica de clase para Object RegistrarTramitePage
 *
 */


import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


@DefaultUrl("https://rimacsegurosperu.sharepoint.com/sites/WorkflowSalud/SitePages/AppRimac.aspx/Tramite/NuevoTramite")

public class RegistrarTramitePage extends PageObject {
	
	//opcion iniciar tramite nueva venta
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div/div[1]/div[1]/div/button")
	public WebElementFacade btnIniciarTramite;
	
	@FindBy(xpath = "//*[@id='s4-bodyContainer']/div[2]/div/div/div[1]/div/div[1]/div[1]/div/ul/li[1]/a")
	public WebElementFacade optVentaNueva;
	
	@FindBy(xpath = "//button[@data-id='cmbTipoTramite']")
	public WebElementFacade cmbTipoTramite;
	
	
	//Datos Empresa =======================================================
	
	@FindBy(xpath = "//*[@id=\"headingOne\"]/h4/a")
	public WebElementFacade cmbDatosEmpresa;	
	
	@FindBy(name = "RUC")
	public WebElementFacade txtRuc;
	
	@FindBy(name = "RazonSocial")
	public WebElementFacade txtRazonSocial;	
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[3]/div/div/span/a/img")
	public WebElementFacade imgDireccion;
	
	@FindBy(name = "DireccionTemp")
	public WebElementFacade txtDireccion;
	
	@FindBy(xpath = "//button[@data-id='cmbDepartamento']")
	public WebElementFacade cmbDepartamento;
	
	@FindBy(xpath = "//button[@data-id='cmbProvincia']")
	public WebElementFacade cmbProvincia;
	
	@FindBy(xpath = "//button[@data-id='cmbDistrito']")
	public WebElementFacade cmbDistrito;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnAceptarDireccion;
	
	@FindBy(xpath = "//button[@data-id='cmbGrupoEconomico']")
	public WebElementFacade cmbGrupoEconomico;
	
	@FindBy(xpath = "//button[@data-id='cmbBroker']")
	public WebElementFacade cmbBroker;
	
	
	//Datos de contacto =============================
	
	@FindBy(xpath = "//*[@id=\"headingTwo\"]/h4/a[text()='Datos de Contacto']")
	public WebElementFacade cmbDatosContacto;	
	
	@FindBy(name = "EmpresaPersonaContacto")
	public WebElementFacade txtPersonaContacto;
	
	@FindBy(name = "EmpresaPersonaCorreo")
	public WebElementFacade txtCorreoContacto;
	
	@FindBy(name = "EmpresaPersonaTelefono")
	public WebElementFacade txtTelefonoContacto;
	
	
	//Datos de Broker =============================
			
	@FindBy(xpath = "//*[@id=\"collapseTwo\"]/div/div/div/div[4]/div/div[2]/input")
	public WebElementFacade txtBroker;
	
	@FindBy(name = "BrokerPersonaCorreo")
	public WebElementFacade txtCorreoBroker;
	
	@FindBy(name = "BrokerPersonaTelefono")
	public WebElementFacade txtTelefonoBroker;
	
	///////procedencia ========================================
	
	@FindBy(xpath = "//*[@id=\"headingTwo\"]/h4/a[text()='Procedencia']")
	public WebElementFacade cmbProcedencia;
	
	@FindBy(xpath = "//button[@data-id='cmbTipoEntidadPredecesora']")
	public WebElementFacade cmbTipoEntidadPredecesora;
	
	@FindBy(xpath = "//button[@data-id='cmbEPS']")
	public WebElementFacade cmbEps;
	
	//guardar
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[1]/div[1]/button[2]")
	public WebElementFacade btnGuardar;
	
	//Confirmacion
	@FindBy(xpath = "//*[@id=\"modalMensajeConfirmacion\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnConfirmacionTramite;
	
	//label de confirmacion
	@FindBy(id = "mensajeExito")
	public WebElementFacade lblConfirmacion;
	
	//label de confirmacion
	@FindBy(xpath = "//*[@id=\"modalMensajeExito\"]/div/div/div[2]/div[3]/button")
	public WebElementFacade btnAceptarTramite;
	
	
	
	public void IniciarTramite() {
		//btnIniciarTramite.sendKeys("");
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='s4-bodyContainer']/div[2]/div/div/div[1]/div/div[1]/div[1]/div/button")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		btnIniciarTramite.click();
	}
	
	public void NuevaVenta() {
		optVentaNueva.click();
	}
	
	public void TipoTramite(String data) {
		
		cmbTipoTramite.click();
		String Elementxpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+data+"']";
		
		WebElement elemento = getDriver().findElement(By.xpath(Elementxpath));	
		
		elemento.click();		
		
	}
	
	public void DatosEmpresa() {
		cmbDatosEmpresa.click();
	}
	
	public void Ruc(String data) {
		txtRuc.sendKeys(data);
	}
	
	public void RazonSocial(String data) {	
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		txtRazonSocial.sendKeys(data);
	}
	
	public void Direccion(String dir, String dep, String prov,String dis) {
		imgDireccion.click();
		
		txtDireccion.sendKeys(dir);
		
		String xpathDep =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+dep+"']";
		String xpathProv =  "//div[@class='modal-body']/div/div[3]/div/div/div/ul/li/a/span[text()='"+prov+"']";
		String xpathdis =  "//div[@class='modal-body']/div/div[4]/div/div/div/ul/li/a/span[text()='"+dis+"']";
		
		cmbDepartamento.click();
		getDriver().findElement(By.xpath(xpathDep)).click();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		cmbProvincia.click();
		getDriver().findElement(By.xpath(xpathProv)).click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		cmbDistrito.click();
		getDriver().findElement(By.xpath(xpathdis)).click();				
		
		btnAceptarDireccion.click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void GrupoEconomico(String data) {
		cmbGrupoEconomico.click();
		String xpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+data+"']";
		getDriver().findElement(By.xpath(xpath)).click();			
			
	}
	
	public void Broker(String data) {
		cmbBroker.click();
		String xpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+data+"']";
		getDriver().findElement(By.xpath(xpath)).click();		
	}
	
	public void DatosContacto(String Contacto,String correo,String telefono) {
		cmbDatosContacto.click();
		txtPersonaContacto.sendKeys(Contacto);
		txtCorreoContacto.sendKeys(correo);
		txtTelefonoContacto.sendKeys(telefono);
			
	}
	
	public void DatosBroker(String contactoBroker, String correoBroker, String telefonoBroker) {
		txtBroker.sendKeys(contactoBroker);
		txtCorreoBroker.sendKeys(correoBroker);
		txtTelefonoBroker.sendKeys(telefonoBroker);
		
	}
	
	public void Procedencia(String TipoEntidad,String Opt2) {
		cmbProcedencia.click();
		cmbTipoEntidadPredecesora.click();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
		String xpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+TipoEntidad+"']";
		getDriver().findElement(By.xpath(xpath)).click();	
		
		if (TipoEntidad!="ESSALUD") {
			cmbEps.click();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			String xpatheps =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+Opt2+"']";
			getDriver().findElement(By.xpath(xpatheps)).click();	
			
		}
		
			
	}
	
	public void Guardar() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[1]/div[1]/button[2]")));
		wait.until(ExpectedConditions.attributeToBeNotEmpty(txtRazonSocial, "value"));
		
		btnGuardar.sendKeys("");
		btnGuardar.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalMensajeConfirmacion")));
		btnConfirmacionTramite.click();
		
			
	}

	public void Verificacion() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mensajeExito")));
		
		String strMensaje = lblConfirmacion.getText();
		
		String CodTramite = strMensaje.substring(strMensaje.length() - 5); 
				
		String destination="";
		Serenity.setSessionVariable("codTramite").to(CodTramite);		
		
		assertThat(strMensaje, containsString("Se ha registrado el trámite con código"));
			
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalMensajeExito")));
		
		btnAceptarTramite.click();
		
		
	}

	
	
	
	
	
	
}
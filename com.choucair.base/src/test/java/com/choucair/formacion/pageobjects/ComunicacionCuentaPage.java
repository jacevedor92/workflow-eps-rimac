package com.choucair.formacion.pageobjects;

import java.io.File;
import java.io.IOException;

/**
 * Estructura básica de clase para Object ComunicacionCuentaPage
 *
 */

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("")
public class ComunicacionCuentaPage extends PageObject {
	
	
	//=======================datos de poliza ================================
	@FindBy(xpath = "//*[@id=\"datetimepicker1\"]/div/div/div/input")
	public WebElementFacade txtFechaVigencia;
	
	@FindBy(id = "comisionPoliza")
	public WebElementFacade txtComisionPoliza;
	
	@FindBy(xpath = "//button[@data-id='cmbMetodoFacturacion']")
	public WebElementFacade cmbMetodoFacturacion;
	
	@FindBy(xpath = "//*[@id='collapseTen']/div/div/div/div[6]/label[2]")
	public WebElementFacade ckbPlanesPotestativosSi;
	
	@FindBy(xpath = "//*[@id='collapseTen']/div/div/div/div[6]/label[3]")
	public WebElementFacade ckbPlanesPotestativosNo;
	
	@FindBy(xpath = "//*[@id=\"collapseCero\"]/div/div/div/div[6]/label[2]")
	public WebElementFacade ckbPlanesPotestativosRenovacionSi;
	
	@FindBy(xpath = "//*[@id=\"collapseCero\"]/div/div/div/div[6]/label[3]")
	public WebElementFacade ckbPlanesPotestativosRenovacionNo;
	
	@FindBy(xpath = "//button[@data-id='cmbRevision']")
	public WebElementFacade cmbRevision;
	
	
	//======================= Reaseguros ================================
	
	//=======================Periodos de carencia ================================
	
	//=======================Condiciones y beneficios adicionales================================
	
	@FindBy(xpath = "//*[@id=\"tinymce\"]/p")
	public WebElementFacade txtCondicionesBeneficios;
	
	
	//renovacion
	@FindBy(id = "ajuste")
	public WebElementFacade txtAjusteRenovacion;

	
	//=======================información de despacho de packs================================
	//=======================Información adicional================================
	
	@FindBy(id = "rutaPlanes")
	public WebElementFacade txtRutaPlanes;	
	
	@FindBy(id = "my-file-selector")
	public WebElementFacade txtAgregarArchivo;
	
	@FindBy(id = "//div[@id='step-1']/div[7]/div/button[2]")
	public WebElementFacade btnEnviarComunicado;
	
	@FindBy(id = "//*[@id=\"modalEnviarComunicado\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnAceptarEnviarComunicado;
	
	@FindBy(id = "//*[@id=\"modalMensajeExito\"]/div/div/div[2]/div[3]/button")
	public WebElementFacade btnAceptarComunicadoEnviado;
	
	
	@FindBy(id = "mensajeExito")
	public WebElementFacade LblconfirmacionExito;
	
	// metodos =====
	public void FechaVigencia(String data) {
		
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		txtFechaVigencia.click();
		
		txtFechaVigencia.clear();
		txtFechaVigencia.sendKeys(data);				
		
	}
	
	public void ComisionPoliza(String data) {
	
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("comisionPoliza")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		String text = txtComisionPoliza.getValue();
		
		if (text.isEmpty()) {
			txtComisionPoliza.sendKeys(data);
		}
		
	}

	public void MetodoFacturacion(String data) {
		cmbMetodoFacturacion.click();
		String xpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+data+"']";
		getDriver().findElement(By.xpath(xpath)).click();		
	}
	
	public void PlanesPotestativos(String data) {
		String answer = data.toLowerCase().trim();
		if (answer.equals("si")) {
			ckbPlanesPotestativosSi.click();
		}else {
			ckbPlanesPotestativosNo.click();
		}		
	}
	
	public void PlanesPotestativosRenovacion(String data) {
		String answer = data.toLowerCase().trim();
		if (answer.equals("si")) {
			ckbPlanesPotestativosRenovacionSi.click();
		}else {
			ckbPlanesPotestativosRenovacionNo.click();
		}		
	}
	
	public void Revision(String data) {
		cmbRevision.click();
		String xpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+data+"']";
		getDriver().findElement(By.xpath(xpath)).click();		
	}
	
		
	public void CondicionesBeneficios(String data) {
		
		getDriver().switchTo().parentFrame();
		getDriver().switchTo().frame(0);
		//getDriver().switchTo().frame(getDriver().findElement(By.xpath("//div[@id='root']/div/div/div[2]/div/div/div[1]/div/div[7]/div/div[1]/div/div/div[2]/div/div/div/div[4]/div[1]/div/div[2]/iframe")));
		getDriver().findElement(By.id("tinymce")).sendKeys(data);
		getDriver().switchTo().defaultContent();
		//txtCondicionesBeneficios.sendKeys(data);
	}
	
	public void RutaPlanes(String data) {
		
		txtRutaPlanes.sendKeys(data);
	}
	
	
	public void AgregarArchivo(String data) throws IOException {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		String ruta = new File(".").getCanonicalPath();		
		
		ruta = ruta + "\\src\\test\\resources\\Files\\" + data;		
		
		WebElement uploadElement = getDriver().findElement(By.id("my-file-selector"));
		uploadElement.sendKeys(ruta);
	}
	
	public void EnviarComunicado() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='step-1']/div[7]/div/button[2]")));
		
		getDriver().findElement(By.xpath("//div[@id='step-1']/div[7]/div/button[2]")).click();
		
	}
	
	public void EnviarComunicadoRenovacion() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("step-1")));
		
		getDriver().findElement(By.xpath("//*[@id='step-1']/div[8]/div/button[2]")).click();
		
	}
	
	public void AceptarEnviarComunicado() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("cabeceraEnviarComunicado")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		getDriver().findElement(By.xpath("//*[@id='modalEnviarComunicado']/div/div/div[2]/div[3]/button[2]")).click();
		//btnAceptarEnviarComunicado.click();
	}
	
	public void AceptarComunicadoEnviado() {
		getDriver().findElement(By.xpath("//*[@id='modalMensajeExito']/div/div/div[2]/div[3]/button")).click();
		//btnAceptarComunicadoEnviado.click();
	}

	public void validacion(String data) {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mensajeExito")));
		
		String strMensaje = LblconfirmacionExito.getText();
		assertThat(strMensaje, containsString(data));		
				
		AceptarComunicadoEnviado();
			
	}

	public void AjusteRenovacion(String data) {
		txtAjusteRenovacion.sendKeys(data);
		
	}

	public void cambiosImportantes(String data) {
		
		getDriver().switchTo().parentFrame();
		getDriver().switchTo().frame(1);
		
		getDriver().findElement(By.id("tinymce")).sendKeys(data);
		getDriver().switchTo().defaultContent();
		//txtCondicionesBeneficios.sendKeys(data);
		
	}
	
	
}
package com.choucair.formacion.pageobjects;

import java.io.File;
import java.io.IOException;

/**
 * Estructura básica de clase para Object SolicitarCotizacionPage
 *
 */

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;
import org.openqa.selenium.Keys;

@DefaultUrl("")
public class SolicitarCotizacionPage extends PageObject {
	
	//=============================== POTENCIAL EMPRESA ===============================
	@FindBy(xpath = "//*[@id=\"collapseCero\"]/div/div/div/div[1]/div/input")
	public WebElementFacade txtNumeroTrabajadores;
	
	@FindBy(xpath = "//button[@data-id='cmbRangoPotencialAfiliados']")
	public WebElementFacade cmbPotencialAfliados;
	
	@FindBy(xpath = "//button[@data-id='cmbFacturacionEmpresa']")
	public WebElementFacade cmbFacturacionEmpresa;
	
	//===============================PLAN ANTERIOR ===============================
	@FindBy(xpath = "//*[@id=\"headingThree\"]/h4/a")
	public WebElementFacade cmbPlanAnterior;
	
	@FindBy(xpath = "//button[@data-id='cmbTipoPlanAnterior']")
	public WebElementFacade cmbTipoPlanAnterior;
	
	@FindBy(xpath = "//*[@id=\"datetimepicker1\"]/div/div/div/input")
	public WebElementFacade txtFechaRenovacion;
	
	@FindBy(xpath = "//*[@id=\"datetimepicker1\"]/div/div[2]/div/div[2]/div[2]/div[3]/div[4]")
	public WebElementFacade btnDiaSeleccionar;
	
	@FindBy(id = "dtFehaInicioVigencia")
	public WebElementFacade txtFechaInicioVigencia;
	
	
	@FindBy(xpath = "//*[@id=\"collapseThree\"]/div/div/div/div[3]/label[2]")
	public WebElementFacade RdDiagnosticoAltoCostoSi;
	
	@FindBy(xpath = "//*[@id=\"collapseThree\"]/div/div/div/div[3]/label[3]")
	public WebElementFacade RdDiagnosticoAltoCostoNo;
	
	
	//===============================CONDICIONES ESPECIALES  ===============================
	@FindBy(xpath= "//*[@id=\"headingFour\"]/div/label[1]")
	public WebElementFacade RdCondicionesEspecialesSi;
	
	@FindBy(xpath= "//*[@id=\"headingFour\"]/div/label[2]")
	public WebElementFacade RdCondicionesEspecialesNo;
	
	
	//===============================DOCUMENTACION  ===============================
	
	
	//solicitar documentacion  cliente/broker
	@FindBy(xpath= "//*[@id=\"collapseFive\"]/div/div/div/div[1]/div[3]/button")
	public WebElementFacade btnSolicitarAClienteBroker;
	
	@FindBy(xpath= "//*[@id=\"modalMensajeConfirmacionTipo\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnConfirmarSolicitarACliente;
	
	
	//solicitar a otra eps
	@FindBy(xpath= "//*[@id=\"collapseFive\"]/div/div/div/div[1]/div[4]/button")
	public WebElementFacade btnSolicitarOtraEps;
	
	//Boton enviar para confirmar solicitar documentacion otra eps
	@FindBy(xpath= "//*[@id=\"modalPopupSolicitarDocumentacionOtraEPS\"]/div/div/div[2]/div[2]/button[2]")
	public WebElementFacade btnConfirmarSolicitarEps;
	
	//agregar documento para solicitar documentacion otra EPS
	@FindBy(xpath= "//div[@id='modalPopupSolicitarDocumentacionOtraEPS']/div/div/div[2]/div[1]/div/div[1]/div/label/input")
	public WebElementFacade txtAgregarArchivoSolicitarEps;
	
	
	//checkbox de documentacion
	
	@FindBy(xpath= "//*[@id=\"headingFive\"]/h4/a")
	public WebElementFacade cmbDocumentacion;
	
	@FindBy(xpath= "//*[@id=\"collapseFive\"]/div/div/div/div[2]/label[2]/input")
	public WebElementFacade ckbCartasGarantia;
	
	@FindBy(xpath= "//*[@id=\"collapseFive\"]/div/div/div/div[2]/label[3]/input")
	public WebElementFacade ckbCasosOncologicos;
	
	@FindBy(xpath= "//*[@id=\"collapseFive\"]/div/div/div/div[2]/label[4]/input")
	public WebElementFacade ckbControlAfiliados;
	
	@FindBy(xpath= "//*[@id=\"collapseFive\"]/div/div/div/div[2]/label[5]/input")
	public WebElementFacade ckbDetalleSiniestros;
	
	@FindBy(xpath= "//*[@id=\"collapseFive\"]/div/div/div/div[2]/label[6]/input")
	public WebElementFacade ckbFichaTecnica;
	
	@FindBy(xpath= "//*[@id=\"collapseFive\"]/div/div/div/div[2]/label[7]/input")
	public WebElementFacade ckbPlanVijente;
	
	//Subir Archivos
	@FindBy(xpath= "//form[@id='js-upload-form']/div/div/label/input")
	public WebElementFacade txtAgregarArchivos;
	
	//SplicitarCotizacion	
	@FindBy(xpath= "//*[@id=\"step-1\"]/div[13]/div/button[2]")
	public WebElementFacade btnSolicitarCotizacion;
	
	@FindBy(xpath= "//*[@id=\"modalMensajeConfirmacion\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnConfirmarSolicitarCotizacion;
	
	@FindBy(xpath= "//*[@id=\"modalMensajeExito\"]/div/div/div[2]/div[3]/button")
	public WebElementFacade btnAceptarCotizacionEnviada;
	
	@FindBy(id= "mensajeConfirmacion")
	public WebElementFacade LblMensajeConfirmacion;
	
	@FindBy(id= "mensajeInformativo")
	public WebElementFacade LblMensajeInformativo;
	/*
	 * Según la información ingresada, al trámite le corresponde un plan 
	 * Multiempresa. Por lo tanto se procederá a realizar el 
	 * envío automático al cliente/bróker.
	 *  ¿Desea continuar?
	 */
	
	@FindBy(id = "mensajeExito")
	public WebElementFacade LblconfirmacionExito;
	//Se ha iniciado el concurso regular.
	//Se ha enviado la propuesta formal y se espera el ingreso del resultado del concurso.
	//Se requiere de información y documentación adicional para proceder con la implementación.
	//Se ha enviado el trámite al área de configuración de planes
	
	
	//Guardar Cotizacion	
	@FindBy(id= "//*[@id=\"step-1\"]/div[13]/div/button[1]")
	public WebElementFacade btnGuardarCotizacion;
	
	@FindBy(xpath= "//*[@id='modalMensajeInformativo']/div/div/div[2]/div[3]/button")
	public WebElementFacade btnAceptarDocIncompleta;
	
	public void NumeroTrabajadores(String data) {
	
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='collapseCero']/div/div/div/div[1]/div/input")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		String text = txtNumeroTrabajadores.getAttribute("value");
		if (text.isEmpty()) {
			txtNumeroTrabajadores.sendKeys(data);
		}		
	}
	
	
	public void PotencialAfiliados(String data) {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String text = cmbPotencialAfliados.getText();	
		
		
		if (text.trim().equals("Seleccione")){
			cmbPotencialAfliados.click();
			String xpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+data+"']";
			getDriver().findElement(By.xpath(xpath)).click();	
		}
			
	}
	
	public void FacturacionEmpresa(String data) {
		String text = cmbFacturacionEmpresa.getText();
		
		
		if (text.trim().equals("Seleccione")) {
			cmbFacturacionEmpresa.click();
			String xpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+data+"']";
			getDriver().findElement(By.xpath(xpath)).click();		
		}
		
			
	}
	
	
	public void TipoPlanAnterior(String data) {
		
		cmbPlanAnterior.click();
		
		cmbTipoPlanAnterior.click();	
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String xpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+data+"']";
		getDriver().findElement(By.xpath(xpath)).click();		
	}
	
	public void FechaInicioVigencia(String fecha) {
		
		txtFechaInicioVigencia.click();
		
		String xp = "//*[@id=\"datetimepicker1\"]/div/div[2]/div/div[2]/div[2]/div/div[contains(text(),'1')]";
		List<WebElement> dias = getDriver().findElements(By.xpath(xp));
		
		dias.get(dias.size()-1).click();
		
		if (getDriver().findElements(By.xpath("//div[@id='modalMensajeInformativo' and @style='display: block;']")).size()>0) {
			btnAceptarDocIncompleta.click();
		}
		
		txtFechaInicioVigencia.clear();
		
		txtFechaInicioVigencia.sendKeys(fecha);
		
	}
	
	public void FechaRenovacion(String data) {
		
		String xp = "//*[@id='datetimepicker1']/div/div/div/input";
		
		WebElement fechaRenovacion = getDriver().findElements(By.xpath(xp)).get(1);
		
		fechaRenovacion.click();		
		btnDiaSeleccionar.click();
		
		fechaRenovacion.clear();
		fechaRenovacion.sendKeys(data);
		
	}
	
	public void DiagnosticoAltoCosto(String data) {
		String answer = data.toLowerCase().trim();
		if (answer.equals("si")) {
			RdDiagnosticoAltoCostoSi.click();
		}else {
			RdDiagnosticoAltoCostoNo.click();
			
		}
	}
	
	public void CondicionesEspeciales(String data) {
		String answer = data.toLowerCase().trim();
		if (answer.equals("no")) {
			RdCondicionesEspecialesNo.click();
		}else {
			RdCondicionesEspecialesSi.click();
			
		}
	}
	
	public void SolicitarDocCliente(String data) {
		String answer = data.toLowerCase().trim();
		if (answer.equals("si")) {
			btnSolicitarAClienteBroker.click();
			btnConfirmarSolicitarACliente.click();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void SolicitarDocOtraEps(String confirm, String path) throws IOException {
		String answer = confirm.toLowerCase().trim();
		if (answer.equals("si")) {
			btnSolicitarOtraEps.click();
			
			String ruta = new File(".").getCanonicalPath();		
			
			ruta = ruta + "\\src\\test\\resources\\Files\\" + path;
		
			 WebElement uploadElement = getDriver().findElement(By.xpath("//div[@id='modalPopupSolicitarDocumentacionOtraEPS']/div/div/div[2]/div[1]/div/div[1]/div/label/input"));
			 uploadElement.sendKeys(ruta);
			 
			 
			 btnConfirmarSolicitarEps.click();
			 try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			//txtAgregarArchivoSolicitarEps.sendKeys(data);
		}
	}
	
	public void MarcarDocumentacion() {
		cmbDocumentacion.click();
		ckbCartasGarantia.click();
		ckbCasosOncologicos.click();
		ckbControlAfiliados.click();
		ckbDetalleSiniestros.click();
		ckbFichaTecnica.click();
		ckbPlanVijente.click();
	}
	
	public void MarcarDocumentacionIncompleta() {
		cmbDocumentacion.click();
		ckbCartasGarantia.click();
		ckbCasosOncologicos.click();
		ckbControlAfiliados.click();
//		ckbDetalleSiniestros.click();
//		ckbFichaTecnica.click();
//		ckbPlanVijente.click();
	}
	
	
	public void MarcarCartasGarantia(String doc1,String doc2,String doc3,String doc4,String doc5,String doc6) {		
		String catarsGar = doc1.toLowerCase().trim();
		String casosOnc = doc2.toLowerCase().trim();
		String controlAf = doc3.toLowerCase().trim();
		String detalleSin = doc4.toLowerCase().trim();
		String fichaTec = doc5.toLowerCase().trim();
		String planVig = doc6.toLowerCase().trim();
						
		if (catarsGar.equals("si")) {
			ckbCartasGarantia.click();
		}
		if (casosOnc.equals("si")) {
			ckbCartasGarantia.click();
		}
		if (controlAf.equals("si")) {
			ckbCartasGarantia.click();
		}
		if (detalleSin.equals("si")) {
			ckbCartasGarantia.click();
		}
		if (fichaTec.equals("si")) {
			ckbCartasGarantia.click();
		}
		if (planVig.equals("si")) {
			ckbCartasGarantia.click();
		}
	}
	
	public void AgregarArchivo(String data) throws IOException {
		
		String ruta = new File(".").getCanonicalPath();		
		
		ruta = ruta + "\\src\\test\\resources\\Files\\" + data;		
		
		WebElement uploadElement = getDriver().findElement(By.xpath("//div[@class='form-group']/label/input"));
		uploadElement.sendKeys(ruta);
	}
		
	public void SolicitarCotizacion() {
		btnSolicitarCotizacion.click();
	
	}
	
	public void GuardarCotizacion(String data) {
		btnGuardarCotizacion.click();
		 try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	}

	public void Validacion() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mensajeConfirmacion")));
		
		String strMensaje = LblMensajeConfirmacion.getText();
		assertThat(strMensaje, containsString("plan Multiempresa"));
		
		btnConfirmarSolicitarCotizacion.click();		
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mensajeExito")));
		
		strMensaje = LblconfirmacionExito.getText();
		assertThat(strMensaje, containsString("Se ha enviado la cotización al cliente/bróker con un plan Multiempresa."));
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalMensajeExito")));
		btnAceptarCotizacionEnviada.click();
	}
	
	public void ValidacionDocumentacionIncompleta() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalMensajeInformativo")));
		
		String strMensaje = LblMensajeInformativo.getText();
		assertThat(strMensaje, containsString("No tiene la documentación necesaria para solicitar la cotización del trámite. Puede solicitar la documentación faltante por correo electrónico con la opción: Solicitar a otra EPS."));
		
		btnAceptarDocIncompleta.click();
		
	}

	

	
	
}
package com.choucair.formacion.pageobjects;

/**
 * Estructura básica de clase para Object RnNuevoTramitePage
 *
 */

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.text.ParseException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("")
public class RnNuevoTramitePage extends PageObject {
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div/div[1]/div[1]/div/button")
	public WebElementFacade btnIniciarTramite;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div/div[1]/div[1]/div/ul/li[2]/a")
	public WebElementFacade optRenovacion;	

	@FindBy(xpath = "//button[@data-id='cmbGrupoEconomico']")
	public WebElementFacade cmbGrupoEconomico;
	
	@FindBy(xpath = "//button[@data-id='cmbBroker']")
	public WebElementFacade cmbBroker;
	
	@FindBy(xpath = "//button[@data-id='cmbTipoPlan']")
	public WebElementFacade cmbTipoPlan;
	
	@FindBy(xpath = "//*[@id=\"collapseDos\"]/div/div/div/div[1]/button")
	public WebElementFacade btnAgregarEmpresa;
	
	
	//datos empresa
	@FindBy(xpath = "//*[@id=\"modalDatosEmpresa\"]/div/div/div[2]/div[1]/div[1]/div/input")
	public WebElementFacade txtRuc;
	
	@FindBy(xpath = "//*[@id=\"modalDatosEmpresa\"]/div/div/div[2]/div[1]/div[2]/div/input")
	public WebElementFacade txtRazonSocial;
	
	@FindBy(xpath = "//*[@id=\"modalDatosEmpresa\"]/div/div/div[2]/div[1]/div[3]/div/input")
	public WebElementFacade txtNumeroPoliza;
	
	// fecha de vigencia - si es plan multiempresa son 45 dias despues
	// si es plan personalizado son 60 dias despues 
	@FindBy(xpath = "//*[@id=\"datetimepicker1\"]/div/div[1]/div/input")
	public WebElementFacade txtFechaFinVigencia;
	
	@FindBy(xpath = "//*[@id=\"modalDatosEmpresa\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade BtnAceptarEmpresa;
	
	
	//datos de contacto
	
	@FindBy(xpath = "//*[@id=\"mytable\"]/tbody/tr/td[6]/a/img")
	public WebElementFacade btnContacto;
	
	@FindBy(name = "DireccionTemp")
	public WebElementFacade txtPersona;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[2]/div/input")
	public WebElementFacade txtCorreoContacto;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[3]/div/input")
	public WebElementFacade txtTelefonoContacto;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[4]/div/input")
	public WebElementFacade txtCargoContacto;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[6]/div[1]/label[2]")
	public WebElementFacade ckbComunicacionElectronicaSi;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[7]/div/input")
	public WebElementFacade txtCorreoComunicacion;		
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[6]/div[1]/label[3]")
	public WebElementFacade ckbComunicacionElectronicaNo;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[9]/div[1]/label[2]")
	public WebElementFacade ckbPolizaElectronicaSi;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[10]/div/input")
	public WebElementFacade txtCorreoPoliza;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[9]/div[1]/label[3]")
	public WebElementFacade ckbPolizaElectronicaNo;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[11]/div/input")
	public WebElementFacade txtDireccion;
	
	@FindBy(xpath ="//button[@data-id='cmbDepartamento']")
	public WebElementFacade cmbDeparatamento;
	
	@FindBy(xpath = "//button[@data-id='cmbProvincia']")
	public WebElementFacade cmbProvincia;
	
	@FindBy(xpath = "//button[@data-id='cmbDistrito']")
	public WebElementFacade cmbDistrito;
		
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnAceptarContacto;
	
	
	
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div/div[1]/button[2]")
	public WebElementFacade btnGuardar;	
	
	@FindBy(xpath = "//*[@id=\"modalMensajeConfirmacion\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnConfirmacion;
	
	@FindBy(id = "mensajeExito")
	public WebElementFacade lblConfirmacion;
	
	@FindBy(xpath = "//*[@id=\"modalMensajeExito\"]/div/div/div[2]/div[3]/button")
	public WebElementFacade btnAceptar;
		
	
	public void Renovacion() {
		optRenovacion.click();	
	}
	
	public void GrupoEconomico(String data) {
		cmbGrupoEconomico.click();
		String xpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+data+"']";
		getDriver().findElement(By.xpath(xpath)).click();			
	}
	public void Broker(String data) {
		cmbBroker.click();
		String xpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+data+"']";
		getDriver().findElement(By.xpath(xpath)).click();			
	}
	
	public void TipoPlan(String data) {
		cmbTipoPlan.click();
		String xpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+data+"']";
		getDriver().findElement(By.xpath(xpath)).click();			
	}
	
	public void AgregarEmpresa() {
		btnAgregarEmpresa.click();	
	}

	public void Ruc(String data) {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalDatosEmpresa")));
		
		txtRuc.sendKeys(data);
	}
	
	public void RazonSocial(String data) {
		txtRazonSocial.sendKeys(Keys.TAB);
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		String text = txtRazonSocial.getAttribute("value");
		
		if (text.isEmpty()) {
			
			txtRazonSocial.sendKeys(data);
//			txtRazonSocial.sendKeys(Keys.TAB);
		}
	}
	
	
	
	public void NumeroPoliza(String data) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		txtNumeroPoliza.sendKeys(data);
	}
	
	// fecha de vigencia - si es plan multiempresa son 45 dias despues
	// si es plan personalizado son 60 dias despues 
	public void FechaVigencia(String date,String TipoPlan) {
		
		 LocalDate localDate = LocalDate.now();
		 
		String oldDate = DateTimeFormatter.ofPattern("dd/MM/yyyy").format(localDate);		
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		Calendar c = Calendar.getInstance();
		
		try{
			   c.setTime(sdf.parse(oldDate));
			}catch(ParseException e){
			   e.printStackTrace();
			 }
		
		
		if (TipoPlan.trim().toLowerCase().equals("personalizado")) {
			c.add(Calendar.DAY_OF_MONTH, 60);  
		}else {
			c.add(Calendar.DAY_OF_MONTH, 45);  
		}
		
		String newDate = sdf.format(c.getTime());  
		
		txtFechaFinVigencia.click();
		
		txtFechaFinVigencia.sendKeys(Keys.ENTER);
				
		txtFechaFinVigencia.clear();
		
		txtFechaFinVigencia.sendKeys(newDate);
		
	}
	
	public void Aceptar() {
		
		BtnAceptarEmpresa.click();
	}
	
	public void Contacto() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"mytable\"]/tbody/tr/td[6]/a/img")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		btnContacto.click();
	}
	
	public void AceptarContacto() {
		
		btnAceptarContacto.click();
	}
	
	
	public void PersonaContacto(String data) {
		
		String text = txtPersona.getAttribute("value");
		
		if (text.isEmpty()) {
			txtPersona.sendKeys(data);
		}
	}
	
	public void CorreoContacto(String data) {
		
		String text = txtCorreoContacto.getAttribute("value");
		
		if (text.isEmpty()) {
			txtCorreoContacto.sendKeys(data);
		}
	}

	public void TelefonoContacto(String data) {
	
		String text = txtTelefonoContacto.getAttribute("value");
		
		if (text.isEmpty()) {
			txtTelefonoContacto.sendKeys(data);
		}
	}

	public void CargoContacto(String data) {
		
		String text = txtCargoContacto.getAttribute("value");
		
		if (text.isEmpty()) {
			txtCargoContacto.sendKeys(data);
		}
	}
	
	public void ComunicacionElectronica(String data,String correo) {
		
		String answer = data.toLowerCase().trim();		
		
		if (answer.equals("si")) {
			ckbComunicacionElectronicaSi.click();
			String mail = txtCorreoComunicacion.getAttribute("value");
			if (mail.isEmpty()) {
				
				txtCorreoComunicacion.sendKeys(correo);
			}
			
		}else
		{
			ckbComunicacionElectronicaNo.click();
		}
	}
	
	public void PolizaElectronica(String data,String correo) {
		
		String answer = data.toLowerCase().trim();		
		
		if (answer.equals("si")) {
			ckbPolizaElectronicaSi.click();
			String mail = txtCorreoPoliza.getAttribute("value");
			if (mail.isEmpty()) {
				
				txtCorreoPoliza.sendKeys(correo);
			}
			
		}else
		{
			ckbPolizaElectronicaNo.click();
		}
	}
	
	public void CorreoComunicacion(String data) {
		
		txtCorreoComunicacion.sendKeys(data);
	}
	
	public void CorreoPoliza(String data) {
		
		txtCorreoPoliza.sendKeys(data);
	}
	
	
	
	public void Direccion(String dir,String Dep,String Prov,String Dis) {
		
		String text = txtDireccion.getAttribute("value");
		
		if (text.isEmpty()) {
			txtDireccion.sendKeys(dir);
			
			String xpathDep =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+Dep+"']";
			String xpathProv =  "//div[@class='modal-body']/div/div[3]/div/div/div/ul/li/a/span[text()='"+Prov+"']";
			String xpathdis =  "//div[@class='modal-body']/div/div[4]/div/div/div/ul/li/a/span[text()='"+Dis+"']";
			
			cmbDeparatamento.click();
			getDriver().findElement(By.xpath(xpathDep)).click();
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
							
			cmbProvincia.click();
			getDriver().findElement(By.xpath(xpathProv)).click();
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			
			cmbDistrito.click();
			getDriver().findElement(By.xpath(xpathdis)).click();				
			
		}
	}
	
	public void Guardar() {
		
		btnGuardar.click();
	}
	
	public void Confirmacion() {
		
		btnConfirmacion.click();
	}
	
public void VerificacionRegistroTramiteRenovacion() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mensajeExito")));
		
		String strMensaje = lblConfirmacion.getText();
		
		String Cod = strMensaje.substring(strMensaje.length() - 6); 	
		
		String CodTramite = Cod.substring(0,Cod.length()-1);
	
		Serenity.setSessionVariable("codTramite").to(CodTramite);		
		
		assertThat(strMensaje, containsString("Se ha registrado el trámite con código RV"));
		
		btnAceptar.click();		
		
	}
	
	
}
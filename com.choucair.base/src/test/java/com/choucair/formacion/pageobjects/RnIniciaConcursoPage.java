package com.choucair.formacion.pageobjects;

import java.io.File;
import java.io.IOException;

/**
 * Estructura básica de clase para Object RnIniciaConcursoPage
 *
 */

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("")
public class RnIniciaConcursoPage extends PageObject {
	
	@FindBy(xpath = "//*[@id='modalPopupIniciaConcurso']/div/div/div[2]/div[1]/div/div[1]")
	public WebElementFacade txtCartaInvitacion;
	
	@FindBy(xpath = "//button[@data-id='cmbBroker']")
	public WebElementFacade cmbBroker;	
	
	@FindBy(name = "EmpresaPersonaContacto")
	public WebElementFacade txtEmpresaPersonaContacto;
	
	@FindBy(name = "EmpresaPersonaCorreo")
	public WebElementFacade txtEmpresaPersonaCorreo;
	
	@FindBy(name = "EmpresaPersonaTelefono")
	public WebElementFacade txtEmpresaPersonaTelefono;
	
	@FindBy(xpath = "//*[@id=\"modalPopupIniciaConcurso\"]/div/div/div[2]/div[1]/div/div[7]/div[1]/div/div[2]/input")
	public WebElementFacade txtBrokerPersonaNombre;
	
	@FindBy(name = "BrokerPersonaCorreo")
	public WebElementFacade txtBrokerPersonaCorreo;
	
	@FindBy(name = "BrokerPersonaTelefono")
	public WebElementFacade txtBrokerPersonaTelefono;
	
	@FindBy(xpath = "//*[@id=\"modalPopupIniciaConcurso\"]/div/div/div[2]/div[2]/button[2]")
	public WebElementFacade btnGuardar;
	
	@FindBy(xpath ="//*[@id=\"modalMensajeConfirmacionTipo\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnConfirmar;
	
	
	@FindBy(xpath ="//*[@id=\"modalMensajeConfirmacionTipo\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade lblConfirmacion;	

	@FindBy(xpath ="//*[@id=\"modalMensajeExito\"]/div/div/div[2]/div[3]/button")
	public WebElementFacade btnAceptarConfirmacion;
	
	@FindBy(id ="mensajeExito")
	public WebElementFacade lblMensajeExito;
	
	
	
	//metodos
	public void CartaInvitacion(String path) throws IOException {
		String ruta = new File(".").getCanonicalPath();		
		
		ruta = ruta + "\\src\\test\\resources\\Files\\" + path;		
		
		WebElement uploadElement = getDriver().findElement(By.xpath("//*[@id='modalPopupIniciaConcurso']/div/div/div[2]/div[1]/div/div[1]/div/label/input"));
		uploadElement.sendKeys(ruta);		
	}
	
	public void Comentarios(String data) {
		
		 getDriver().switchTo().frame(getDriver().findElement(By.xpath("//div[@id='modalPopupIniciaConcurso']/div/div/div[2]/div[1]/div/div[4]/div/div[1]/div[1]/div/div[2]/iframe")));
		 getDriver().findElement(By.id("tinymce")).sendKeys(data);
		 getDriver().switchTo().defaultContent();
		 
//		getDriver().switchTo().parentFrame();
//		getDriver().switchTo().frame(5);
//		getDriver().findElement(By.id("tinymce")).sendKeys(data);
//		getDriver().switchTo().defaultContent();
		
	}
	
	public void DocumentoOpcional(String path) throws IOException {
		
		String ruta = new File(".").getCanonicalPath();		
		
		ruta = ruta + "\\src\\test\\resources\\Files\\" + path;		
		WebElement uploadElement = getDriver().findElement(By.xpath("//*[@id='modalPopupIniciaConcurso']/div/div/div[2]/div[1]/div/div[4]/div/div[2]/label/input"));
		uploadElement.sendKeys(ruta);		
	}
	
	public void Broker(String data) {
		
		String broker = cmbBroker.getText();
		
		if (broker.isEmpty()) {
			
			cmbBroker.click();
			String xpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+data+"']";
			getDriver().findElement(By.xpath(xpath)).click();		
		}
		
	}
	
	public void EmpresaPersonaContacto(String data) {
		String text = txtEmpresaPersonaContacto.getText();
		if (text.isEmpty()) {
			txtEmpresaPersonaContacto.sendKeys(data);
		}		
	}
	
	public void EmpresaPersonaCorreo(String data) {
		txtEmpresaPersonaCorreo.sendKeys(data);
	}
	
	public void EmpresaPersonaTelefono(String data) {
		txtEmpresaPersonaTelefono.sendKeys(data);
	}
	
	public void BrokerPersonaNombre(String data) {
		txtBrokerPersonaNombre.sendKeys(data);
	}
	
	public void BrokerPersonaCorreo(String data) {
		txtBrokerPersonaCorreo.sendKeys(data);
	}
	
	public void BrokerPersonaTelefono(String data) {
		txtBrokerPersonaTelefono.sendKeys(data);
	}
	
	public void Guardar() {
		btnGuardar.click();
	}

	public void validacion() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
				
		btnConfirmar.click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mensajeExito")));
		
		String strMensaje = lblMensajeExito.getText();
		assertThat(strMensaje, containsString("Se ha iniciado el concurso"));
				
		btnAceptarConfirmacion.click();
		
		
	}
}
package com.choucair.formacion.pageobjects;

import java.io.File;
import java.io.IOException;

/**
 * Estructura básica de clase para Object RequisitosPreimplementacionPage
 *
 */

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

@DefaultUrl("")
public class RequisitosPreimplementacionPage extends PageObject {
	
	@FindBy(xpath = "//*[@id=\"collapseUno\"]/div/div/div[1]/div[1]/label[2]")
	public WebElementFacade ckbExisteRimacSalud;
	
	@FindBy(xpath = "//*[@id=\"collapseUno\"]/div/div/div[1]/div[1]/label[3]")
	public WebElementFacade ckbExisteRimacSaludNo;
	
	@FindBy(xpath = "//*[@id=\"collapseUno\"]/div/div/div[2]/div[1]/label[2]")
	public WebElementFacade ckbPolizaElectronica;
	

	@FindBy(xpath = "//*[@id=\"collapseUno\"]/div/div/div[2]/div[1]/label[3]")
	public WebElementFacade ckbPolizaElectronicaNo;
	
	@FindBy(xpath = "//*[@id=\"collapseUno\"]/div/div/div[3]/div[1]/label[2]")
	public WebElementFacade ckbContratoDigital;
		
	@FindBy(xpath = "//*[@id=\"collapseUno\"]/div/div/div[3]/div[1]/label[3]")
	public WebElementFacade ckbContratoDigitalNo;
	
	
	//adjuntar poliza
	@FindBy(xpath = "//*[@id=\"my-file-selector1\"]")
	public WebElementFacade txtAjuntarPoliza;
	
	//ContactoPoliza Electronica
	@FindBy(name = "ContactoPolizaElectronica")
	public WebElementFacade txtContactoPoliza;	
	
	//CorreoContacto Poliza Electronica
	@FindBy(name = "CorreoContactoPolizaElectronica")
	public WebElementFacade txtCorreoContactoPoliza;
	
	
	//contacto contrato digital
	@FindBy(name = "ContactoContratoDigital")
	public WebElementFacade txtContratoDigital;
	
	//Correo Contacto Contrato Digital
	@FindBy(name = "CorreoContactoContratoDigital")
	public WebElementFacade txtCorreoContratoDigital;
	
	
	
	//Informacion para contrato ==========
	
	//Información para contrato:
	@FindBy(xpath = "//*[@id=\"headingDos\"]/h4/a")
	public WebElementFacade cmbInformaciónContrato;	
	
	
	@FindBy(xpath = "//button[@data-id='cmbTipoDocumento']")
	public WebElementFacade cmbTipoDocumento;
	
	@FindBy(id = "cmbTipoDocumento")
	public WebElementFacade cmbTipoDocumentoRenovacion;
	
	@FindBy(name = "DNIRepresentanteLegal")
	public WebElementFacade txtDniRepresentanteLegal;
	
	@FindBy(name = "NombreRepresentanteLegal")
	public WebElementFacade txtNombreRepresentanteLegal;
	
	
	//direccion
	@FindBy(xpath = "//*[@id=\"collapseDos\"]/div/div/div/div[4]/div/div[1]/span/a/img")
	public WebElementFacade imgDireccion;
	
	@FindBy(name = "DireccionLegalTemp")
	public WebElementFacade txtDireccion;
	
	@FindBy(xpath = "//button[@data-id='cmbDepartamento']")
	public WebElementFacade cmbDepartamento;
	
	@FindBy(xpath = "//button[@data-id='cmbProvincia']")
	public WebElementFacade cmbProvincia;
	
	@FindBy(xpath = "//button[@data-id='cmbDistrito']")
	public WebElementFacade cmbDistrito;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnAceptarDireccion;
	
		
	@FindBy(name
			= "PartidaElectronica")
	public WebElementFacade txtPartidaElectronica;
	
	
	//Acta de escrutinio  =============
	
	@FindBy(xpath = "//*[@id=\"headingCuatro\"]/h4/a")
	public WebElementFacade cmbActaEscrutinio;
	
	@FindBy(xpath = "//*[@id=\"my-file-selector2\"]")
	public WebElementFacade txtAdjuntarActaEscrutinio;
	
	@FindBy(xpath = "//*[@id=\"datetimepicker2\"]/div/div/div/input")
	public WebElementFacade txtFechaVotacion;
	
	@FindBy(xpath = "//*[@id=\"datetimepicker2\"]/div/div[2]/div/div[2]/div[2]/div[5]/div[4]")
	public WebElementFacade txtDiaFechaVotacion;
	
	
	//Contacto de Empresa para Correo de Bienvenida:
	@FindBy(xpath = "//*[@id=\"headingCinco\"]/h4/a")
	public WebElementFacade cmbContactoEmpresaBienvenida;
	
		
	@FindBy(name = "ContactoEmpresaBienvenida")
	public WebElementFacade txtPersonaContacto;
	
	@FindBy(name = "CorreoContactoEmpresaBienvenida")
	public WebElementFacade txtCorreoContacto;
	
	
	//Informacion Adicional
	@FindBy(xpath = "//*[@id=\"headingThree\"]/h4/a")
	public WebElementFacade cmbInformacionAdicional;
	
		
	@FindBy(xpath = "//*[@id=\"datetimepicker1\"]/div/div/div/input")
	public WebElementFacade txtFechaInicioVigencia;
	
	
	@FindBy(xpath = "//*[@id=\"datetimepicker1\"]/div/div[2]/div/div[2]/div[2]/div[5]/div[5]")
	public WebElementFacade txtDiaFechaInicioVigencia;
	
	
	@FindBy(xpath = "//button[@data-id='cmbProducto']")
	public WebElementFacade cmbProducto;
	
	
	//Documentacion
	@FindBy(xpath = "//*[@id=\"headingFive\"]/h4/a")
	public WebElementFacade cmbDocumentacion;
	
	@FindBy(xpath = "//*[@id=\"collapseFive\"]/div/div/div[1]/div[1]/label[2]/input")
	public WebElementFacade ckbActaEscrutinio;
	
	
	//*[@id="collapseFive"]/div/div/div[1]/div[1]/label[2]
	
//	
//	@FindBy(xpath = "//*[@id=\"collapseFive\"]/div/div/div[1]/div[1]/label[3]/input")
//	public WebElementFacade ckbAutorizacionEnvioDigital;
//	
	@FindBy(xpath = "//*[@id=\"collapseFive\"]/div/div/div[1]/div[1]/label[4]/input")
	public WebElementFacade ckbCartaNombramiento;
	
	@FindBy(xpath = "//*[@id=\"collapseFive\"]/div/div/div[1]/div[2]/label[5]/input")
	public WebElementFacade ckbTramaAsegurados;
	
	
	//adjuntar Archivo
//	@FindBy(xpath = "//*[@id=\"my-file-selector3\"]")
//	public WebElementFacade txtAdjuntarDocumentacion;
	@FindBy(id = "my-file-selector3")
	public WebElementFacade txtAdjuntarDocumentacion;
	
	//Iniciar IMPLEMENTACION
	@FindBy(xpath = "//*[@id=\"step-2\"]/div[4]/div/button[2]")
	public WebElementFacade btnIniciarImplementacion;
	
	//confirmar iniciar implementacion
	@FindBy(xpath = "//*[@id=\"modalMensajeConfirmacionTipo\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnIniciarImplementacionSi;
	
	//aceptar inicio de implementacion
	@FindBy(xpath = "//*[@id='modalMensajeExito']/div/div/div[2]/div[3]/button")
	public WebElementFacade btnAceptarInicioImplementacion;
		
	@FindBy(id = "mensajeExito")
	public WebElementFacade LblconfirmacionExito;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[7]/div/div/div/div[1]/div/a/img")
	public WebElementFacade btnActividadPendiente;
	
	@FindBy(xpath = "//*[@id=\"collapseUno\"]/div/div/div[1]/div[2]/label[2]")
	public WebElementFacade ckbAutorizacionEnvioDigitalRenovacion;
	
	
	@FindBy(xpath = "//*[@id=\"collapseFive\"]/div/div/div[1]/div[1]/label[3]/input")
	public WebElementFacade ckbAutorizacionEnvioDigital;
	
	
	//=============Metodos ===================
	
	public void ExisteRimacSalud(String data) {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("collapseUno")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		String answer = data.toLowerCase().trim();
		if (answer.equals("no")) {
			ckbExisteRimacSaludNo.click();
		}else {
			ckbExisteRimacSalud.click();
			
		}
	}
	
	public void PolizaElectronica(String data) {
		String answer = data.toLowerCase().trim();
		if (answer.equals("no")) {
			ckbPolizaElectronicaNo.click();
		}else {
			ckbPolizaElectronica.click();
			
		}
	}
	
	public void ContratoDigital(String data) {
		String answer = data.toLowerCase().trim();
		if (answer.equals("no")) {
			ckbContratoDigitalNo.click();
		}else {
			ckbContratoDigital.click();
			
		}
	}
	
	public void AdjuntarPoliza(String path) throws IOException {
		
		String ruta = new File(".").getCanonicalPath();		
		ruta = ruta + "\\src\\test\\resources\\Files\\" + path;		
		
		WebElement uploadElement = getDriver().findElement(By.id("my-file-selector1"));
		 uploadElement.sendKeys(ruta);		
	}
	
	public void ContactoPoliza(String Contacto) {
		
		String text = txtContactoPoliza.getValue();
		
		if (text.isEmpty()) {
			//txtCorreoContacto.clear();
			txtContactoPoliza.sendKeys(Contacto);
		}	
			
	}
	
	public void CorreoPoliza(String Contacto) {
				
		String text = txtCorreoContactoPoliza.getValue();
		
		if (text.isEmpty()) {
			//txtCorreoContacto.clear();
			txtCorreoContactoPoliza.sendKeys(Contacto);
		}		
	}
	
	
	public void ContactoContrato(String Contacto) {
		
		String text = txtContratoDigital.getValue();
		
		if (text.isEmpty()) {
			//txtCorreoContacto.clear();
			txtContratoDigital.sendKeys(Contacto);
		}
			
	}
	
	public void CorreoContrato(String Contacto) {
					
		String text = txtCorreoContratoDigital.getValue();
		
		if (text.isEmpty()) {
			//txtCorreoContacto.clear();
			txtCorreoContratoDigital.sendKeys(Contacto);
		}		
	}
	
	//Informacion para contrato ==========
	
	public void InformaciónContrato() {
		
		cmbInformaciónContrato.click();		
				
	}
	
	public void TipoDocumentoRL(String data) {
		
		cmbTipoDocumento.click();
		String Elementxpath =  "//*[@id=\"collapseDos\"]/div/div/div/div[1]/div/div[1]/div/ul/li[2]/a/span[text()='"+data+"']";

		WebElement elemento = getDriver().findElement(By.xpath(Elementxpath));	
		
		elemento.click();	
		
	}
	
	public void TipoDocumentoRLRenovacion(String data) {
		
		cmbTipoDocumentoRenovacion.selectByVisibleText(data);
		
	}
		
	
	
	public void RepresentanteLegal(String Contacto) {
		
		txtDniRepresentanteLegal.sendKeys(Contacto);		
				
	}
	
	public void NombreRL(String Contacto) {
		
		txtNombreRepresentanteLegal.sendKeys(Contacto);		
				
	}
	
	public void PartidaElectronica(String Contacto) {
		
		txtPartidaElectronica.sendKeys(Contacto);		
				
	}
	
	public void Direccion(String dir, String dep, String prov, String dis) {
		imgDireccion.click();
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		
		txtDireccion.sendKeys(dir);
		
		String xpathDep =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+dep+"']";
		String xpathProv =  "//div[@class='modal-body']/div/div[3]/div/div/div/ul/li/a/span[text()='"+prov+"']";
		String xpathdis =  "//div[@class='modal-body']/div/div[4]/div/div/div/ul/li/a/span[text()='"+dis+"']";
		
		cmbDepartamento.click();
		getDriver().findElement(By.xpath(xpathDep)).click();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		cmbProvincia.click();
		getDriver().findElement(By.xpath(xpathProv)).click();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		cmbDistrito.click();
		getDriver().findElement(By.xpath(xpathdis)).click();				
		
		btnAceptarDireccion.click();
	}
	
	
	//============= Acta de escrutinio  =============
	
	public void ActaEscrutinio() {
		
		cmbActaEscrutinio.click();				
	}
	
	public void AdjuntarActaEscrutinio(String path) throws IOException {
		String ruta = new File(".").getCanonicalPath();		
		
		ruta = ruta + "\\src\\test\\resources\\Files\\" + path;		
		
		WebElement uploadElement = getDriver().findElement(By.id("my-file-selector2"));
		 uploadElement.sendKeys(ruta);		
	}
	
	public void FechaVotacion(String data) {
		btnIniciarImplementacion.sendKeys("");
		txtFechaVotacion.click();
		txtDiaFechaVotacion.click();	
		txtFechaVotacion.clear();
		txtFechaVotacion.sendKeys(data);
	}
	
	public void ContactoBienvenida() {
		
		cmbContactoEmpresaBienvenida.click();				
	}
	
	
	public void PersonaContacto(String Contacto) {
				
		String text = txtPersonaContacto.getValue();
		
		if (text.isEmpty()) {
			//txtCorreoContacto.clear();
			txtPersonaContacto.sendKeys(Contacto);
		}
				
	}

	public void CorreoContacto(String correo) {
		
		String text = txtCorreoContacto.getValue();
		
		if (text.isEmpty()) {
			//txtCorreoContacto.clear();
			txtCorreoContacto.sendKeys(correo);
		}
		
				
	}
	
	public void InformacionAdicional() {
		
		cmbInformacionAdicional.click();				
	}
	
	public void FechaInicioVigencia(String data) {
		
		String text = txtFechaInicioVigencia.getValue();
		
		if (text.isEmpty()) {
			txtFechaInicioVigencia.click();
			txtDiaFechaInicioVigencia.click();		
			txtFechaInicioVigencia.clear();
			txtFechaInicioVigencia.sendKeys(data);
			
		}		
		
	}
	
	public void Producto(String data) {
		cmbProducto.click();
		btnIniciarImplementacion.sendKeys("");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String xpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+data+"']";
		getDriver().findElement(By.xpath(xpath)).click();		
	}
	
	
	// DOCUMENTACION
	public void Documentacion() {
			
		cmbDocumentacion.click();				
	}
	
	public void DocActaEscrutinio() {
		
		ckbActaEscrutinio.click();				
	}
	
	public void DocAutorizacionEnvioDigital() {
		
		ckbAutorizacionEnvioDigital.click();				
	}
	
	public void DocCartaNombramiento() {
		
		ckbCartaNombramiento.click();				
	}
	
	public void DocInformacionAdicional() {
		
		ckbTramaAsegurados.click();				
	}
	
	public void AdjuntarDocumentos(String path) throws IOException {
		String ruta = new File(".").getCanonicalPath();		
		
		ruta = ruta + "\\src\\test\\resources\\Files\\" + path;		
		
		WebElement uploadElement = getDriver().findElement(By.id("my-file-selector3"));
		 uploadElement.sendKeys(ruta);		
	}
	
	public void iniciarImplementacion() {
		
		btnIniciarImplementacion.click();				
	}
	
	public void ConfirmacionIniciarImplementacion() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalMensajeConfirmacionTipo")));
		
		btnIniciarImplementacionSi.click();				
	}
	
	
	public void AcptarIniciarImplementacion() {
		
		btnAceptarInicioImplementacion.click();				
	}

	public void validacion() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mensajeExito")));
		
		String strMensaje = LblconfirmacionExito.getText();
		assertThat(strMensaje, containsString("Se ha iniciado la etapa de implementación del trámite"));
		
		AcptarIniciarImplementacion();
		
	}

	public void AdjuntarArchivo(String path) throws IOException {
		
		String ruta = new File(".").getCanonicalPath();		
		ruta = ruta + "\\src\\test\\resources\\Files\\" + path;		
		
		WebElement uploadElement = getDriver().findElement(By.id("my-file-selector3"));
		uploadElement.sendKeys(ruta);		
	}

	public void CheckBox(String EntidadPredecesora) {
		if (EntidadPredecesora.equals("ESSALUD")||EntidadPredecesora.equals("Otra aseguradora")) {
			ckbActaEscrutinio.click();
			ckbAutorizacionEnvioDigital.click();
			ckbCartaNombramiento.click();
		}
		
		
	}

	public void ActividadPendiente() {
	
		String xp = "//*[@id='s4-bodyContainer']/div[2]/div/div/div[1]/div[1]/div[7]/div/div/div/div[1]/div/a/img";
		
		if (getDriver().findElements(By.xpath(xp)).size()!=0) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			WebDriverWait wait = new WebDriverWait(getDriver(), 20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xp)));
			btnActividadPendiente.click();
		}
		
		
	}

	public void AutorizaciónDigital(String trim) {
		ckbAutorizacionEnvioDigital.click();
		
	}
	
	public void AutorizaciónDigitalRenovacion(String trim) {
		ckbAutorizacionEnvioDigitalRenovacion.click();
		
	}

	public void AdjuntarAutorizaciónDigital(String path) throws IOException {
		String ruta = new File(".").getCanonicalPath();
		ruta = ruta + "\\src\\test\\resources\\Files\\" + path;		
		
		WebElement uploadElement = getDriver().findElement(By.id("my-file-selector1"));
		 uploadElement.sendKeys(ruta);	
		
	}

	
	
	
	
}
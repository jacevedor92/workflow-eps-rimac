package com.choucair.formacion.pageobjects;

import java.sql.Date;

/**
 * Estructura básica de clase para Object NuevoTramiteRenovacion
 *
 */

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.Calendar;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("")
public class NuevoTramiteRenovacion extends PageObject {
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div/div[1]/div[1]/div/button")
	public WebElementFacade btnIniciarTramite;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div/div[1]/div[1]/div/ul/li[2]/a")
	public WebElementFacade optRenovacion;	

	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[1]/div/div[1]/button/span[1]")
	public WebElementFacade cmbGrupoEconomico;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[2]/div/div[1]/button/span[1]")
	public WebElementFacade cmbBroker;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[3]/div/div[1]/button/span[1]")
	public WebElementFacade cmbTipoPlan;
	
	@FindBy(xpath = "//*[@id=\"collapseDos\"]/div/div/div/div[1]/button")
	public WebElementFacade btnAgregarEmpresa;
	
	
	//datos empresa
	@FindBy(xpath = "//*[@id=\"modalDatosEmpresa\"]/div/div/div[2]/div[1]/div[1]/div/input")
	public WebElementFacade txtRuc;
	
	@FindBy(xpath = "//*[@id=\"modalDatosEmpresa\"]/div/div/div[2]/div[1]/div[2]/div/input")
	public WebElementFacade txtRazonSocial;
	
	@FindBy(xpath = "//*[@id=\"modalDatosEmpresa\"]/div/div/div[2]/div[1]/div[3]/div/input")
	public WebElementFacade txtNumeroPoliza;
	
	// fecha de vigencia - si es plan multiempresa son 45 dias despues
	// si es plan personalizado son 60 dias despues 
	@FindBy(xpath = "//*[@id=\"modalDatosEmpresa\"]/div/div/div[2]/div[1]/div[1]/div/input")
	public WebElementFacade txtFechaFinVigencia;
	
	@FindBy(xpath = "//*[@id=\"modalDatosEmpresa\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade BtnAceptarEmpresa;
	
	
	//datos de contacto
	
	@FindBy(xpath = "//*[@id=\"mytable\"]/tbody/tr/td[6]/a/img")
	public WebElementFacade btnContacto;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[1]/div/input")
	public WebElementFacade txtPersona;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[2]/div/input")
	public WebElementFacade txtCorreoContacto;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[3]/div/input")
	public WebElementFacade txtTelefonoContacto;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[4]/div/input")
	public WebElementFacade txtCargoContacto;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[6]/div[1]/label[2]")
	public WebElementFacade ckbComunicacionElectronicaSi;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[7]/div/input")
	public WebElementFacade txtCorreoComunicacion;		
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[6]/div[1]/label[3]")
	public WebElementFacade ckbComunicacionElectronicaNo;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[9]/div[1]/label[2]")
	public WebElementFacade ckbPolizaElectronicaSi;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[10]/div/input")
	public WebElementFacade txtCorreoPoliza;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[9]/div[1]/label[3]")
	public WebElementFacade ckbPolizaElectronicaNo;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[11]/div/input")
	public WebElementFacade txtDireccion;
	
	@FindBy(xpath ="//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[12]/div/div[1]/button/span[1]")
	public WebElementFacade cmbDeparatamento;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[13]/div/div[1]/button/span[1]")
	public WebElementFacade cmbProvincia;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[14]/div/div[1]/button/span[1]")
	public WebElementFacade cmbDistrito;
		
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnAceptarContacto;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div/div[1]/button[2]")
	public WebElementFacade btnGuardar;	
	
	@FindBy(xpath = "//*[@id=\"modalMensajeConfirmacion\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnConfirmacion;
	
	@FindBy(id = "mensajeExito")
	public WebElementFacade lblConfirmacion;
	
	@FindBy(id = "//*[@id=\"modalMensajeExito\"]/div/div/div[2]/div[3]/button")
	public WebElementFacade btnAceptar;
	
	
	
	public void GrupoEconomico(String data) {
		cmbGrupoEconomico.click();
		String xpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+data+"']";
		getDriver().findElement(By.xpath(xpath)).click();			
	}
	public void Broker(String data) {
		cmbBroker.click();
		String xpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+data+"']";
		getDriver().findElement(By.xpath(xpath)).click();			
	}
	
	public void TipoPlan(String data) {
		cmbTipoPlan.click();
		String xpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+data+"']";
		getDriver().findElement(By.xpath(xpath)).click();			
	}
	
	public void AgregarEmpresa(String data) {
		btnAgregarEmpresa.click();	
	}

	public void Ruc(String data) {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalDatosEmpresa")));
		
		txtRuc.sendKeys(data);
	}
	
	public void RazonSocial(String data) {		
		
		String text = txtRazonSocial.getAttribute("value");
		
		if (text.isEmpty()) {
			txtRazonSocial.sendKeys(data);
		}
	}
	
	
	
	public void NumeroPoliza(String data) {
		txtNumeroPoliza.sendKeys(data);
	}
	
	// fecha de vigencia - si es plan multiempresa son 45 dias despues
	// si es plan personalizado son 60 dias despues 
	public void FechaVigencia(String data) {
		
		txtFechaFinVigencia.click();
		
		txtFechaFinVigencia.sendKeys(Keys.ENTER);
				
		txtFechaFinVigencia.clear();
		
		txtFechaFinVigencia.sendKeys(data);
		
		BtnAceptarEmpresa.click();
	}
	
	public void Aceptar(String data) {
		
		BtnAceptarEmpresa.click();
	}
	
public void VerificacionRegistroTramiteRenovacion() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mensajeExito")));
		
		String strMensaje = lblConfirmacion.getText();
		
		String CodTramite = strMensaje.substring(strMensaje.length() - 5); 
				
		String destination="";
		Serenity.setSessionVariable("codTramite").to(CodTramite);		
		
		assertThat(strMensaje, containsString("Se ha registrado el trámite con código RV"));
		
		btnAceptar.click();		
		
	}
	
	
	
}
package com.choucair.formacion.pageobjects;

/**
 * Estructura básica de clase para Object RegistrarNuevaEmpresaPage
 *
 */

import java.util.List;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("https://rimacsegurosperu.sharepoint.com/sites/WorkflowSalud/SitePages/AppRimac.aspx/Cliente/NuevoCliente")
public class RegistrarNuevaEmpresaPage extends PageObject {
	
	//boton nueva empresa
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[1]/div[1]/button[2]")
	public WebElementFacade btnNuevaEmpresa;
	
	//Datos de empresa ===========================================================
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[1]/div/input")
	public WebElementFacade txtRuc;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[2]/div/input")
	public WebElementFacade txtRazonSocial;	
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[3]/div/div/span/a/img")
	public WebElementFacade imgDireccion;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[1]/div/input")
	public WebElementFacade txtDireccion;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[2]/div/div[1]/button/span[1]")
	public WebElementFacade cmbDepartamento;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[3]/div/div[1]/button/span[1]")
	public WebElementFacade cmbProvincia;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[1]/div[4]/div/div[1]/button/span[1]")
	public WebElementFacade cmbDistrito;
	
	@FindBy(xpath = "//*[@id=\"modalDireccion\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnAceptarDireccion;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[4]/div/div[1]/button/span[1]")
	public WebElementFacade cmbGrupoEconomico;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[5]/div/div[1]/button/span[1]")
	public WebElementFacade cmbBroker;
	
	//Datos de contacto ===========================================================
	
	@FindBy(xpath = "//*[@id=\"collapseTwo\"]/div/div/div[1]/div[2]/div/input")
	public WebElementFacade txtPersonaContacto;
	
	@FindBy(xpath = "//*[@id=\"collapseTwo\"]/div/div/div[1]/div[3]/div/input")
	public WebElementFacade txtCorreoContacto;
	
	@FindBy(xpath = "//*[@id=\"collapseTwo\"]/div/div/div[1]/div[4]/div/input")
	public WebElementFacade txtTelefonoContacto;
	
	//Datos de Broker =============================
	
	@FindBy(xpath = "//*[@id=\"collapseTwo\"]/div/div/div[2]/div[2]/div/input")
	public WebElementFacade txtBroker;
	
	@FindBy(xpath = "//*[@id=\"collapseTwo\"]/div/div/div[2]/div[3]/div/input")
	public WebElementFacade txtCorreoBroker;
	
	@FindBy(xpath = "//*[@id=\"collapseTwo\"]/div/div/div[2]/div[4]/div/input")
	public WebElementFacade txtTelefonoBroker;

	///////procedencia ========================================
	
	@FindBy(xpath = "//*[@id=\"headingTwo\"]/h4/a[text()='Procedencia']")
	public WebElementFacade cmbProcedencia;
	
	@FindBy(xpath = "//*[@id=\"collapseThree\"]/div/div/div/div[1]/div/div[1]/button/span[1]")
	public WebElementFacade cmbTipoEntidadPredecesora;
	
	@FindBy(xpath = "//*[@id=\"collapseThree\"]/div/div/div/div[2]/div/div[1]/button/span[1]")
	public WebElementFacade cmbEps;
	
	//guardar
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div/div[1]/button[2]")
	public WebElementFacade btnGuardar;
	
	//confirmar guardar
	@FindBy(xpath = "//*[@id=\"modalMensajeConfirmacion\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnConfirmarGuardar;
	
	
	public void NuevaEmpresa() {
		btnNuevaEmpresa.click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void Ruc(String data) {
		txtRuc.sendKeys(data);
	}
	
	public void RazonSocial(String data) {	
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		txtRazonSocial.sendKeys(data);
	}
	
	public void Direccion(String dir, String dep, String prov,String dis) {
		imgDireccion.click();
		
		txtDireccion.sendKeys(dir);
		
		String xpathDep =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+dep+"']";
		String xpathProv =  "//div[@class='modal-body']/div/div[3]/div/div/div/ul/li/a/span[text()='"+prov+"']";
		String xpathdis =  "//div[@class='modal-body']/div/div[4]/div/div/div/ul/li/a/span[text()='"+dis+"']";
		
		cmbDepartamento.click();
		getDriver().findElement(By.xpath(xpathDep)).click();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		cmbProvincia.click();
		getDriver().findElement(By.xpath(xpathProv)).click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		cmbDistrito.click();
		getDriver().findElement(By.xpath(xpathdis)).click();				
		
		btnAceptarDireccion.click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void GrupoEconomico(String data) {
		cmbGrupoEconomico.click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String xpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+data+"']";
		getDriver().findElement(By.xpath(xpath)).click();			
			
	}
	
	public void Broker(String data) {
		cmbBroker.click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String xpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+data+"']";
		getDriver().findElement(By.xpath(xpath)).click();		
	}
	
	public void DatosContacto(String Contacto,String correo,String telefono) {
		
		txtPersonaContacto.sendKeys(Contacto);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		txtCorreoContacto.sendKeys(correo);
		txtTelefonoContacto.sendKeys(telefono);
			
	}
	
	public void DatosBroker(String contactoBroker, String correoBroker, String telefonoBroker) {
		txtBroker.sendKeys(contactoBroker);
		txtCorreoBroker.sendKeys(correoBroker);
		txtTelefonoBroker.sendKeys(telefonoBroker);
		
	}
	
	public void Procedencia(String TipoEntidad,String Opt2) {

		cmbTipoEntidadPredecesora.click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String xpath =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+TipoEntidad+"']";
		getDriver().findElement(By.xpath(xpath)).click();	
		
		if (TipoEntidad!="ESSALUD") {
			cmbEps.click();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			String xpatheps =  "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+Opt2+"']";
			getDriver().findElement(By.xpath(xpatheps)).click();	
			
		}
		
			
	}
	
	public void Guardar() {
		btnGuardar.sendKeys("");
		btnGuardar.click();
			
	}
	
	public void ConfirmarGuardar() {
		btnConfirmarGuardar.click();
			
	}
	
	
	
	
}
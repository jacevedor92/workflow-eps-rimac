package com.choucair.formacion.pageobjects;

/**
 * Estructura básica de clase para Object CrearPolizaPage
 *
 */

import java.util.List;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("")
public class CrearPolizaPage extends PageObject {
	
	@FindBy(xpath = "//*[@id=\"mytable\"]/tbody/tr/td[3]/div/input")
	public WebElementFacade txtNumeroPoliza;
	
	@FindBy(xpath = "//*[@id=\"tinymce\"]/p[1]")
	public WebElementFacade txtComentario;
	
	@FindBy(xpath = "//*[@id=\"step-1\"]/div[5]/div/button[2]")
	public WebElementFacade btnGuardar;
	
	@FindBy(xpath = "//*[@id=\"modalMensajeConfirmacion\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnConfirmarEnvioPoliza;
	
	@FindBy(xpath = "//*[@id=\"modalMensajeExito\"]/div/div/div[2]/div[3]/button")
	public WebElementFacade btnAceptarPolizaCreada;
	
	@FindBy(id = "mensajeConfirmacion")
	public WebElementFacade Lblconfirmacion;
	//Se enviará el trámite a configurar los planes
	
	
	@FindBy(id = "mensajeExito")
	public WebElementFacade LblconfirmacionExito;
	
	public void NumeroPoliza(String data) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='mytable']/tbody/tr/td[3]/div/input")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		txtNumeroPoliza.sendKeys(data);
	}
	
	public void Comentario(String data) {
		txtComentario.sendKeys(data);
	}
	
	public void EnviarPlanes() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		btnGuardar.click();
	}
	
	public void Confirmar() {
		btnConfirmarEnvioPoliza.click();
	}
	
	public void Aceptar() {
		btnAceptarPolizaCreada.click();
	}

	public void validacion() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mensajeConfirmacion")));
		
		String strMensaje = Lblconfirmacion.getText();
		assertThat(strMensaje, containsString("Se enviará el trámite a configurar los planes"));
		
		Confirmar();		
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mensajeExito")));
		
		strMensaje = LblconfirmacionExito.getText();
		assertThat(strMensaje, containsString("Se ha enviado el trámite al área de configuración de planes"));
		
		Aceptar();
		
	}
	
	
}
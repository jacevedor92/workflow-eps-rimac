package com.choucair.formacion.pageobjects;

/**
 * Estructura básica de clase para Object BusquedaTramitePage
 *
 */

import java.util.List;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("")
public class BusquedaTramitePage extends PageObject {
	
	@FindBy(xpath = "//*[@id=\"headingOne\"]/h4/a")
	public WebElementFacade cmbBuscador;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[1]/div/input")
	public WebElementFacade txtNumeroTramite;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[16]/button[2]")
	public WebElementFacade btnBuscar;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div/div[4]/div/div/div/div[1]/div/a[2]/img")
	public WebElementFacade btnTramiteRenovacion;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div/div[4]/div/div/div/div[1]/div/a[1]/img")
	public WebElementFacade btnTramite;
	
	public void Buscador() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='headingOne']/h4/a")));		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		cmbBuscador.click();		
	}
	
	public void NumeroTramite() {
		//String codTramite = Serenity.sessionVariableCalled("codTramite").toString(); 		
		txtNumeroTramite.click();
		
		 String CodTramite= Serenity.sessionVariableCalled("codTramite").toString(); 
		txtNumeroTramite.sendKeys(CodTramite);		
	}
	
	public void Buscar() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='collapseOne']/div/div/div/div[16]/button[2]")));
		
		btnBuscar.click();		
	}
	
	public void Tramite() {
		btnTramite.click();		
	}
	
	public void TramiteRenovacion() {
		btnTramiteRenovacion.click();		
	}
	
	
	
}
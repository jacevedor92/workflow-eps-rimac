package com.choucair.formacion.pageobjects;

import java.io.File;
import java.io.IOException;

/**
 * Estructura básica de clase para Object ConfiguracionPlanesPage
 *
 */

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("")
public class ConfiguracionPlanesPage extends PageObject {
	
	@FindBy(xpath = "//*[@id=\"tinymce\"]/p")
	public WebElementFacade txtPlanesCreados;
	
	@FindBy(xpath = "//*[@id=\"tinymce\"]/p")
	public WebElementFacade txtComentariosPlanes;
	
	@FindBy(xpath = "//*[@id=\"step-1\"]/div[5]/div/button[2]")
	public WebElementFacade btnEnviarPlanes;
	
	@FindBy(xpath ="//*[@id='step-1']/div[5]/div/button[1]")
	public WebElementFacade btnDevolver;
	
	@FindBy(xpath = "//*[@id=\"modalMensajeConfirmacion\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnConfirmarEnviarPlanes;
	
	@FindBy(xpath = "//*[@id=\"modalMensajeExito\"]/div/div/div[2]/div[3]/button")
	public WebElementFacade btnAceptarPlanesEnviados;
	
	@FindBy(xpath = "//*[@id=\"modalMensajeConfirmacionxTipo\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnConfirmarDevolver;
	
	@FindBy(xpath ="//*[@id=\"modalPopupDevolver\"]/div/div/div[2]/div[2]/button[2]")
	public WebElementFacade btnAceptarDevolver;
	
	@FindBy(id = "mensajeConfirmacionTipo")
	public WebElementFacade Lblconfirmacion;
	//Se enviará el trámite a configurar los planes
	
	
	@FindBy(id = "mensajeExito")
	public WebElementFacade LblconfirmacionExito;
	

	@FindBy(id = "//*[@id='s4-bodyContainer']/div[2]/div/div/div[1]/div[1]/div[7]/div/div/div/div[2]/dl/dt")
	public WebElementFacade LblCrearPoliza;
	
	@FindBy(id = "//*[@id='s4-bodyContainer']/div[2]/div/div/div[1]/div[1]/div[7]/div/div/div/div[2]/dl/dt")
	public WebElementFacade txtPlanesRenovacion;
	
	@FindBy(xpath = "//*[@id=\"step-1\"]/div[3]/div/button")
	public WebElementFacade btnEnviarPlanesRenovacion;

	
	public void Planes(String data) {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("collapseTen")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		getDriver().switchTo().frame(getDriver().findElement(By.xpath("//div[@id='root']/div/div/div[2]/div/div/div[1]/div/div[7]/div/div[1]/div/div/div[2]/div/div/div/div[4]/div[1]/div/div[2]/iframe")));
		getDriver().findElement(By.id("tinymce")).sendKeys(data);
		getDriver().switchTo().defaultContent();
	}
	
	public void PlanesRenovacion(String data) {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("s4-workspace")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		 getDriver().switchTo().frame(1);
		 getDriver().findElement(By.id("tinymce")).sendKeys(data);
		 getDriver().switchTo().defaultContent();
		 //txtPlanesCreados.sendKeys(data);
	}
	
	public void ComentariosPlanes(String data) {
		getDriver().switchTo().frame(getDriver().findElement(By.id("tiny-tiny-react_46080805431545855709199_ifr")));
			//txtComentariosPlanes.click();
			txtComentariosPlanes.sendKeys(data);
	}
	
	public void EnviarPlanes() {
		btnEnviarPlanes.sendKeys("");
		btnEnviarPlanes.click();
	}
	
	public void Confirmar() {
		btnConfirmarEnviarPlanes.click();
	}
	
	public void Aceptar() {
		btnAceptarPlanesEnviados.click();
	}
	
	public void AceptarDevolver() {
		btnAceptarDevolver.click();
	}

	public void ConfirmarDevolver() {
		btnConfirmarDevolver.click();
	}
	
	public void validacion() {

		Confirmar();
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mensajeExito")));
		
		String strMensaje = LblconfirmacionExito.getText();
		assertThat(strMensaje, containsString("comunicado interno"));
		
		Aceptar();
		
	}
	

	public void validacionDevolver(String motivo, String path) throws IOException {

				
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalPopupDevolver")));
		
		getDriver().switchTo().parentFrame();
		getDriver().switchTo().frame(2);
		getDriver().findElement(By.id("tinymce")).sendKeys(motivo);
		getDriver().switchTo().defaultContent();
		
		String ruta = new File(".").getCanonicalPath();		
		
		ruta = ruta + "\\src\\test\\resources\\Files\\" + path;		
		
		WebElement uploadElement = getDriver().findElement(By.xpath("//div[@id='modalPopupDevolver']/div/div/div[2]/div[1]/div/div[2]/label/input"));
		uploadElement.sendKeys(ruta);
		
		AceptarDevolver();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalMensajeConfirmacionxTipo")));
		
		ConfirmarDevolver();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalMensajeExito")));
		
		
		String strMensaje = LblconfirmacionExito.getText();
		assertThat(strMensaje, containsString("Se ha devuelto el trámite al analista de suscripción"));
		
		Aceptar();

	}

	public void devolver() {
		btnDevolver.click();
		
	}

	public void EnviarPlanesRenovacion() {
		
		btnEnviarPlanesRenovacion.click();
	}
	
	
	
}
package com.choucair.formacion.pageobjects;

import java.io.File;
import java.io.IOException;

/**
 * Estructura básica de clase para Object RnSolicitarProspectoPage
 *
 */

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("")
public class RnSolicitarProspectoPage extends PageObject {
	
	@FindBy(xpath = "//div[@id='collapseSix']/div/div[1]/div/div[1]/div/label/input")
	public WebElementFacade txtPlanVigente;
	
	@FindBy(xpath = "//button[@data-id='cmbRangoPrimaProyectada']")
	public WebElementFacade cmbRangoPrimaProyecta;
	
	@FindBy(xpath = "//*[@id=\"collapseSix\"]/div/div[1]/div/div[5]/div/input")
	public WebElementFacade txtSiniestralidadProyectada;
	
	@FindBy(xpath = "//*[@id=\"tinymce\"]")
	public WebElementFacade txtCambiosCondiciones;	
	
//	@FindBy(xpath = "//*[@id=\"collapseSix\"]/div/div[2]/div/button[2]")
//	public WebElementFacade btnSolicitarProspecto;
	
	@FindBy(xpath = "//*[@id=\"step-1\"]/div[2]/div/button[2]")
	public WebElementFacade btnSolicitarProspecto;
	
	@FindBy(xpath ="//*[@id=\"modalMensajeConfirmacion\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnConfirmar;
	
	@FindBy(xpath ="//*[@id=\"modalMensajeExito\"]/div/div/div[2]/div[3]/button")
	public WebElementFacade btnAceptarConfirmacion;
	
	@FindBy(id ="mensajeExito")
	public WebElementFacade lblMensajeExito;
	
	public void PlanVigente(String path) throws IOException{		
				
		String ruta = new File(".").getCanonicalPath();		
		
		ruta = ruta + "\\src\\test\\resources\\Files\\" + path;		
		
		WebElement uploadElement = getDriver().findElement(By.xpath("//div[@id='collapseSix']/div/div[1]/div/div[1]/div/label/input"));
		uploadElement.sendKeys(ruta);		
	}
	
	public void RangoPrimaProyectada(String data) {
		
		cmbRangoPrimaProyecta.click();
		
		String Elementxpath = "//ul[@class='dropdown-menu inner']/li/a/span[text()='"+data+"']";

		WebElement elemento = getDriver().findElement(By.xpath(Elementxpath));	
		
		elemento.click();	
		
	}
	
	public void SiniestralidadProyectada(String data) {
		txtSiniestralidadProyectada.sendKeys(data);
	}
	
	public void SolicitarProspecto() {
		btnSolicitarProspecto.click();
	}
	
	
	public void CambiosCondiciones(String data) {
		
//		 getDriver().switchTo().frame(getDriver().findElement(By.xpath("//div[@id='modalPopupIniciaConcurso']/div/div/div[2]/div[1]/div/div[4]/div/div[1]/div[1]/div/div[2]/iframe")));
//		 getDriver().findElement(By.id("tinymce")).sendKeys(data);
//		 getDriver().switchTo().defaultContent();
//		 
		getDriver().switchTo().parentFrame();
		getDriver().switchTo().frame(1);
		getDriver().findElement(By.id("tinymce")).sendKeys(data);
		getDriver().switchTo().defaultContent();
		
	}

	public void validacion(String data) {
		
		int siniestralidad = Integer.parseInt(data);		
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		
		btnConfirmar.click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mensajeExito")));
		
		String strMensaje = lblMensajeExito.getText();
		
		if (siniestralidad<=70) {
			assertThat(strMensaje, containsString("Se ha enviado la solicitud de prospecto"));
		}else {
			assertThat(strMensaje, containsString("Se ha enviado el trámite para su aprobación"));
		}
		
//		assertEquals(strMensaje, containsString("Se ha enviado la solicitud de prospecto"));
		btnAceptarConfirmacion.click();
	}
	
	
}
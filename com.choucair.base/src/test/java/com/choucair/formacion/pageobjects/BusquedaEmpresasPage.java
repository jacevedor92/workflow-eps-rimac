package com.choucair.formacion.pageobjects;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("https://rimacsegurosperu.sharepoint.com/sites/WorkflowSalud")
public class BusquedaEmpresasPage extends PageObject {
	
	@FindBy(xpath = "//*[@id='headingOne']/h4/a")
	public WebElementFacade slctBuscador;
	
	@FindBy(xpath = "//*[@id='collapseOne']/div/div/div/div[1]/div/input")
	public WebElementFacade txtRuc;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[2]/div/input")
	public WebElementFacade txtRazonSocial;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[3]/div/div/button")
	public WebElementFacade cmbGrupoEconomico;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[4]/div/div/button")
	public WebElementFacade cmbBroker;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[5]/div/div/button")
	public WebElementFacade cmbTipoEntidadPredecesora;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[6]/div/div/button")
	public WebElementFacade cmbCategoria;
	
	@FindBy(xpath = "//*[@id=\"datetimepicker1\"]/div/div[1]/div/input")
	public WebElementFacade txtFechaDesde;
	
	@FindBy(xpath = "//*[@id=\"datetimepicker2\"]/div/div/div/input")
	public WebElementFacade txtFechaHasta;
	
	@FindBy(xpath = "//*[@id=\"datetimepicker2\"]/div/div/div/input")
	public WebElementFacade btnBuscar;
	
	@FindBy(xpath = "//*[@id=\"datetimepicker2\"]/div/div/div/input")
	public WebElementFacade btnLimpiar;
	
	//registros empresas
	
	////////////////////////////////////////////////
	
	@FindBy(xpath = "//form/div/div/div/span/div/div/div/div/div/div/div/div/div/div[@class='col-lg-12 col-md-12 ng-scope']/div/div/div")
	public List<WebElement> regEmpresa;
	
	@FindBy(xpath = "//form/div/div/div/span/div/div/div/div/div/div/div/div/div/div[@class='col-lg-12 col-md-12 ng-scope']/div/div/div/div[@class='colnew9']/dl/dt")
	public List<WebElement> regEmpresaCampos;

	////////////////////////////////////////////////
	
	@FindBy(xpath = "//form/div/div/div/span/div/div/div/div/div/div/div/div/div/div[@class='col-lg-12 col-md-12 ng-scope']/div/div/div/div[@class='colnew9']/dl/dd/span")
	public List<WebElement> lblRucEmpresas;
	
	@FindBy(xpath = "//form/div/div/div/span/div/div/div/div/div/div/div/div/div/div[@class='col-lg-12 col-md-12 ng-scope']/div/div/div/div[@class='colnew9']/dl/dt")
	public List<WebElement> lblNombreEmpresas;
	
	@FindBy(xpath = "//form/div/div/div/span/div/div/div/div/div/div/div/div/div/div[@class='col-lg-12 col-md-12 ng-scope']/div/div/div/div[@class='colnew9']/dl/dt")
	public List<WebElement> lblNombreBroker;
	
	@FindBy(xpath = "//form/div/div/div/span/div/div/div/div/div/div/div/div/div/div[@class='col-lg-12 col-md-12 ng-scope']/div/div/div/div[@class='colnew9']/dl/dt")
	public List<WebElement> lblNombreTipoEntidad;
	
	@FindBy(xpath = "//form/div/div/div/span/div/div/div/div/div/div/div/div/div/div[@class='col-lg-12 col-md-12 ng-scope']/div/div/div/div[@class='colnew9']/dl/dt")
	public List<WebElement> lblNombreTipoCategoria;
	
	@FindBy(xpath = "//form/div/div/div/span/div/div/div/div/div/div/div/div/div/div[@class='col-lg-12 col-md-12 ng-scope']/div/div/div/div[@class='colnew9']/dl/dt")
	public List<WebElement> lblNombreTipoFechaActualizacion;
	
	//xpath contenedor registro busqueda empresas
	
	//xpath base registro empresas
	//form/div/div/div/span/div/div/div/div/div/div/div/div/div/div[@class='col-lg-12 col-md-12 ng-scope']/div/div/div/div[@class='colnew9']/dl
	
	
	//campo buscador
	public void Buscador() {
		slctBuscador.click();
	}
	
	//rUC
	public void Ruc(String text) {
		txtRuc.click();
		txtRuc.sendKeys(text);		
	}
	
	//razon social
	public void RazonSocial(String text) {
		txtRazonSocial.click();
		txtRazonSocial.sendKeys(text);		
	}
	
	//razon social
	public void GrupoEconomico(String text) {
		cmbGrupoEconomico.selectByVisibleText(text);
	}
	
	//razon social
	public void Broker(String text) {
		cmbBroker.selectByVisibleText(text);
	}
	
	//razon social
	public void TipoEntidadPredecesora(String text) {
		cmbTipoEntidadPredecesora.selectByVisibleText(text);
	}
	//razon social
	public void Categoria(String text) {
		cmbCategoria.selectByVisibleText(text);
	}
		
	//Fecha desde actualization 
	public void FechaActualizacionDesde(String text) {
		cmbGrupoEconomico.selectByVisibleText(text);
	}
		
	//Fecha hasta actualizacion 
	public void FechaActualizacionHasta(String text) {
		cmbGrupoEconomico.selectByVisibleText(text);
	}

	//Fecha hasta actualizacion 
	public void Buscar() {
		cmbGrupoEconomico.click();
	}
	
	public void Limpiar() {
		cmbGrupoEconomico.click();
	}
	
	
	public void VerificarBusqueda() {
		
	}

	
	
	
}
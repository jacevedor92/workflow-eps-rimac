package com.choucair.formacion.pageobjects;

import java.io.File;
import java.io.IOException;

/**
 * Estructura básica de clase para Object DetalleTramitePage
 *
 */

import java.util.List;

import javax.validation.constraints.AssertFalse;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("")
public class DetalleTramitePage extends PageObject {
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[1]/div[1]/div[1]/a")
	public WebElementFacade btnCambiarPrioridad;	
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[1]/div[1]/div[2]/a")
	public WebElementFacade btnCerrarTramite;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[5]/div/ul/li[1]/form/a/img")
	public WebElementFacade imgHistorialActividadesCotizacion;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[5]/div/ul/li[2]/form/a/img")
	public WebElementFacade imgHistorialActividadesConcurso;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[5]/div/ul/li[3]/form/a/img")
	public WebElementFacade imgHistorialActividadesImplementacion;
	
	@FindBy(xpath = "//*[@id=\"modalPopupHistorialActivades\"]/div/div/div[2]/div[2]/button")
	public WebElementFacade btnCerrarHistorialActividades;
	
	
	//ojito  de actividad pendiente !!!!!!
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[7]/div/div/div/div[1]/div/a/img")
	public WebElementFacade btnActividadPendiente;
	
	//actividad pendiente trama afiliados
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[8]/div/div/div/div[1]/div/a/img")
	public WebElementFacade btnActividadPendienteTramaAfiliados;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[9]/div/div/div/div[1]/div/a/img")
	public WebElementFacade btnActividadPendienteTres;
	
//	//Creacion de poliza con permiso de colaborar
//	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[7]/div/div/div/div[1]/div/a/img")
//	public WebElementFacade btnActividadPendienteCreacionPoliza ;
//	
//	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[7]/div/div/div/div[1]/div/a/img")
//	public WebElementFacade btnActividadPendienteComunicadoCuenta ;
//			
//	//actividad pendiente configurar planes
//	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[7]/div/div/div/div[1]/div/a/img")
//	public WebElementFacade btnActividadPendienteConfigurarPlanes ;
//	
//	//Registrar afiliados
//	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[7]/div/div/div/div[1]/div/a/img")
//	public WebElementFacade btnActividadRegistroAfiliados;
//	
		
	//iniciar concurso
	@FindBy(xpath = "//*[@id=\"collapseSix\"]/div/div[2]/div/button[2]")
	public WebElementFacade btnIniciarConsurso;
	
	
	@FindBy(xpath = "//*[@id=\"step-1\"]/div[3]/div/button[2]")
	public WebElementFacade btnEnviarPropuestaCargarPlanes;
	
	
	@FindBy(xpath = "//*[@id=\"step-1\"]/div[2]/div/button[2]")
	public WebElementFacade btnEnviarNumeroProspecto;
	
	@FindBy(xpath = "//*[@id=\"collapseSix\"]/div/div[2]/div/button[3]")
	public WebElementFacade btnAdjudicacionDirecta;
	
	//*[@id="collapseSix"]/div/div[2]/div/button[2]
	//cotizacion Rechazada
	@FindBy(xpath = "//*[@id=\"collapseSix\"]/div/div[2]/div/button[1]")
	public WebElementFacade btnCotizacionRechazada;
	
	//cotizacion Rechazada
	@FindBy(xpath = "//*[@id=\"modalPopupCotizacionRechazada\"]/div/div/div[2]/div[2]/button[2]")
	public WebElementFacade btnAceptarCotizacionRechazada;
	
	
	
	//agregar archivos inicio de concurso regular
	@FindBy(xpath = "//div[@id='modalPopupInicioConcurso']/div/div/div[2]/div[1]/div/div/div[1]/div/label/input")
	public WebElementFacade txtAdjuntarInicioConcurso;
	
	//Aceptar Iniciar Concurso
	@FindBy(xpath = "//*[@id=\"modalPopupInicioConcurso\"]/div/div/div[2]/div[2]/button[2]")
	public WebElementFacade btnAceptarIniciarConcurso;
	
	//confirmar inicio de concurso regular
	//btnConfirmarSolicitudRespuesta;
	//btnConfirmarConcursoGanado
	@FindBy(xpath = "//*[@id=\"modalPopupInicioConcurso\"]/div/div/div[2]/div[2]/button[2]")
	public WebElementFacade btnAceptarConfirmacion;
	
	@FindBy(xpath = "//*[@id=\"modalMensajeConfirmacionTipo\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnGanadoSi;
	
	//aceptar
	//aceptar alerta documentacion pre implementacion
	//btnAceptarSolicitudRespuesta
	@FindBy(xpath = "//*[@id=\"modalMensajeExito\"]/div/div/div[2]/div[3]/button")
	public WebElementFacade btnAceptar;
	
	@FindBy(xpath = "//*[@id=\"modalMensajeExito\"]/div/div/div[2]/div[3]/button")
	public WebElementFacade btnAceptarTrmiteFin;
	
	
	
	@FindBy(xpath = "//*[@id=\"modalMensajeConfirmacionTipo\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnConfirmacionSi;
	
	
	
	@FindBy(xpath = "//*[@id=\"modalMensajeConfirmacionTipo\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnSiCotizacionRechazada;
	
	//Resultado del concurso ===================
	@FindBy(xpath = "//*[@id=\"dash_1\"]/div[3]/div/div/button")
	public WebElementFacade btnResultadoConcurso;
	
	@FindBy(xpath = "//*[@id=\"dash_1\"]/div[3]/div/div/ul/li[2]/a")
	public WebElementFacade btnGanado;
	
	@FindBy(xpath = "//*[@id=\"dash_1\"]/div[3]/div/div/ul/li[1]/a")
	public WebElementFacade btnNoGanado;
	
	
	@FindBy(xpath = "//*[@id=\"dash_1\"]/div[3]/div/button")
	public WebElementFacade btnSolicitarRespuesta;
	
	
	
	//Solicitar  Resultado de concurso =============
	@FindBy(xpath = "//*[@id=\"dash_1\"]/div[3]/div/button")
	public WebElementFacade btnSolicitarRespuestas;
	
//	@FindBy(xpath = "//*[@id=\"modalMensajeConfirmacionTipo\"]/div/div/div[2]/div[3]/button[2]")
//	public WebElementFacade btnConfirmarSolicitudRespuesta;
	
//	@FindBy(xpath = "//*[@id=\"modalMensajeExito\"]/div/div/div[2]/div[3]/button")
//	public WebElementFacade btnAceptarSolicitudRespuesta;
	
//	//confirmar concurso ganado
//	@FindBy(xpath = "//*[@id=\"modalMensajeConfirmacionTipo\"]/div/div/div[2]/div[3]/button[2]")
//	public WebElementFacade btnConfirmarConcursoGanado;
	
//	//aceptar alerta documentacion pre implementacion
//	@FindBy(xpath = "//*[@id=\"modalMensajeExito\"]/div/div/div[2]/div[3]/button")
//	public WebElementFacade btnAceptarConcursoGanado;
	
	
	//Finalizar proceso
	@FindBy(xpath = "//*[@id=\"step-1\"]/div[4]/div/button")
	public WebElementFacade btnFin;
	
	//Finalizar proceso
	@FindBy(xpath = "//*[@id=\"modalMensajeConfirmacion\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnFinSi;
	
	
//	@FindBy(xpath = "//*[@id=\"modalMensajeConfirmacion\"]/div/div/div[2]/div[3]/button[2]")
//	public WebElementFacade btnAceptarSi; // revisar 
	
	// ckeckbox Resultado:(*) en registrar afiliados
	@FindBy(xpath = "//*[@id=\"collapseTres\"]/div/div/div/div[1]/div[2]/dl/dt/label[1]")
	public WebElementFacade ckbCargaCompleta;
	
	
	
	//mensajes de conformacion para validaciones
	//Se procederá con el inicio del concurso regular. ¿Desea continuar?
	
	@FindBy(id = "mensajeConfirmacionTipo")
	public WebElementFacade Lblconfirmacion;
	//Se enviará el trámite a configurar los planes
	
	
	@FindBy(id = "mensajeExito")
	public WebElementFacade LblconfirmacionExito;
	//Se ha iniciado el concurso regular.
	//Se ha enviado la propuesta formal y se espera el ingreso del resultado del concurso.
	//Se requiere de información y documentación adicional para proceder con la implementación.
	//Se ha enviado el trámite al área de configuración de planes

	// ===============================metodos ===========================================
	
	
	
	
	//determinar responsable de actividad
	
	//rol actiivo
	@FindBy(xpath = "//*[@id=\"lista\"]/div/div[1]/div")
	public WebElementFacade lblUsuarioActivo;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[7]/div/div/div/div[3]/dl/dt")
	public WebElementFacade lblResponsableTarea;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[7]/div/div/div/div[2]/dl/dt")	
	public WebElementFacade lblactividadPendiente;
		
	@FindBy(xpath = "//*[@id=\"lista\"]/div/div[2]/a/img")
	public WebElementFacade btnLogin;
	
	@FindBy(xpath = "//*[@id=\"lista\"]/div/div[2]/ul/li/a/span")
	public WebElementFacade btnCerrarSesion;
	
	@FindBy(id = "otherTileText")
	public WebElementFacade btnUsarOtraCuenta;
	
	@FindBy(xpath = "//div[@class='placeholderContainer']/input")
	public WebElementFacade txtUserName;
	
	@FindBy(id = "idSIButton9")
	public WebElementFacade BtnContinuar;
	
	@FindBy(xpath = "//div[@class='placeholderContainer']/input")
	public WebElementFacade txtPassword;
	
//	@FindBy(id = "idSIButton9")
//	public WebElementFacade btnIniciarSesion;
//		
//	@FindBy(id = "idSIButton9")
//	public WebElementFacade btnSesionActiva;
	
	@FindBy(id = "KmsiCheckboxField")
	public WebElementFacade ckbManterActiva;
	
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[1]/div/div[1]/ul/li[3]/a/img")
	public WebElementFacade menuTramites;
	
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[1]/div/div[1]/ul/li[3]/ul/li[1]/a")
	public WebElementFacade submenuBusquedaTramites;
	
	
	@FindBy(xpath = "//*[@id=\"headingOne\"]/h4/a")
	public WebElementFacade cmbBuscador;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[1]/div/input")
	public WebElementFacade txtNumeroTramite;
	
	@FindBy(xpath = "//*[@id=\"collapseOne\"]/div/div/div/div[16]/button[2]")
	public WebElementFacade btnBuscar;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div/div[4]/div/div/div/div[1]/div/a[1]/img")
	public WebElementFacade btnTramite;
	
	@FindBy(xpath ="//*[@id=\"modalPopupConcursoNoGanado\"]/div/div/div[2]/div[2]/button[2]")
	public WebElementFacade btnAceptarConcursoNoganado;
	
	
	//renovacion ======================
	@FindBy(xpath ="//*[@id=\"step-1\"]/div[3]/div/button[2]")
	public WebElementFacade btnAprobarPropuestaPersonalizada;
	
	@FindBy(name ="NumeroProspecto")
	public WebElementFacade txtNumeroProspecto;
	
	@FindBy(xpath ="//*[@id=\"modalPopupEnviarNumeroProspecto\"]/div/div/div[2]/div[2]/button[2]")
	public WebElementFacade txtEnviarNumeroProspecto;

	//CargarPlanesPropuesta

	@FindBy(xpath ="//div[@id='step-1']/div[2]/div/div/div[2]/div/div[1]/div/div[4]/div/label/input")
	public WebElementFacade txtAgregarPlanVigente;
	
	@FindBy(xpath ="//div[@id='step-1']/div[2]/div/div/div[2]/div/div[1]/div/div[7]/div/label/input")
	public WebElementFacade txtAgregarFichaTecnica;


	@FindBy(xpath = "//*[@id=\"collapseTen\"]/div/div/div/div[2]/label[2]")
	public WebElementFacade ckbCambios;
	
	@FindBy(xpath = "//*[@id=\"step-1\"]/div[5]/div/button[3]")
	public WebElementFacade btnEnviarPropuestaRenovacion;
	
	@FindBy(xpath = "//*[@id=\"step-1\"]/div[2]/div/div/button")
	public WebElementFacade btnResultadoConcursoRenovacion;
	
	@FindBy(xpath = "//*[@id=\"step-1\"]/div[2]/div/div/ul/li[2]/a")
	public WebElementFacade btnConcursoGanadoRenovacion;
	
	@FindBy(xpath = "//*[@id=\"radiob\"]/label[1]")
	public WebElementFacade ckbCuentaAfiliadosSi;
	
	@FindBy(xpath = "//*[@id=\"radiob\"]/label[2]")
	public WebElementFacade ckbCuentaAfiliadosNo;
	
	@FindBy(xpath = "//*[@id=\"step-1\"]/div[3]/div/button[2]")
	public WebElementFacade btnFinalizarRenovacion;
	
	@FindBy(xpath = "//*[@id=\"modalPopupObservar\"]/div/div/div[2]/div[2]/button[2]")
	public WebElementFacade btnEnviarObservarPropuesta;
	
	@FindBy(xpath = "//*[@id=\"modalPopupReajusteMejora\"]/div/div/div[2]/div[2]/button[2]")
	public WebElementFacade btnEnviarReajuste;
	
	
	@FindBy(xpath = "//*[@id=\"step-1\"]/div[3]/div/button[1]")
	public WebElementFacade btnobservarPropuesta;
	

	@FindBy(xpath = "//*[@id=\"step-1\"]/div[5]/div/button[2]")
	public WebElementFacade btnSolicitarReajusteEnviarPropuesta;
	
	@FindBy(xpath = "//*[@id=\"step-1\"]/div[2]/div/button[2]")
	public WebElementFacade btnSolicitarRespuestaIngresarRespuestaConcurso;

	
	public void CambiarPrioridad() {
		btnCambiarPrioridad.click();
	}
	
	public void CerrarTramite() {
		btnCerrarTramite.click();
	}
	
	public void HistorialCotizacion() {
		imgHistorialActividadesCotizacion.click();
	}
	
	public void HistorialConcurso() {
		imgHistorialActividadesConcurso.click();
	}
	
	public void HistorialImplementacion() {
		imgHistorialActividadesImplementacion.click();
	}
	
	public void CerrarHistorial() {
		btnCerrarHistorialActividades.click();
	}
		
	
	//Actividades pendientes 
	// revisar se pueden reutilizar algunos webelements
	
	public void ActividadPendiente() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='s4-bodyContainer']/div[2]/div/div/div[1]/div[1]/div[7]/div/div/div/div[1]/div/a/img")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		btnActividadPendiente.click();
	}
	
	public void ActividadPendienteTramaAfiliados() {
		int size = getDriver().findElements(By.xpath("//*[@id='s4-bodyContainer']/div[2]/div/div/div[1]/div[1]/div[8]/div/div/div/div[1]/div/a/img")).size();
		
		if (size>0) {
			btnActividadPendienteTramaAfiliados.click();
		}else {
			btnActividadPendiente.click();
		}
		
	}
	
	public void ActividadPendienteTres() {
		btnActividadPendienteTres.click();
	}
	
	public void IniciarConcurso() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 15);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='collapseSix']/div/div[2]/div/button[2]")));		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		btnIniciarConsurso.click();
	}
	
	public void AdjudicacionDirecta() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='collapseSix']/div/div[2]/div/button[3]")));		
		btnAdjudicacionDirecta.click();
	}
	
	public void CotizacionRechazada() {
		btnCotizacionRechazada.click();
	}
	
	public void ComentarioCotizacionRechazada(String data) {
		getDriver().switchTo().parentFrame();
		getDriver().switchTo().frame(1);
		
		getDriver().findElement(By.id("tinymce")).sendKeys(data);
		getDriver().switchTo().defaultContent();
		//txtCondicionesBeneficios.sendKeys(data);
	}
	
	
	
	public void AceptarCotizacionRechazada() {
		btnAceptarCotizacionRechazada.click();
	}	
	
	public void AdjuntatInicioConcurso(String path) throws IOException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalPopupInicioConcurso")));		
		
		String ruta = new File(".").getCanonicalPath();		
		
		ruta = ruta + "\\src\\test\\resources\\Files\\" + path;		
		
		WebElement uploadElement = getDriver().findElement(By.xpath("//div[@id='modalPopupInicioConcurso']/div/div/div[2]/div[1]/div/div/div[1]/div/label/input"));
		 uploadElement.sendKeys(ruta);		
	}
	

	public void AjuntarCotizacionRechazada(String path) throws IOException {
		String ruta = new File(".").getCanonicalPath();		
		
		ruta = ruta + "\\src\\test\\resources\\Files\\" + path;		
		
		WebElement uploadElement = getDriver().findElement(By.xpath("//div[@id='modalPopupCotizacionRechazada']/div/div/div[2]/div/div/div[2]/label/input"));
		 uploadElement.sendKeys(path);		
	}
	
	public void AceptarIniciarConcurso() {
		btnAceptarIniciarConcurso.click();
	}
	
	public void AceptarConfirmacion() {
		btnAceptarConfirmacion.click();
		btnConfirmacionSi.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Aceptar();
	}
	
	
	public void Aceptar() {
		btnAceptar.click();
	}
	
	public void AceptarTramiteFin() {
		btnAceptarTrmiteFin.click();
	}
	
	
	

	public void ResultadoConcurso() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='dash_1']/div[3]/div/div/button")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));		
		btnResultadoConcurso.click();
	}
	
	public void ConfirmarCierreTramite() {
		btnSiCotizacionRechazada.click();
	}
	
	
	
	public void ConcursoGanado() {
		btnGanado.click();
	}
	
	public void ConcursoNoGanado() {
		btnNoGanado.click();
	}
	
	public void ConfirmacionConcursoGanado() {
		btnGanadoSi.click();
	}
	
	
	public void ConcursoPerdido() {
		btnNoGanado.click();
	}
	

	public void SolicitarRespuesta() {
		btnSolicitarRespuesta.click();
	}

	public void CargaCompleta() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("collapseDatostramas")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		String Xp = "//*[@id=\"collapseTres\"]/div/div/div/div[1]/div[2]/dl/dt/label[1]";
		if (getDriver().findElements(By.xpath(Xp)).size()!=0) {
			
			ckbCargaCompleta.click();			
		}
		
	}
	
	public void Finalizar() {
	
		btnFin.click();
	}
	
	public void FinalizarRenovacion() {
		
		btnFinalizarRenovacion.click();
	}
	
	public void FinalizarSi() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mensajeConfirmacion")));
		btnFinSi.click();
	}

	public void validarRespuestaCotizacion() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalPopupInicioConcurso")));
		
		btnAceptarConfirmacion.click();
		
		String strMensaje = Lblconfirmacion.getText();
		assertThat(strMensaje, containsString("concurso regular"));
		
		btnConfirmacionSi.click();		
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mensajeExito")));
		
		strMensaje = LblconfirmacionExito.getText();
		assertThat(strMensaje, containsString("Se ha iniciado el concurso regular"));			
		
		Aceptar();
		
	}
	
	public void validarRespuestaCotizacionPyme() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalMensajeConfirmacionTipo")));
		
		btnGanadoSi.click();
				
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalMensajeExito")));
				
		String strMensaje = LblconfirmacionExito.getText();
		assertThat(strMensaje, containsString("Se ha iniciado el concurso PYME"));			
	
		Aceptar();
		
	}

	public void validacionResultadoConcurso() {
						
		WebDriverWait wait = new WebDriverWait(getDriver(), 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mensajeConfirmacionTipo")));
		
		String strMensaje = Lblconfirmacion.getText();
		assertThat(strMensaje, containsString("concurso ganado"));			
		
		ConfirmacionConcursoGanado();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mensajeExito")));
		
		strMensaje = LblconfirmacionExito.getText();
		assertThat(strMensaje, containsString("Se requiere de información y documentación adicional"));		
		
		Aceptar();
		
	}
	
	public void validacionResultadoConcursoNoGanado(String path,String comentario) throws IOException {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalPopupConcursoNoGanado")));
		
		String ruta = new File(".").getCanonicalPath();		
		
		ruta = ruta + "\\src\\test\\resources\\Files\\" + path;		
		
		WebElement uploadElement = getDriver().findElement(By.xpath("//div[@id='modalPopupConcursoNoGanado']/div/div/div[2]/div[1]/div/div/div[1]/div/label/input"));
		uploadElement.sendKeys(ruta);	
				
		getDriver().switchTo().parentFrame();
		getDriver().switchTo().frame(1);
		
		getDriver().findElement(By.id("tinymce")).sendKeys(comentario);
		getDriver().switchTo().defaultContent();
		
		btnAceptarConcursoNoganado.click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalMensajeConfirmacionTipo")));
		
		String strMensaje = Lblconfirmacion.getText();
		assertThat(strMensaje, containsString("Se cerrará el trámite como concurso no ganado"));			
		
		ConfirmacionConcursoGanado();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalMensajeExito")));
		
		strMensaje = LblconfirmacionExito.getText();
		assertThat(strMensaje, containsString("Se ha cerrado el trámite"));	
		
		Aceptar();
		
	}


	public void validarRegistroAfiliados() {
	
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mensajeExito")));
		
		String strMensaje = LblconfirmacionExito.getText();
		assertThat(strMensaje, containsString("Se ha registrado el resultado de cada trama con éxito"));		
				
		Aceptar();
		
	}

	public void DeterminarResponsable(String user, String pass) {
		
		String Responsable = lblResponsableTarea.getText();
		String UsuarioActivo = lblUsuarioActivo.getText();
		if (!Responsable.equals(UsuarioActivo)) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			btnLogin.click();
			btnCerrarSesion.click();
			
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			btnUsarOtraCuenta.click();
			
			txtUserName.sendKeys(user);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			BtnContinuar.click();
			
			  try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			txtPassword.sendKeys(pass);
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			BtnContinuar.submit();
			
			if (getDriver().findElements(By.id("idSIButton9")).size()!=0) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				BtnContinuar.submit();			
			}
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			menuTramites.click();
			submenuBusquedaTramites.click();
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			cmbBuscador.click();	
			txtNumeroTramite.click();
			
			String CodTramite= Serenity.sessionVariableCalled("codTramite").toString(); 
			txtNumeroTramite.sendKeys(CodTramite);	
			
			btnBuscar.click();	
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			btnTramite.click();		
			
			
			
		}
		
		
	}

	public void validarCotizacionRechazada() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalMensajeExito")));
		
		String strMensaje = LblconfirmacionExito.getText();
		assertThat(strMensaje, containsString("Se ha finalizado el trámite"));			
		
		Aceptar();
		
	}
	
	public boolean isElementPresent(By locatorKey) {
	    try {
	        getDriver().findElement(locatorKey);
	        return true;
	    } catch (org.openqa.selenium.NoSuchElementException e) {
	        return false;
	    }
	}

	public void validacionSolicitarRespuesta() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalMensajeExito")));
		
		String strMensaje = LblconfirmacionExito.getText();
		assertThat(strMensaje, containsString("Se envió el correo electrónico al cliente/bróker"));	
		
		Aceptar();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
		Boolean present = false;
		
		String xp = "//*[@style='display: none;' and @class='btn btn-default rippledefault ml5 mb5 mt5 btn-w110']";
		
		if (getDriver().findElements(By.xpath(xp)).size()==0) {
			 present = true;
		}		
		
		assertFalse(present);
		
	}

	public void EsperarActividadIngresarRespuestaPropuesta() {
		
		String responsable = lblactividadPendiente.getText();
		
		while (responsable.equals("Enviar carta de renovación")) {
			try {
				Thread.sleep(30000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			getDriver().navigate().refresh();
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			responsable = lblactividadPendiente.getText();
		}
	}
	
	public void AprobarPropuestaPersonalizada() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='collapseSix']/div/div[2]/div/button[2]")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		btnAprobarPropuestaPersonalizada.click();
	}	
	
	public void NumeroProspecto(String data) {
		txtNumeroProspecto.sendKeys(data);
		
	}
	
	public void EnviarNumeroProspecto() {
		txtEnviarNumeroProspecto.click();
		
	}

	public void validar(String string) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalMensajeExito")));
		
		String strMensaje = LblconfirmacionExito.getText();
		assertThat(strMensaje, containsString(string));
		
		btnAceptar.click();
		
	}

	public void AdjuntarPlanVigente(String path) throws IOException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("accordion")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		String ruta = new File(".").getCanonicalPath();		
		ruta = ruta + "\\src\\test\\resources\\Files\\" + path;		
		
		WebElement uploadElement = getDriver().findElement(By.xpath("//div[@id='step-1']/div[2]/div/div/div[2]/div/div[1]/div/div[4]/div/label/input"));
		 uploadElement.sendKeys(ruta);		
	}
	
	public void AdjuntarFichaTecnica(String path) throws IOException {
		
		String ruta = new File(".").getCanonicalPath();		
		ruta = ruta + "\\src\\test\\resources\\Files\\" + path;		
		
		WebElement uploadElement = getDriver().findElement(By.xpath("//div[@id='step-1']/div[2]/div/div/div[2]/div/div[1]/div/div[7]/div/label/input"));
		 uploadElement.sendKeys(ruta);		
	}
	
	public void CambiosCondiciones() { 
		WebDriverWait wait = new WebDriverWait(getDriver(), 60);		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		ckbCambios.click();	
	}
	
	public void EnviarPropuestaRenovacion() {
		
		btnEnviarPropuestaRenovacion.click();	
	}
	
	public void ResultadoConcursoRenovacion() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='step-1']/div[2]/div/div/button")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		btnResultadoConcursoRenovacion.click();	
	}
	
	public void ConcursoGanadoRenovacion() {
		
		btnConcursoGanadoRenovacion.click();	
	}

	public void CuentaConAfiliados() {
		ckbCuentaAfiliadosSi.click();
		
	}
	
	public void EnviarNumeroPrspectoRenovacion() {
		btnEnviarNumeroProspecto.click();
		
	}
	
	public void EnviarPropuestaCargarPlanes() {
		btnEnviarPropuestaCargarPlanes.click();
		
	}
	
	public void SolicitarReajusteEnviarPropuesta() {
		btnSolicitarReajusteEnviarPropuesta.click();
		
	}
	
	public void SolicitarRespuestaIngresarRespuestaConcurso() {
		btnSolicitarRespuestaIngresarRespuestaConcurso.click();
		
	}
	
	
	public void ObservarPropuestaRenovacion() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 25);		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		btnobservarPropuesta.click();
		
		getDriver().switchTo().parentFrame();
		getDriver().switchTo().frame(1);
		
		getDriver().findElement(By.id("tinymce")).sendKeys("OBSERVAR PROPUESTA DE RENOVACIÓN");
		getDriver().switchTo().defaultContent();
		
		btnEnviarObservarPropuesta.click();
		btnConfirmacionSi.click();
		
		validar("Se ha enviado su observación");
		
	}

	public void SolicitarReajuste() {
		
		WebDriverWait wait = new WebDriverWait(getDriver(), 15);		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		btnSolicitarReajusteEnviarPropuesta.click();
		
		getDriver().switchTo().frame(getDriver().findElement(By.xpath("//div[@id='modalPopupReajusteMejora']/div/div/div[2]/div[1]/div/div[1]/div[1]/div/div[2]/iframe")));
		
		getDriver().findElement(By.id("tinymce")).sendKeys("Solicitud de reajuste enviar propuesta");
		getDriver().switchTo().defaultContent();
		
		btnEnviarReajuste.click();
		btnConfirmacionSi.click();
		
		validar("Se ha enviado su solicitud");
		
		
		
	}
	
}
package com.choucair.formacion.pageobjects;

import java.util.List;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("")
public class MenuPrincipalPage extends PageObject {
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[1]/div/div[1]/ul/li[1]/a/img")
	public WebElementFacade menuInicio;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[1]/div/div[1]/ul/li[2]/a/img")
	public WebElementFacade menuEmpresas;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[1]/div/div[1]/ul/li[3]/a/img")
	public WebElementFacade menuTramites;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[1]/div/div[1]/ul/li[4]/a/imgs")
	public WebElementFacade menuAdministacion;	
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[1]/div/div[1]/ul/li[2]/ul/li/a")
	public WebElementFacade submenuBusquedaEmpresas;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[1]/div/div[1]/ul/li[3]/ul/li[1]/a")
	public WebElementFacade submenuBusquedaTramites;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[1]/div/div[1]/ul/li[3]/ul/li[1]/a")
	public WebElementFacade submenuBandejaPendiente;
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[1]/div/div[1]/ul/li[3]/ul/li[1]/a")
	public WebElementFacade submenuRegistrarCargos;
	
	@FindBy(xpath = "//*[@id=\"lista\"]/div/div[2]/a/img")
	public WebElementFacade btnLogin;
	
	@FindBy(xpath = "//*[@id=\"lista\"]/div/div[2]/ul/li/a/span")
	public WebElementFacade btnCerrarSesion;
	
	@FindBy(id = "otherTileText")
	public WebElementFacade btnUsarOtraCuenta;
	
	
	
	/* Campo <<Nombre WebElementFacade  */
	public void Inicio() {
		menuInicio.click();		
	}
	
	/* Campo <<Nombre WebElementFacade  */
	public void Empresas() {
		menuEmpresas.click();
	}
	
	/* Campo <<Nombre WebElementFacade  */
	public void Tramites() {		
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='s4-bodyContainer']/div[1]/div/div[1]/ul/li[3]/a/img")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		menuTramites.click();
	}
	
	
	
	/* Verificar campo en pantalla  */
	public void Administracion() {		
		menuAdministacion.click();
	}
	
	/* Verificar campo en pantalla  */
	public void BusquedaEmpresas() {
		submenuBusquedaEmpresas.click();
	}
	
	/* Verificar campo en pantalla  */
	public void BusquedaTramites() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='s4-bodyContainer']/div[1]/div/div[1]/ul/li[3]/ul/li[1]/a")));
		
		submenuBusquedaTramites.click();
	}
	
	/* Verificar campo en pantalla  */
	public void BandejaPendientes() {
		submenuBandejaPendiente.click();
	}
	
	/* Verificar campo en pantalla  */
	public void RegistrarCargos() {
		submenuRegistrarCargos.click();
	}
	
	public void CerrarSesion() {
//		try {
//			Thread.sleep(3000);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='lista']/div/div[2]/a/img")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		btnLogin.click();
		btnCerrarSesion.click();
	}
	
	public void UsarOtraCuenta() {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		btnUsarOtraCuenta.click();
	}
	
	
}
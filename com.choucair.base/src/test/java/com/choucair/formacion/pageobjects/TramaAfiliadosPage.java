package com.choucair.formacion.pageobjects;

import java.io.File;
import java.io.IOException;

/**
 * Estructura básica de clase para Object TramaAfiliadosPage
 *
 */

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.matchers.JUnitMatchers.*;

@DefaultUrl("")
public class TramaAfiliadosPage extends PageObject {
	
	@FindBy(id = "my-file-selector")
	public WebElementFacade txtAdjuntarTrama;
	
	@FindBy(xpath = "//*[@id=\"step-1\"]/div[2]/div/button[2]")
	public WebElementFacade btnEnviarTramaAfiliados;
	
	@FindBy(xpath = "//*[@id=\"modalMensajeConfirmacion\"]/div/div/div[2]/div[3]/button[2]")
	public WebElementFacade btnEnviarTramaSi;
	
	@FindBy(xpath = "//*[@id=\"modalMensajeExito\"]/div/div/div[2]/div[3]/button")
	public WebElementFacade btnAceptarTramaEnviada;
	
	@FindBy(id = "mensajeExito")
	public WebElementFacade LblconfirmacionExito;
	
	
	public void AdjuntarTrama(String data) throws IOException {
				
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"dash_2\"]/div[2]/div/div/div[1]/div/label[2]")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		String ruta = new File(".").getCanonicalPath();	
		
		ruta = ruta + "\\src\\test\\resources\\Files\\" + data;		
		
		WebElement uploadElement = getDriver().findElement(By.id("my-file-selector"));
		uploadElement.sendKeys(ruta);
	}
	
	public void Enviar() {
	
		btnEnviarTramaAfiliados.click();
	}
	

	public void Confirmacion() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalMensajeConfirmacion")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		btnEnviarTramaSi.click();
	}
	

	public void Aceptar() {
		btnAceptarTramaEnviada.click();
	}

	public void validacion() {
	
		WebDriverWait wait = new WebDriverWait(getDriver(), 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mensajeExito")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		String strMensaje = LblconfirmacionExito.getText();
		assertThat(strMensaje, containsString("Se ha registrado la(s) trama(s) con éxito"));		
				
		Aceptar();
		
	}
	
	
}
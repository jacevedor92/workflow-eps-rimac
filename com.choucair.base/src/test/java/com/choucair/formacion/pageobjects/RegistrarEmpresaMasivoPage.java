package com.choucair.formacion.pageobjects;

import java.io.File;
import java.io.IOException;

/**
 * Estructura básica de clase para Object RegistrarEmpresaMasivoPage
 *
 */

import java.util.List;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import net.thucydides.core.annotations.DefaultUrl;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("https://rimacsegurosperu.sharepoint.com/sites/WorkflowSalud/SitePages/AppRimac.aspx/Cliente/BusquedaCliente")
public class RegistrarEmpresaMasivoPage extends PageObject {
	
	@FindBy(xpath = "//*[@id=\"s4-bodyContainer\"]/div[2]/div/div/div[1]/div[1]/div[1]/div[1]/a")
	public WebElementFacade btnCargaMasivaEmpresa;
	
	@FindBy(xpath = "//*[@id=\"step-1\"]/div[3]/button[2]")
	public WebElementFacade btnContinuar;
	
	@FindBy(id = "my-file-selector")
	public WebElement btnAdjuntarArchivo;
	
	@FindBy(xpath = "//*[@id=\"step-2\"]/div[7]/button[3]")
	public WebElementFacade btnProcesar;
	
	@FindBy(xpath = "//*[@id=\"step-3\"]/div[1]/label")
	public WebElementFacade lblConfirmacion;
	
	@FindBy(xpath = "//*[@id=\"step-3\"]/div[3]/button")
	public WebElementFacade btnAceptar;
	
	
	public void CargaMasiva() {
		btnCargaMasivaEmpresa.click();
	}
	
	public void Continuar() {
		btnContinuar.click();
	}
	
	public void AdjuntarArchivo(String data) throws IOException {
		 
		String ruta = new File(".").getCanonicalPath();		
		ruta = ruta + "\\src\\test\\resources\\Files\\" + data;		
		
		WebElement uploadElement = getDriver().findElement(By.id("my-file-selector"));
		 uploadElement.sendKeys(ruta);
	}
	
	public void Procesar() {
		btnProcesar.click();
	}
	
	public void ValidacionEmpresaMasivo() {		
		String strMensaje = lblConfirmacion.getText();
		assertThat(strMensaje, containsString("El proceso de carga puede tardar algunos minutos"));
		btnAceptar.click();
	}
	
	
	
}
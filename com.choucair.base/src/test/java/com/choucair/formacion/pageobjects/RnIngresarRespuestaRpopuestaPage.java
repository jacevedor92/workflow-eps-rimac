package com.choucair.formacion.pageobjects;

import java.io.File;
import java.io.IOException;

/**
 * Estructura básica de clase para Object RnIngresarRespuestaRpopuestaPage
 *
 */

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("")
public class RnIngresarRespuestaRpopuestaPage extends PageObject {

	@FindBy(xpath = "//*[@id=\"headingFive\"]/h4/a")
	public WebElementFacade cmbOtrosDocumentos;
	
	@FindBy(xpath = "//*[@id=\"modalPopupIngresarRespuesta\"]/div/div/div[2]/div[1]/div/div[2]/label")
	public WebElementFacade ckbAceptaPropuestaRenovacion;
	
	@FindBy(xpath = "//*[@id=\"modalPopupIngresarRespuesta\"]/div/div/div[2]/div[1]/div/div[3]/label")
	public WebElementFacade ckbSolicitaAmpliacion;
	
	@FindBy(xpath = "//*[@id=\"modalPopupIngresarRespuesta\"]/div/div/div[2]/div[1]/div/div[4]/label")
	public WebElementFacade ckbIniciaConcurso;
	
	@FindBy(xpath = "//*[@id=\"modalPopupIngresarRespuesta\"]/div/div/div[2]/div[1]/div/div[4]/label")
	public WebElementFacade ckbAceptaPropuesta;
	
	@FindBy(xpath = "//*[@id=\"modalPopupIngresarRespuesta\"]/div/div/div[2]/div[1]/div/div[5]/label")
	public WebElementFacade ckbNoaceptaPropuesta;
	
	@FindBy(xpath = "//*[@id=\"modalPopupIngresarRespuesta\"]/div/div/div[2]/div[1]/div/div[6]/label")
	public WebElementFacade ckbSinRespuesta;

	@FindBy(xpath = "//*[@id=\"modalPopupIngresarRespuesta\"]/div/div/div[2]/div[2]/button[2]")
	public WebElementFacade BtnAceptar;
	
	@FindBy(xpath = "//*[@id=\"step-1\"]/div[5]/div/button[3]")
	public WebElementFacade BtnIngresarRespuesta;
	
	@FindBy(xpath = "//*[@id=\"step-1\"]/div[5]/div/button[3]")
	public WebElementFacade btnEnviarPropuesta;	

	@FindBy(xpath = "*[@id=\"modalPopupPropuestaRenovacion\"]/div/div/div[2]/div[2]/button[2]")
	public WebElementFacade btnEnviarAceptarPropuesta;
	//

	public void AdjuntarArchivo(String path) throws IOException {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		
		cmbOtrosDocumentos.click();
		
		String ruta = new File(".").getCanonicalPath();	
		ruta = ruta + "\\src\\test\\resources\\Files\\" + path;	
		
		WebElement uploadElement = getDriver().findElement(By.id("my-file-selector3"));
		uploadElement.sendKeys(ruta);		
	}
	
	
	public void IngresarRespuesta() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalCargando")));
		BtnIngresarRespuesta.click();
	}
	
	
	public void SeleccionaRespuesta() {
		ckbIniciaConcurso.click();
	}
	
	public void SeleccionaAceptaPropuestaRenovacion() {
		ckbAceptaPropuesta.click();
	}
	
	public void Aceptar() {
		BtnAceptar.click();
	}

	public void EnviarPropuesta() {
		btnEnviarPropuesta.click();
	}


	public void AceptaPropuesta() {
		
		getDriver().switchTo().frame(getDriver().findElement(By.xpath("//div[@id='modalPopupPropuestaRenovacion']/div/div/div[2]/div[1]/div/div[1]/div[1]/div/div[2]/iframe")));
		getDriver().findElement(By.id("tinymce")).sendKeys("ACEPTA PROPUESTA DE RENOVACIÓN");
		getDriver().switchTo().defaultContent();
		
		btnEnviarAceptarPropuesta.click();
		
	}
	
	
}
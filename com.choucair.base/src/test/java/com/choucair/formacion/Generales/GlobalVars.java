package com.choucair.formacion.Generales;

import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.pages.PageObject;

public class GlobalVars extends PageObject{
	
	public static int CodTramite;
	public WebDriverWait wait;	
	
	public GlobalVars(WebDriverWait wait) {
		super();
		this.wait = new WebDriverWait(getDriver(), 10);
	}

	public void SetCodTramite(int dato) {
		this.CodTramite = dato;
	}
	
	public int SetCodTramite() {
		return this.CodTramite;
	}

}

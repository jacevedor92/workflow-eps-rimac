package com.choucair.formacion.steps;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

import java.io.IOException;

/**
 * Estructura básica de clase para Steps RegistrarNuevaEmpresaStep
 *
 */

import java.util.List;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.choucair.formacion.pageobjects.DetalleTramitePage;
import com.choucair.formacion.pageobjects.MenuPrincipalPage;
import com.choucair.formacion.pageobjects.RegistrarEmpresaMasivoPage;
import com.choucair.formacion.pageobjects.RegistrarNuevaEmpresaPage;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;
import net.thucydides.core.annotations.Step;

public class RegistrarNuevaEmpresaStep {
	
	MenuPrincipalPage menuPrincipalPage;
	RegistrarNuevaEmpresaPage registrarNuevaEmpresaPage;
	RegistrarEmpresaMasivoPage registrarEmpresaMasivoPage;
	DetalleTramitePage detalleTramitePage;
	
	@Step
	public void IngresarNuevaEmpresa() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		menuPrincipalPage.Empresas();
		menuPrincipalPage.BusquedaEmpresas();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		registrarNuevaEmpresaPage.NuevaEmpresa();
		
	}
	
	@Step
	public void IngresarEmpresaMasivo() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		menuPrincipalPage.Empresas();
		menuPrincipalPage.BusquedaEmpresas();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
	}
	
	@Step
	public void RegistrarEmpresa(List<List<String>> data, int id) {
		String dir = data.get(id).get(2).trim();
		String dep = data.get(id).get(3).trim();
		String prov = data.get(id).get(4).trim();
		String dis = data.get(id).get(5).trim();
		
		String Contacto = data.get(id).get(8).trim();
		String CorreoCon = data.get(id).get(9).trim();;
		String TelefonoCon = data.get(id).get(10).trim();
		
		String ContactoBroker = data.get(id).get(11).trim();
		String CorreoBroker = data.get(id).get(12).trim();;
		String TelefonoBroker = data.get(id).get(13).trim();
		
		String TipoEntidad = data.get(id).get(14).trim();
		String Opt2 = data.get(id).get(15).trim();
		
		registrarNuevaEmpresaPage.Ruc(data.get(id).get(0).trim());
		registrarNuevaEmpresaPage.RazonSocial(data.get(id).get(1).trim());
		
		
		registrarNuevaEmpresaPage.Direccion(dir, dep, prov, dis);
		registrarNuevaEmpresaPage.GrupoEconomico(data.get(id).get(6).trim());
		registrarNuevaEmpresaPage.Broker(data.get(id).get(7).trim());
		
		registrarNuevaEmpresaPage.DatosContacto(Contacto,CorreoCon,TelefonoCon);
		
		if (data.get(id).get(7).trim()!="SIN BRÓKER") {
			registrarNuevaEmpresaPage.DatosBroker(ContactoBroker,CorreoBroker,TelefonoBroker);
		}
		
		
		registrarNuevaEmpresaPage.Procedencia(TipoEntidad, Opt2);
		registrarNuevaEmpresaPage.Guardar();
		registrarNuevaEmpresaPage.ConfirmarGuardar();
		
	}	
	
	@Step
	public void RegistrarEmpresaCamposIncompletos(List<List<String>> data, int id) {
				
		registrarNuevaEmpresaPage.Ruc(data.get(id).get(0).trim());
		registrarNuevaEmpresaPage.RazonSocial(data.get(id).get(1).trim());		
		
		registrarNuevaEmpresaPage.Guardar();
		
	}
	
	@Step
	public void VerificarRegistro() {
		detalleTramitePage.validar("Se registro correctamente la información");
		
	}
	
	@Step
	public void CargarArchivo(String data) throws IOException {
		registrarEmpresaMasivoPage.CargaMasiva();		
		registrarEmpresaMasivoPage.Continuar();
		registrarEmpresaMasivoPage.AdjuntarArchivo(data);
		registrarEmpresaMasivoPage.Procesar();
		registrarEmpresaMasivoPage.ValidacionEmpresaMasivo();
		
	}
	
	@Step
	public void verificarEmpresaExistente() {
		detalleTramitePage.validar("El RUC o Razón Social ya se encuentran registrados en el sistema.");
		
	}

	@Step
	public void verificarCamposIncompletos() {
		detalleTramitePage.validar("Debe completar los campos obligatorios");
		
	}
	
	
}
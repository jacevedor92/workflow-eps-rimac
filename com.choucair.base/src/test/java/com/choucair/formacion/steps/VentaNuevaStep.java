package com.choucair.formacion.steps;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.sql.Driver;

/**
 * Estructura básica de clase para Steps VentaNuevaStep
 *
 */

import java.util.List;

import com.choucair.formacion.pageobjects.BusquedaTramitePage;
import com.choucair.formacion.pageobjects.ComunicacionCuentaPage;
import com.choucair.formacion.pageobjects.ConfiguracionPlanesPage;
import com.choucair.formacion.pageobjects.CrearPolizaPage;
import com.choucair.formacion.pageobjects.DetalleTramitePage;
import com.choucair.formacion.pageobjects.EnviarPropuestaPage;
import com.choucair.formacion.pageobjects.LoginPage;
import com.choucair.formacion.pageobjects.MenuPrincipalPage;
import com.choucair.formacion.pageobjects.RegistrarTramitePage;
import com.choucair.formacion.pageobjects.RequisitosPreimplementacionPage;
import com.choucair.formacion.pageobjects.SolicitarCotizacionPage;
import com.choucair.formacion.pageobjects.TramaAfiliadosPage;

import cucumber.api.DataTable;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

public class VentaNuevaStep {
	
	LoginPage loginPage;
	RegistrarTramitePage registrarTramitePage;
	MenuPrincipalPage menuPrincipalPage;
	SolicitarCotizacionPage solicitarCotizacionPage;
	DetalleTramitePage detalleTramitePage;
	EnviarPropuestaPage enviarPropuestaPage;
	RequisitosPreimplementacionPage requisitosPreimplementacionPage;
	TramaAfiliadosPage tramaAfiliadosPage;
	CrearPolizaPage crearPolizaPage;
	ConfiguracionPlanesPage configuracionPlanesPage;
	ComunicacionCuentaPage comunicacionCuentaPage;
	
	BusquedaTramitePage busquedaTramitePage;
	
	@Step
	public void AbrirNavegador() {
		loginPage.open();
	}
	
	@Step
	public void IniciarSesion(String user, String pass) {
		loginPage.UserName(user);
		loginPage.Continuar();
		loginPage.Password(pass);
		loginPage.IniciarSesion();
		loginPage.SesionActiva();
	}
	
	@Step
	public void CerrarSesion() {
		
		menuPrincipalPage.CerrarSesion();		
		menuPrincipalPage.UsarOtraCuenta();
	}

	
	@Step
	public void IngresarNuevoTramite() {
	
		menuPrincipalPage.Tramites();
		menuPrincipalPage.BusquedaTramites();
		
		registrarTramitePage.IniciarTramite();
		registrarTramitePage.NuevaVenta();		
		
	}
	
	@Step
	public void IngresarBusquedaTramite() {
		
		menuPrincipalPage.Tramites();
		
		menuPrincipalPage.BusquedaTramites();		
	}
	
	
	@Step
	public void BusquedaTramite() {
	
		busquedaTramitePage.Buscador();
		busquedaTramitePage.NumeroTramite();
		busquedaTramitePage.Buscar();
		busquedaTramitePage.Tramite();
	}
	
	
	@Step
	public void RegistrarTramiteEmpresaExistente(List<List<String>> data,int id) {
				
		registrarTramitePage.TipoTramite(data.get(id).get(0).trim());
		registrarTramitePage.DatosEmpresa();
		registrarTramitePage.Ruc(data.get(id).get(1).trim());
		
		registrarTramitePage.Guardar();
		
	}
	
	
	@Step
	public void RegistrarTramite(List<List<String>> data,int id) {
		String dir = data.get(id).get(3).trim();
		String dep = data.get(id).get(4).trim();
		String prov = data.get(id).get(5).trim();
		String dis = data.get(id).get(6).trim();
		
		String Contacto = data.get(id).get(9).trim();
		String CorreoCon = data.get(id).get(10).trim();;
		String TelefonoCon = data.get(id).get(11).trim();
		
		String ContactoBroker = data.get(id).get(12).trim();
		String CorreoBroker = data.get(id).get(13).trim();;
		String TelefonoBroker = data.get(id).get(14).trim();
		
		String TipoEntidad = data.get(id).get(15).trim();
		String Opt2 = data.get(id).get(16).trim();
		
		registrarTramitePage.TipoTramite(data.get(id).get(0).trim());
		registrarTramitePage.DatosEmpresa();
		registrarTramitePage.Ruc(data.get(id).get(1).trim());
		registrarTramitePage.RazonSocial(data.get(id).get(2).trim());
		
		
		registrarTramitePage.Direccion(dir, dep, prov, dis);
		registrarTramitePage.GrupoEconomico(data.get(id).get(7).trim());
		registrarTramitePage.Broker(data.get(id).get(8).trim());
		
		registrarTramitePage.DatosContacto(Contacto,CorreoCon,TelefonoCon);
		
		if (data.get(id).get(8).trim()!="SIN BRÓKER") {
			registrarTramitePage.DatosBroker(ContactoBroker,CorreoBroker,TelefonoBroker);
		}		
		
		registrarTramitePage.Procedencia(TipoEntidad, Opt2);
		registrarTramitePage.Guardar();
		
	}
	
	
	
	@Step
	public void VerificarRegistroTramite() {
		registrarTramitePage.Verificacion();		
		
	}
	
	@Step
	public void SolicitarCotizacion(List<List<String>> data,int id) throws IOException {
		
		solicitarCotizacionPage.NumeroTrabajadores(data.get(id).get(0).trim());
		solicitarCotizacionPage.PotencialAfiliados(data.get(id).get(1).trim());
		solicitarCotizacionPage.FacturacionEmpresa(data.get(id).get(2).trim());

		solicitarCotizacionPage.FechaInicioVigencia(data.get(id).get(3).trim());
		solicitarCotizacionPage.TipoPlanAnterior(data.get(id).get(4).trim());
		solicitarCotizacionPage.FechaRenovacion(data.get(id).get(5).trim());
		solicitarCotizacionPage.DiagnosticoAltoCosto(data.get(id).get(6).trim());
		solicitarCotizacionPage.CondicionesEspeciales(data.get(id).get(7).trim());
		solicitarCotizacionPage.MarcarDocumentacion();
		solicitarCotizacionPage.AgregarArchivo(data.get(id).get(8).trim());
		solicitarCotizacionPage.SolicitarCotizacion();
		
		solicitarCotizacionPage.Validacion();
		
	}

	@Step
	public void SolicitarCotizacionDocumentacionIncompleta(List<List<String>> data,int id) throws IOException {
		
		solicitarCotizacionPage.NumeroTrabajadores(data.get(id).get(0).trim());
		solicitarCotizacionPage.PotencialAfiliados(data.get(id).get(1).trim());
		solicitarCotizacionPage.FacturacionEmpresa(data.get(id).get(2).trim());
		solicitarCotizacionPage.FechaInicioVigencia(data.get(id).get(3).trim());
		
		solicitarCotizacionPage.TipoPlanAnterior(data.get(id).get(4).trim());
		solicitarCotizacionPage.FechaRenovacion(data.get(id).get(5).trim());
		solicitarCotizacionPage.DiagnosticoAltoCosto(data.get(id).get(6).trim());
		solicitarCotizacionPage.CondicionesEspeciales(data.get(id).get(7).trim());
		solicitarCotizacionPage.MarcarDocumentacionIncompleta();
		solicitarCotizacionPage.AgregarArchivo(data.get(id).get(8).trim());
		solicitarCotizacionPage.SolicitarCotizacion();
		
		solicitarCotizacionPage.ValidacionDocumentacionIncompleta();
		
	}
	
	@Step
	public void RespuestaCotizacion(String path) throws IOException {
		
		detalleTramitePage.ActividadPendiente();	
		
		detalleTramitePage.IniciarConcurso();
		detalleTramitePage.AdjuntatInicioConcurso(path);		
		//detalleTramitePage.AceptarConfirmacion();
		detalleTramitePage.validarRespuestaCotizacion();
		
	}
	
	
	@Step
	public void RespuestaCotizacionPyme(String path) {
		
		detalleTramitePage.ActividadPendiente();
		
		detalleTramitePage.AdjudicacionDirecta();			
		detalleTramitePage.validarRespuestaCotizacionPyme();
		
	}
	
	@Step
	public void RechazarCotizacion(String path, String Commentario) throws IOException {
		detalleTramitePage.ActividadPendiente();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		detalleTramitePage.CotizacionRechazada();
		detalleTramitePage.AjuntarCotizacionRechazada(path);
		
		detalleTramitePage.ComentarioCotizacionRechazada(Commentario);
		detalleTramitePage.AceptarCotizacionRechazada();
		detalleTramitePage.ConfirmarCierreTramite();
//		detalleTramitePage.AceptarTramiteFin();
		detalleTramitePage.validarCotizacionRechazada();
		
	}

	@Step
	public void PropuestaFormal(List<List<String>> data, int id) {
	
		detalleTramitePage.ActividadPendiente();
		
		enviarPropuestaPage.EnviarPropuesta();
		enviarPropuestaPage.FechaDespacho(data.get(id).get(0).trim());		
		enviarPropuestaPage.Contacto(data.get(id).get(1).trim());		
		enviarPropuestaPage.Email(data.get(id).get(2).trim());
		enviarPropuestaPage.TelefonoContacto(data.get(id).get(3).trim());
		enviarPropuestaPage.Enviar();
		
		enviarPropuestaPage.validacion();
		
	}
	
	@Step
	public void PropuestaFormalRenovacion(List<List<String>> data, int id) {
	
		detalleTramitePage.ActividadPendiente();
		
		enviarPropuestaPage.EnviarPropuestaRenovacion();
		enviarPropuestaPage.FechaDespacho(data.get(id).get(0).trim());		
		enviarPropuestaPage.Contacto(data.get(id).get(1).trim());		
		enviarPropuestaPage.Email(data.get(id).get(2).trim());
		enviarPropuestaPage.TelefonoContacto(data.get(id).get(3).trim());
		enviarPropuestaPage.Enviar();
		
		enviarPropuestaPage.validacion();
		
	}

	@Step
	public void IngresarResultadoConcurso() {
			
		
		detalleTramitePage.ActividadPendiente();
			
		detalleTramitePage.ResultadoConcurso();
		detalleTramitePage.ConcursoGanado();

		detalleTramitePage.validacionResultadoConcurso();

	}
	
	@Step
	public void ConcursoNoGanado(String ruta, String comentario) throws IOException {
		
		detalleTramitePage.ActividadPendiente();
			
		detalleTramitePage.ResultadoConcurso();
		detalleTramitePage.ConcursoNoGanado();

		detalleTramitePage.validacionResultadoConcursoNoGanado(ruta,comentario);

	}
	
	
	@Step
	public void SolicitarRespuesta() {
		
		detalleTramitePage.ActividadPendiente();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		detalleTramitePage.SolicitarRespuesta();
		detalleTramitePage.ConfirmacionConcursoGanado();
		detalleTramitePage.validacionSolicitarRespuesta();
		

	}
	
	

	@Step
	public void RequisitosImplementacion(List<List<String>> data, int id) throws IOException {
		
		String dir = data.get(id).get(11).trim();
		String dep = data.get(id).get(12).trim();
		String prov = data.get(id).get(13).trim();
		String dis = data.get(id).get(14).trim();
		
		requisitosPreimplementacionPage.ActividadPendiente();		
		requisitosPreimplementacionPage.ExisteRimacSalud(data.get(id).get(0).trim());
		//requisitosPreimplementacionPage.AutorizaciónDigital(data.get(id).get(1).trim());
		//requisitosPreimplementacionPage.AdjuntarAutorizaciónDigital(data.get(id).get(2).trim());
		
		requisitosPreimplementacionPage.PolizaElectronica(data.get(id).get(1).trim());
		requisitosPreimplementacionPage.AdjuntarAutorizaciónDigital(data.get(id).get(2).trim());
		//requisitosPreimplementacionPage.AutorizaciónDigital(data.get(id).get(1).trim());
		//requisitosPreimplementacionPage.AdjuntarPoliza(data.get(id).get(4).trim());
		requisitosPreimplementacionPage.ContactoPoliza(data.get(id).get(3).trim());
		requisitosPreimplementacionPage.CorreoPoliza(data.get(id).get(4).trim());
		requisitosPreimplementacionPage.ContratoDigital(data.get(id).get(5).trim());
		requisitosPreimplementacionPage.ContactoContrato(data.get(id).get(6).trim());
		requisitosPreimplementacionPage.CorreoContrato(data.get(id).get(7).trim());
		requisitosPreimplementacionPage.InformaciónContrato();
		requisitosPreimplementacionPage.TipoDocumentoRL(data.get(id).get(8).trim());
		requisitosPreimplementacionPage.RepresentanteLegal(data.get(id).get(9).trim());
		requisitosPreimplementacionPage.NombreRL(data.get(id).get(10).trim());
		requisitosPreimplementacionPage.Direccion(dir,dep,prov,dis);
		requisitosPreimplementacionPage.PartidaElectronica(data.get(id).get(15).trim());
		requisitosPreimplementacionPage.ActaEscrutinio();
		requisitosPreimplementacionPage.AdjuntarActaEscrutinio(data.get(id).get(16).trim());
		requisitosPreimplementacionPage.FechaVotacion(data.get(id).get(17).trim());
		requisitosPreimplementacionPage.ContactoBienvenida();
		requisitosPreimplementacionPage.PersonaContacto(data.get(id).get(18).trim());
		requisitosPreimplementacionPage.CorreoContacto(data.get(id).get(19).trim());
		requisitosPreimplementacionPage.InformacionAdicional();
		requisitosPreimplementacionPage.FechaInicioVigencia(data.get(id).get(20).trim());
		requisitosPreimplementacionPage.Producto(data.get(id).get(21).trim());
		requisitosPreimplementacionPage.Documentacion();
		//requisitosPreimplementacionPage.AdjuntarDocumentos(data.get(id).get(15).trim());
		//requisitosPreimplementacionPage.CheckBox(data.get(id).get(15).trim());
		
		requisitosPreimplementacionPage.DocActaEscrutinio();
		requisitosPreimplementacionPage.DocAutorizacionEnvioDigital();
		requisitosPreimplementacionPage.DocCartaNombramiento();
		//requisitosPreimplementacionPage.DocInformacionAdicional();	
		requisitosPreimplementacionPage.AdjuntarArchivo(data.get(id).get(22).trim());
		requisitosPreimplementacionPage.iniciarImplementacion();
		requisitosPreimplementacionPage.ConfirmacionIniciarImplementacion();		
		requisitosPreimplementacionPage.validacion();

	}
	
	@Step
	public void RequisitosImplementacionRenovacion(List<List<String>> data, int id) throws IOException {
		
		String dir = data.get(id).get(13).trim();
		String dep = data.get(id).get(14).trim();
		String prov = data.get(id).get(15).trim();
		String dis = data.get(id).get(16).trim();
		
		requisitosPreimplementacionPage.ActividadPendiente();		
		requisitosPreimplementacionPage.ExisteRimacSalud(data.get(id).get(0).trim());
		requisitosPreimplementacionPage.AutorizaciónDigitalRenovacion(data.get(id).get(1).trim());
		requisitosPreimplementacionPage.AdjuntarAutorizaciónDigital(data.get(id).get(2).trim());
		
		requisitosPreimplementacionPage.PolizaElectronica(data.get(id).get(3).trim());
//		requisitosPreimplementacionPage.AdjuntarPoliza(data.get(id).get(4).trim());
		requisitosPreimplementacionPage.ContactoPoliza(data.get(id).get(5).trim());
		requisitosPreimplementacionPage.CorreoPoliza(data.get(id).get(6).trim());
		requisitosPreimplementacionPage.ContratoDigital(data.get(id).get(7).trim());
		requisitosPreimplementacionPage.ContactoContrato(data.get(id).get(8).trim());
		requisitosPreimplementacionPage.CorreoContrato(data.get(id).get(9).trim());
		requisitosPreimplementacionPage.InformaciónContrato();
		requisitosPreimplementacionPage.TipoDocumentoRLRenovacion(data.get(id).get(10).trim());
		requisitosPreimplementacionPage.RepresentanteLegal(data.get(id).get(11).trim());
		requisitosPreimplementacionPage.NombreRL(data.get(id).get(12).trim());
		requisitosPreimplementacionPage.Direccion(dir,dep,prov,dis);
		requisitosPreimplementacionPage.PartidaElectronica(data.get(id).get(17).trim());
		requisitosPreimplementacionPage.ActaEscrutinio();
		requisitosPreimplementacionPage.AdjuntarActaEscrutinio(data.get(id).get(18).trim());
		requisitosPreimplementacionPage.FechaVotacion(data.get(id).get(19).trim());
		requisitosPreimplementacionPage.ContactoBienvenida();
		requisitosPreimplementacionPage.PersonaContacto(data.get(id).get(20).trim());
		requisitosPreimplementacionPage.CorreoContacto(data.get(id).get(21).trim());
		requisitosPreimplementacionPage.InformacionAdicional();
		requisitosPreimplementacionPage.FechaInicioVigencia(data.get(id).get(22).trim());
		requisitosPreimplementacionPage.Producto(data.get(id).get(23).trim());
		requisitosPreimplementacionPage.Documentacion();
		//requisitosPreimplementacionPage.AdjuntarDocumentos(data.get(id).get(15).trim());
		//requisitosPreimplementacionPage.CheckBox(data.get(id).get(15).trim());
		
//		requisitosPreimplementacionPage.DocActaEscrutinio();
//		requisitosPreimplementacionPage.DocAutorizacionEnvioDigital();
//		requisitosPreimplementacionPage.DocCartaNombramiento();
		//requisitosPreimplementacionPage.DocInformacionAdicional();	
		requisitosPreimplementacionPage.AdjuntarArchivo(data.get(id).get(24).trim());
		requisitosPreimplementacionPage.iniciarImplementacion();
		requisitosPreimplementacionPage.ConfirmacionIniciarImplementacion();		
		requisitosPreimplementacionPage.validacion();

	}
	
	@Step
	public void TramaAfiliados(String trim) throws IOException {
		
		detalleTramitePage.ActividadPendienteTramaAfiliados();
				
		tramaAfiliadosPage.AdjuntarTrama(trim);
		tramaAfiliadosPage.Enviar();
						
		tramaAfiliadosPage.Confirmacion();
		
		tramaAfiliadosPage.validacion();

	}
	
	
	@Step
	public void CreaPoliza(String data) {
		detalleTramitePage.ActividadPendiente();
		
		crearPolizaPage.NumeroPoliza(data);
		crearPolizaPage.EnviarPlanes();
		
		crearPolizaPage.validacion();

	}
	
	@Step
	public void ConfigurarPlanes(String data) {
		detalleTramitePage.ActividadPendiente();
		
		configuracionPlanesPage.Planes(data);
		configuracionPlanesPage.EnviarPlanes();		
		configuracionPlanesPage.validacion();		
		
	}
	
	@Step
	public void PlanesDevolverTramite(String motivo,String path) throws IOException {
		
		detalleTramitePage.ActividadPendiente();		
		configuracionPlanesPage.devolver();
		configuracionPlanesPage.validacionDevolver(motivo,path);
	}
	
	
	@Step
	public void ComunicacionCuenta(List<List<String>> data, int id) throws IOException {
		
		detalleTramitePage.ActividadPendiente();		
		
		//comunicacionCuentaPage.FechaVigencia(data.get(id).get(0).trim());
		comunicacionCuentaPage.ComisionPoliza(data.get(id).get(1).trim());
		comunicacionCuentaPage.CondicionesBeneficios(data.get(id).get(5).trim());
	
		comunicacionCuentaPage.RutaPlanes(data.get(id).get(6).trim());
		comunicacionCuentaPage.AgregarArchivo(data.get(id).get(7).trim());
		comunicacionCuentaPage.EnviarComunicado();
		comunicacionCuentaPage.AceptarEnviarComunicado();
		
		comunicacionCuentaPage.validacion("Se ha enviado el comunicado de cuenta");
	}
	
	@Step
	public void RegistrarAfiliados(String user, String pass) {
		detalleTramitePage.DeterminarResponsable(user,pass);
		
		detalleTramitePage.ActividadPendiente();
		detalleTramitePage.CargaCompleta();
		detalleTramitePage.Finalizar();
		detalleTramitePage.FinalizarSi();
		
		detalleTramitePage.validarRegistroAfiliados();		
		
	}

	

	
	
	
	
	
	
}
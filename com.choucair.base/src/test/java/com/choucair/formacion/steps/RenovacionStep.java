package com.choucair.formacion.steps;

import java.io.IOException;

/**
 * Estructura básica de clase para Steps RenovacionStep
 *
 */

import java.util.List;

import com.choucair.formacion.pageobjects.BusquedaTramitePage;
import com.choucair.formacion.pageobjects.ComunicacionCuentaPage;
import com.choucair.formacion.pageobjects.ConfiguracionPlanesPage;
import com.choucair.formacion.pageobjects.DetalleTramitePage;

import com.choucair.formacion.pageobjects.LoginPage;
import com.choucair.formacion.pageobjects.MenuPrincipalPage;
import com.choucair.formacion.pageobjects.RegistrarTramitePage;
import com.choucair.formacion.pageobjects.RnGestionarClientesRenovarPage;
import com.choucair.formacion.pageobjects.RnIngresarRespuestaRpopuestaPage;
import com.choucair.formacion.pageobjects.RnIniciaConcursoPage;
import com.choucair.formacion.pageobjects.RnNuevoTramitePage;
import com.choucair.formacion.pageobjects.RnSolicitarProspectoPage;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

public class RenovacionStep {
	
	RnNuevoTramitePage rnNuevoTramitePage;
	MenuPrincipalPage menuPrincipalPage;
	RegistrarTramitePage registrarTramitePage;
	RnGestionarClientesRenovarPage rnGestionarClientesRenovarPage;
	DetalleTramitePage detalleTramitePage;
	BusquedaTramitePage busquedaTramitePage;
	RnIngresarRespuestaRpopuestaPage rnIngresarRespuestaRpopuestaPage;
	RnIniciaConcursoPage rnIniciaConcursoPage;
	RnSolicitarProspectoPage rnSolicitarProspectoPage;
	ConfiguracionPlanesPage configuracionPlanesPage;
	ComunicacionCuentaPage comunicacionCuentaPage;

	@Step
	public void NuevoTramiteRenovacion() {
			
		
		menuPrincipalPage.Tramites();
		menuPrincipalPage.BusquedaTramites();		
		
		registrarTramitePage.IniciarTramite();
		rnNuevoTramitePage.Renovacion();
		
		
	}
	
	@Step
	public void RegistraNuevoTramiteRenovacion(List<List<String>> data,int id) {
		

		String dir = data.get(id).get(13).trim();
		String Dep = data.get(id).get(14).trim();
		String Prov = data.get(id).get(15).trim();
		String Dis = data.get(id).get(16).trim();
		
		rnNuevoTramitePage.GrupoEconomico(data.get(id).get(0).trim());
		rnNuevoTramitePage.Broker(data.get(id).get(1).trim());
		rnNuevoTramitePage.TipoPlan(data.get(id).get(2).trim());
		rnNuevoTramitePage.AgregarEmpresa();
		rnNuevoTramitePage.Ruc(data.get(id).get(3).trim());
		rnNuevoTramitePage.RazonSocial(data.get(id).get(4).trim());
		rnNuevoTramitePage.NumeroPoliza(data.get(id).get(5).trim());
		rnNuevoTramitePage.FechaVigencia(data.get(id).get(6).trim(),data.get(id).get(2).trim());
		rnNuevoTramitePage.Aceptar();
		rnNuevoTramitePage.Contacto();
		rnNuevoTramitePage.PersonaContacto(data.get(id).get(7).trim());
		rnNuevoTramitePage.CorreoContacto(data.get(id).get(8).trim());
		rnNuevoTramitePage.TelefonoContacto(data.get(id).get(9).trim());
		rnNuevoTramitePage.CargoContacto(data.get(id).get(10).trim());
		rnNuevoTramitePage.ComunicacionElectronica(data.get(id).get(11).trim(),data.get(id).get(12).trim());
		rnNuevoTramitePage.PolizaElectronica(data.get(id).get(13).trim(),data.get(id).get(14).trim());
		rnNuevoTramitePage.Direccion(dir, Dep, Prov, Dis);
		rnNuevoTramitePage.AceptarContacto();
		rnNuevoTramitePage.Guardar();
		rnNuevoTramitePage.Confirmacion();
		rnNuevoTramitePage.VerificacionRegistroTramiteRenovacion();
	}

	@Step
	public void GestionarClientesRenovar() {
		detalleTramitePage.ActividadPendiente();
		rnGestionarClientesRenovarPage.Renovacion();
		rnGestionarClientesRenovarPage.IniciarProceso();
		rnGestionarClientesRenovarPage.Confirmacion();
		rnGestionarClientesRenovarPage.validacionGestionClientes();		
	}
	
	@Step
	public void SolicitarProspecto(List<List<String>> data, int id) throws IOException {
		detalleTramitePage.ActividadPendiente();
		rnSolicitarProspectoPage.PlanVigente(data.get(id).get(0).trim());
		rnSolicitarProspectoPage.RangoPrimaProyectada(data.get(id).get(1).trim());
		rnSolicitarProspectoPage.SiniestralidadProyectada(data.get(id).get(2).trim());
		rnSolicitarProspectoPage.CambiosCondiciones(data.get(id).get(3).trim());
		rnSolicitarProspectoPage.SolicitarProspecto();
		rnSolicitarProspectoPage.validacion(data.get(id).get(2).trim());
	}
	
	@Step
	public void AprobarPropuesta() {
		detalleTramitePage.ActividadPendiente();
		detalleTramitePage.AprobarPropuestaPersonalizada();
		detalleTramitePage.ConfirmacionConcursoGanado();
		detalleTramitePage.validar("Se ha aprobado los cambios en condiciones");
	}
	
	@Step
	public void ObservarPropuesta() {
		detalleTramitePage.ActividadPendiente();
		
		detalleTramitePage.ObservarPropuestaRenovacion();		
	}
	
	
	@Step
	public void ConfigurarProspecto(String numero) {
		detalleTramitePage.ActividadPendiente();
		//Enviar Numero de prospecto 
		detalleTramitePage.EnviarNumeroPrspectoRenovacion();
		
		detalleTramitePage.NumeroProspecto(numero);
		detalleTramitePage.EnviarNumeroProspecto();
		detalleTramitePage.ConfirmacionConcursoGanado();
		detalleTramitePage.validar("Se ha enviado el número de prospecto");
		
	}
	
	
	@Step
	public void CargarPlanesPropuesta(String plan, String fichaTecnica) throws IOException {
		detalleTramitePage.ActividadPendiente();
		detalleTramitePage.AdjuntarPlanVigente(plan);
		detalleTramitePage.AdjuntarFichaTecnica(fichaTecnica);
		detalleTramitePage.EnviarPropuestaCargarPlanes();
		detalleTramitePage.ConfirmacionConcursoGanado();
		detalleTramitePage.validar("Se ha enviado la propuesta al administrador de cuenta");
	}
	
	@Step
	public void EnviarPropuestaRenovacion() {
		detalleTramitePage.ActividadPendiente();
		detalleTramitePage.CambiosCondiciones();
		detalleTramitePage.EnviarPropuestaRenovacion();
		detalleTramitePage.ConfirmacionConcursoGanado();
		detalleTramitePage.validar("Se ha enviado la propuesta formal");
	}
	
	
	//flujo alterno
	@Step
	public void SolicitarReajusteEnviarPropuesta() {
		detalleTramitePage.ActividadPendiente();
		
		detalleTramitePage.SolicitarReajuste();
	}
	
	@Step
	public void EsperarActividad() {
		detalleTramitePage.EsperarActividadIngresarRespuestaPropuesta();
		
	}
	
	@Step
	public void BuscarTramite() {
		busquedaTramitePage.Buscador();
		busquedaTramitePage.NumeroTramite();
		busquedaTramitePage.Buscar();
		busquedaTramitePage.TramiteRenovacion();
		
	}

	@Step
	public void IngresarRespuestaPropuesta(String path) {
		detalleTramitePage.ActividadPendiente();
		//rnIngresarRespuestaRpopuestaPage.AdjuntarArchivo(path);
		rnIngresarRespuestaRpopuestaPage.IngresarRespuesta();
		rnIngresarRespuestaRpopuestaPage.SeleccionaRespuesta();
		rnIngresarRespuestaRpopuestaPage.Aceptar();	
	}
	
	@Step
	public void IngresarRespuestaAceptaPropuesta() {
		detalleTramitePage.ActividadPendiente();
		rnIngresarRespuestaRpopuestaPage.IngresarRespuesta();
		rnIngresarRespuestaRpopuestaPage.SeleccionaAceptaPropuestaRenovacion();
		rnIngresarRespuestaRpopuestaPage.AceptaPropuesta();
		detalleTramitePage.ConfirmacionConcursoGanado();
		detalleTramitePage.validar("Se ha guardado la aceptación de la propuesta.");
	
	}
	
	
	
	@Step
	public void EnviarPropuesta() {
		detalleTramitePage.ActividadPendiente();
		rnIngresarRespuestaRpopuestaPage.EnviarPropuesta();
		detalleTramitePage.ConfirmacionConcursoGanado();
		detalleTramitePage.validar("Se ha enviado la propuesta formal");		
	}

	@Step
	public void IniciarConcurso(List<List<String>> data, int id) throws IOException {
		rnIniciaConcursoPage.CartaInvitacion(data.get(id).get(0).trim());
		rnIniciaConcursoPage.Comentarios(data.get(id).get(1).trim());
		rnIniciaConcursoPage.DocumentoOpcional(data.get(id).get(2).trim());
		rnIniciaConcursoPage.Broker(data.get(id).get(3).trim());
		rnIniciaConcursoPage.EmpresaPersonaContacto(data.get(id).get(4).trim());
		rnIniciaConcursoPage.EmpresaPersonaCorreo(data.get(id).get(5).trim());
		rnIniciaConcursoPage.EmpresaPersonaTelefono(data.get(id).get(6).trim());
		rnIniciaConcursoPage.BrokerPersonaNombre(data.get(id).get(7).trim());
		rnIniciaConcursoPage.BrokerPersonaCorreo(data.get(id).get(8).trim());
		rnIniciaConcursoPage.BrokerPersonaTelefono(data.get(id).get(9).trim());
		rnIniciaConcursoPage.Guardar();
		rnIniciaConcursoPage.validacion();
		
		
	}
	
	@Step
	public void ResultadoConcursoRenovacion() {
		detalleTramitePage.ResultadoConcursoRenovacion();
		detalleTramitePage.ConcursoGanadoRenovacion();

		detalleTramitePage.validacionResultadoConcurso();
		
	}
	
	@Step
	public void SolicitarRespuestaResultadoConcurso() {
		detalleTramitePage.SolicitarRespuestaIngresarRespuestaConcurso();
		detalleTramitePage.ConfirmacionConcursoGanado();
		detalleTramitePage.validar("Se envió el correo electrónico al cliente/bróker");
		
	}
	
	
	
	@Step
	public void RegistrarPlanesRenovacion(String data) {
				
		detalleTramitePage.ActividadPendiente();
		
		configuracionPlanesPage.PlanesRenovacion(data);
		configuracionPlanesPage.EnviarPlanesRenovacion();
		configuracionPlanesPage.Confirmar();
		//configuracionPlanesPage.Aceptar();
		detalleTramitePage.validar("Se ha enviado la información al analista de suscripción");
		
	}

	@Step
	public void ComunicadoCuentaRenovacion(List<List<String>> data, int id) throws IOException {
		detalleTramitePage.ActividadPendiente();		
		
		//comunicacionCuentaPage.FechaVigencia(data.get(id).get(0).trim());
		comunicacionCuentaPage.ComisionPoliza(data.get(id).get(0).trim());
		comunicacionCuentaPage.PlanesPotestativosRenovacion(data.get(id).get(1).trim());
		comunicacionCuentaPage.AjusteRenovacion(data.get(id).get(2).trim());
		comunicacionCuentaPage.cambiosImportantes(data.get(id).get(3).trim());
		comunicacionCuentaPage.RutaPlanes(data.get(id).get(4).trim());
		comunicacionCuentaPage.AgregarArchivo(data.get(id).get(5).trim());
		comunicacionCuentaPage.EnviarComunicadoRenovacion();
		comunicacionCuentaPage.AceptarEnviarComunicado();
		
		comunicacionCuentaPage.validacion("La notificación está siendo procesada y será enviada en breve");
		
	}

	@Step
	public void MigrarAfiliados() {
		detalleTramitePage.ActividadPendiente();		
		detalleTramitePage.CuentaConAfiliados();
		detalleTramitePage.FinalizarRenovacion();
		detalleTramitePage.ConfirmacionConcursoGanado();
		detalleTramitePage.validar("Se ha finalizado su tarea");
	}

	


	

	

	
	
	
}
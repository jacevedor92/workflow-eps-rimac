package com.choucair.formacion.definition;

/**
 * Estructura básica de clase para Definition VentaNuevaDefinition
 *
 */

import net.thucydides.core.annotations.Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.io.IOException;
import java.util.List;
import com.choucair.formacion.steps.VentaNuevaStep;

public class VentaNuevaDefinition {
	
	@Steps
	VentaNuevaStep ventaNuevaStep;
	
	@Given("^Abrir Navegador$")
	public void abrir_navegador(){
		ventaNuevaStep.AbrirNavegador();	
	}
	
	@Given("^Autenticar y verificar$")
	public void autenticar_y_verificar(DataTable arg1){
		
		List<List<String>> data = arg1.raw();
		String User = data.get(1).get(0).trim();
		String Pass = data.get(1).get(1).trim();
		
		ventaNuevaStep.IniciarSesion(User,Pass);		
	}
	
	@Given("^Ingreso opcion nuevo tramite$")
	public void ingreso_opcion_nuevo_tramite(){
		ventaNuevaStep.IngresarNuevoTramite();
		
	}
	
	@When("^Registro de tramite Empresa Existente$")
	public void registro_de_tramite_empresa_existente(DataTable arg1){
		
		List<List<String>> data = arg1.raw();	
		
		for (int i = 1; i < data.size(); i++) {			
			ventaNuevaStep.RegistrarTramiteEmpresaExistente(data,i);			
		}
		ventaNuevaStep.VerificarRegistroTramite();
	}
	
	
	@When("^Realizo el registro del tramite$")
	public void realizo_el_registro_del_tramite(DataTable arg1){
		
	List<List<String>> data = arg1.raw();	
		
		for (int i = 1; i < data.size(); i++) {			
			ventaNuevaStep.RegistrarTramite(data,i);			
		}
		ventaNuevaStep.VerificarRegistroTramite();
	}
	
	@When("^Solicitar Cotizacion$")
	public void solicitar_cotizacion(DataTable arg1) throws IOException{
		
		List<List<String>> data = arg1.raw();	
		
		for (int i = 1; i < data.size(); i++) {			
			ventaNuevaStep.SolicitarCotizacion(data,i);			
		}
	}
	
	@When("^Solicitar Cotizacion Documentacion Incompleta$")
	public void solicitar_cotizacion_documentacion_incompleta(DataTable arg1) throws IOException{
		
		List<List<String>> data = arg1.raw();	
		
		for (int i = 1; i < data.size(); i++) {			
			ventaNuevaStep.SolicitarCotizacionDocumentacionIncompleta(data,i);			
		}
	}
	
	@When("^Rechazar Cotizacion$")
	public void rechazar_cotizacion(DataTable arg1) throws IOException{
		List<List<String>> data = arg1.raw();		
		ventaNuevaStep.RechazarCotizacion(data.get(1).get(0).trim(),data.get(1).get(1).trim());
	}

	
	@When("^Ingresar Respuesta de Cotizacion$")
	public void ingresar_Respuesta_de_Cotizacion(DataTable arg1) throws IOException{
		List<List<String>> data = arg1.raw();		
		ventaNuevaStep.RespuestaCotizacion(data.get(1).get(0).trim());
	}
	
	@When("^Ingresar Respuesta de Cotizacion Pyme$")
	public void ingresar_Respuesta_de_Cotizacion_pyme(DataTable arg1){
		List<List<String>> data = arg1.raw();		
		ventaNuevaStep.RespuestaCotizacionPyme(data.get(1).get(0).trim());
	}	
	
	
	@When("^Enviar Propuesta Formal$")
	public void enviar_propuesta_formal(DataTable arg1){
		
		List<List<String>> data = arg1.raw();	
		
		for (int i = 1; i < data.size(); i++) {			
			ventaNuevaStep.PropuestaFormal(data,i);			
		}		
	}	
	
	@When("^Resultado de concurso$")
	public void resultado_de_concurso(){
		ventaNuevaStep.IngresarResultadoConcurso();
	}
	
	@When("^Concurso No Ganado$")
	public void concurso_no_ganado(DataTable arg1) throws IOException{
		
		List<List<String>> data = arg1.raw();
		String ruta = data.get(1).get(0).trim();
		String Comentario = data.get(1).get(1).trim();
		
		
		ventaNuevaStep.ConcursoNoGanado(ruta,Comentario);
	}
	
	@When("^Solicitar Respuesta Concurso$")
	public void solicitar_respuesta_concurso(){
		ventaNuevaStep.SolicitarRespuesta();
	}
	
	@When("^Requisitos pre-implementación$")
	public void requisitos_pre_implementación(DataTable arg1) throws IOException{
		
		List<List<String>> data = arg1.raw();	
		
		for (int i = 1; i < data.size(); i++) {			
			ventaNuevaStep.RequisitosImplementacion(data,i);			
		}	
	}			
	
	@When("^Enviar trama afiliados$")
	public void enviar_trama_afiliados(DataTable arg1) throws Throwable {
		
		List<List<String>> data = arg1.raw();		
		ventaNuevaStep.TramaAfiliados(data.get(1).get(0).trim());
	}
	
	@When("^Cerrar sesion$")
	public void cerrar_sesion(){
		ventaNuevaStep.CerrarSesion();
	}
	
	@When("^Buscar tramite$")
	public void buscar_tramite(){
		ventaNuevaStep.IngresarBusquedaTramite();
		ventaNuevaStep.BusquedaTramite();
	}
	
	@When("^Crea Poliza$")
	public void crea_Poliza(DataTable arg1){
		List<List<String>> data = arg1.raw();		
		
		ventaNuevaStep.CreaPoliza(data.get(1).get(0).trim());
	}
	
	@When("^Configurar Planes$")
	public void configurar_Planes(DataTable arg1){
		
		List<List<String>> data = arg1.raw();		
		ventaNuevaStep.ConfigurarPlanes(data.get(1).get(0).trim());
	}
	
	
	@When("^Planes Configurados Devolver tramite$")
	public void planes_configurados_devolver_tramite(DataTable arg1) throws IOException{
		
		List<List<String>> data = arg1.raw();		
		ventaNuevaStep.PlanesDevolverTramite(data.get(1).get(0).trim(),data.get(1).get(1).trim());
	}
	
	@When("^Comunicacion de Cuenta$")
	public void comunicacion_de_Cuenta(DataTable arg1) throws Throwable {
		
		List<List<String>> data = arg1.raw();	
		
		for (int i = 1; i < data.size(); i++) {			
			ventaNuevaStep.ComunicacionCuenta(data,i);	
		}
	}
	
	@When("^Registrar afiliados$")
	public void registrar_afiliados(DataTable arg1) {
		
		List<List<String>> data = arg1.raw();
		String User = data.get(1).get(0).trim();
		String Pass = data.get(1).get(1).trim();
		
		ventaNuevaStep.RegistrarAfiliados(User,Pass);
	}
		
		
		
	
	}
package com.choucair.formacion.definition;

/**
 * Estructura básica de clase para Definition RenovacionDefinition
 *
 */

import net.thucydides.core.annotations.Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.io.IOException;
import java.util.List;

import com.choucair.formacion.steps.RenovacionStep;
import com.choucair.formacion.steps.VentaNuevaStep;

public class RenovacionDefinition {
	
	@Steps
	RenovacionStep renovacionStep;
	
	@Steps
	VentaNuevaStep ventaNuevaStep;
	
	public static List<List<String>> data;
	
	@Given("^Ingreso opcion renovacion$")
	public void ingreso_opcion_renovacion(){
		renovacionStep.NuevoTramiteRenovacion();
		
	}
	
	@When("^Nuevo Tramite Renovacion$")
	public void Nuevo_tramite_renovacion(DataTable arg1){
		
		List<List<String>> data = arg1.raw();	
		
		for (int i = 1; i < data.size(); i++) {			
			renovacionStep.RegistraNuevoTramiteRenovacion(data,i);
		}		
	}
	
	@When("^Gestionar Clientes a renovar$")
	public void gestionar_clientes_a_renovar(){		
		renovacionStep.GestionarClientesRenovar();			
	}
	
	@When("^Solicitar Prospecto$")
	public void solicitar_prospecto(DataTable arg1) throws IOException{
		
		List<List<String>> data = arg1.raw();	
		
		for (int i = 1; i < data.size(); i++) {			
			renovacionStep.SolicitarProspecto(data,i);
		}		
	}
	
	@When("^Aprobar propuesta personalizada$")
	public void aprobar_propuesta_personalizada(){		
		renovacionStep.AprobarPropuesta();
	}
	
	
	@When("^Observar propuesta personalizada$")
	public void Observar_propuesta_personalizada(){		
		
		renovacionStep.ObservarPropuesta();
	}
	
	
	@Given("^Registrar Prospecto$")
	public void registrar_prospecto(DataTable arg1){
		
		List<List<String>> data = arg1.raw();
		String Numero = data.get(1).get(0).trim();
		
		renovacionStep.ConfigurarProspecto(Numero);	
	  
	}
	
	@Given("^Cargar Planes Propuesta$")
	public void Cargar_planes_propuesta(DataTable arg1) throws IOException{
		
		List<List<String>> data = arg1.raw();
		String PlanPropuesto = data.get(1).get(0).trim();
		String FichaTecnica = data.get(1).get(1).trim();
		
		renovacionStep.CargarPlanesPropuesta(PlanPropuesto,FichaTecnica);		
	}
	
	
	@Given("^Observar Prospecto$")
	public void Observar_Prospecto(){
				
		renovacionStep.ObservarPropuesta();
	}
	
	
	@Given("^Enviar Propuesta Renovacion$")
	public void enviar_propuesta_renovacion(){				
		renovacionStep.EnviarPropuestaRenovacion();		
	}
	
	@Given("^Solicitar Reajuste Enviar Propuesta$")
	public void Solicitar_Reajuste_Enviar_Propuesta(){				
		renovacionStep.SolicitarReajusteEnviarPropuesta();		
	}
	
	
	@When("^Esperar Actividad$")
	public void esperar_actividad(){		
		renovacionStep.EsperarActividad();		
	}
	
	@When("^Buscar tramite renovacion$")
	public void Buscar_tramite_renovacion(){
		ventaNuevaStep.IngresarBusquedaTramite();
		renovacionStep.BuscarTramite();
	}
	
	@Given("^Ingresar respuesta propuesta$")
	public void ingresar_respuesta_propuesta(DataTable arg1){
		
		List<List<String>> data = arg1.raw();
		String path = data.get(1).get(0).trim();
		
		renovacionStep.IngresarRespuestaPropuesta(path);	  
	}
	
	//flujo alterno Acepta propuesta
	@Given("^Ingresar respuesta Acepta Propuesta$")
	public void Ingresar_respuesta_Acepta_Propuesta(){
				
		renovacionStep.IngresarRespuestaAceptaPropuesta();	  
	}
	
	
	
	
	//flujo alterno
	@Given("^Enviar Propuesta - Ingresar Respuesta Propuesta$")
	public void Enviar_Propuesta(){				
		renovacionStep.EnviarPropuesta();
	}
	
	
	
	
	@When("^Iniciar Concurso$")
	public void Iniciar_concurso(DataTable arg1) throws IOException{
		
		List<List<String>> data = arg1.raw();	
		
		for (int i = 1; i < data.size(); i++) {			
			renovacionStep.IniciarConcurso(data,i);
		}		
	}
	
	@When("^Enviar Propuesta Formal Renovacion$")
	public void enviar_propuesta_formal_renovacion(DataTable arg1){
		
		List<List<String>> data = arg1.raw();	
		
		for (int i = 1; i < data.size(); i++) {			
			ventaNuevaStep.PropuestaFormalRenovacion(data,i);			
		}		
	}
	
	@When("^Resultado de concurso Renovacion$")
	public void resultado_de_concurso_renovacion(){		
		renovacionStep.ResultadoConcursoRenovacion();	
	}
	
	
	@When("^Solicitar Respuesta Resultado de concurso$")
	public void Solicitar_Respuesta_Resultado_de_concurso(){		
		renovacionStep.SolicitarRespuestaResultadoConcurso();	
	}
	
	
	@When("^Requisitos pre-implementación Renovacion$")
	public void requisitos_pre_implementación_renovacion(DataTable arg1) throws IOException{
		
		List<List<String>> data = arg1.raw();	
		
		for (int i = 1; i < data.size(); i++) {			
			ventaNuevaStep.RequisitosImplementacionRenovacion(data,i);			
		}	
	}	
	
	
	
	@When("^Configurar Planes Renovacion$")
	public void Configurar_Planes_Renovacion(DataTable arg1){
		
		List<List<String>> data = arg1.raw();	
			
		renovacionStep.RegistrarPlanesRenovacion(data.get(0).get(0).trim());			
		
	}
	

	
	@When("^Comunicacion de Cuenta Renovacion$")
	public void Comunicacion_de_Cuenta_Renovacion(DataTable arg1) throws IOException{
		
		List<List<String>> data = arg1.raw();	
		
		for (int i = 1; i < data.size(); i++) {			
			renovacionStep.ComunicadoCuentaRenovacion(data,i);			
		}	
	}
	
	
	@When("^Migrar Afiliados$")
	public void migrar_afiliados(){		
		renovacionStep.MigrarAfiliados();	
	}
	
	
}
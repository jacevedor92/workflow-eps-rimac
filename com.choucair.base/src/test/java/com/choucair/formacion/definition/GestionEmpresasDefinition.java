package com.choucair.formacion.definition;

/**
 * Estructura básica de clase para Definition GestionEmpresasDefinition
 *
 */

import net.thucydides.core.annotations.Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.List;

import com.choucair.formacion.steps.RegistrarNuevaEmpresaStep;
import com.choucair.formacion.steps.VentaNuevaStep;

public class GestionEmpresasDefinition {
	
	@Steps
	RegistrarNuevaEmpresaStep gestionEmpresasStep;
	
	@Given("^Ingreso opcion nueva Empresa$")
	public void ingreso_opcion_nueva_empresa(){
		gestionEmpresasStep.IngresarNuevaEmpresa();
		
	}
	
	@Then("^Ingreso datos de la nueva empresa$")
	public void Ingreso_datos_nueva_empresa(DataTable arg1){
		
		List<List<String>> data = arg1.raw();	
		
		for (int i = 1; i < data.size(); i++) {			
			gestionEmpresasStep.RegistrarEmpresa(data,i);
		}		
	}
	
	
	@Then("^Ingreso datos incompletos de la nueva empresa$")
	public void Ingreso_datos_incompletos_nueva_empresa(DataTable arg1){
		
		List<List<String>> data = arg1.raw();	
		
		for (int i = 1; i < data.size(); i++) {			
			gestionEmpresasStep.RegistrarEmpresaCamposIncompletos(data,i);
		}		
	}
	
	@Then("^Verifico Registro Exitoso$")
	public void Verifico_registro_exitoso(){
		gestionEmpresasStep.VerificarRegistro();
		
	}
	
	@Then("^Verifico Empresa Existente$")
	public void verifico_Empresa_Existente(){
		gestionEmpresasStep.verificarEmpresaExistente();
	}
	
	@Then("^Verifico campos incompletos$")
	public void Verifico_campos_incompletos(){
		gestionEmpresasStep.verificarCamposIncompletos();
	}
	

	
}
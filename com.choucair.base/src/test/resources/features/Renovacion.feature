

@tag
Feature: Flujo Renovación WorkFlow EPS 

  	@RenovacionPersonalizada
			Scenario: Renovacion Personalizada
		Given Abrir Navegador
		And Autenticar y verificar
			|Correo																				|password		|
			|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*	|
		And Ingreso opcion renovacion
		And Nuevo Tramite Renovacion
			|GrupoEconomico	|Broker															|TipoPLan				|Ruc				|RazonSocial|NroPoliza|FechaVigencia|PersonaContacto|CorreoContacto				|TelefonoContacto	|CargoContacto|ComunicacioElectronica	|CorreoComucicacionElectronica|Poliza	|CorreoPoliza					|Direccion							|Departamento	|provincia|distrito	|																																																																	
			|SIN GRUPO			|CONTACTO CORREDORES DE SEGUROS S.A.|Personalizado	|20271653940|Empresa 1	|E1234		|01/03/2019		|Jhon E Acevedo	|jeacevedo92@gmail.com|3154439086				|Analista			|Si											|jeacevedo92@gmail.com				|si			|jeacevedo92@gmail.com|avnida 123, san isidro	|Lima					|Lima			|Lima			|																											
		And Gestionar Clientes a renovar
		And Solicitar Prospecto
		|PlanVigente					|RangoPrimaProyectada	|SiniestralidadProyectadaConAjuste|CambiosEnCondiciones		|
		|PlanMultiempresa.pdf	|0 - 300							|50																|cambios en condiciones	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$ 	|		
		And Buscar tramite renovacion
		And Registrar Prospecto
		|numeroProspecto|
		|12345					|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*|
		And Buscar tramite renovacion
		And Cargar Planes Propuesta
		|PlanPropuesto				|FichaTecnica					|
		|PlanMultiempresa.pdf	|PlanMultiempresa.pdf	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$ 	|		
		And Buscar tramite renovacion
		And Enviar Propuesta Renovacion	
		And Ingresar respuesta propuesta
		|documento(opcional)					|
		|TRAMA_EPS_(Actualizado).xlsx	|
		And Iniciar Concurso
		|CartaInvitacion				|Comentario				|Documento(opcional)					|Broker															|PersonaContacto|CorreoContacto					|TelefonoContacto	|EjecutivoBroker|CorreoEBorker			|TelefonoEBroker|
		|Carta_de_Invitación.doc|no hay comentario|TRAMA_EPS_(Actualizado).xlsx	|CONTACTO CORREDORES DE SEGUROS S.A.|John E Acevedo	|jeacevedo92@gmail.com	|3154439086				|Jhon Acevedo		|jeacevedo@gmail.com|3154439086			|																																												
		And Enviar Propuesta Formal Renovacion
		|FechaDespacho		|Contacto								|Email										|Telefono		|
		|02/02/2019 12:15	|jhon contacto propuesta|jhonedison92@hotmail.com	|9315443908	|
		And Resultado de concurso Renovacion
		And Requisitos pre-implementación Renovacion
		|ExisteRimacSalud	|AutorizaciónDigital	|AdjuntarAutorizacion		|PolizaElectronica|RutaAjuntarPoliza						|ContactoPoliza	|CorreoContactoPoliza					|ContratoDigital|ContactoContratoDigital|CorreoContratoDigital		|TipoDocumento|DniRepLegal|NombreRepLegal						|DIRECIION																		|DEPARTAMENTO	|PROVINCIA|DISTRITO|PartidaElectronica|RutaActaEscrutinio				|FechaVotacion|PersonaContacto					|CorreoContacto					|FechaInicioVigencia|Producto						|RutaDocumentacion						|EntidadPredecesora	|		
		|No								|Si										|Carta_de_Invitación.doc|Si								|Comunicación Electrónica.doc	|jhon poliza		|jacevedor@choucairtesting.com|Si							|Jhon Contrato Digital	|jhonedison92@hotmail.com	|DNI					|45423558		|Jhon Representante Legal	|Paseo de la República 3211, San Isidro 15047 |Lima					|Lima			|San Luis|P456664						|SolicitarCotizacion.docx	|05/01/2019		|jhon Contacto Bienvenida	|jeacevedo92@utp.edu.co	|05/01/2019					|Planes médicos EPS	|Comunicación Electrónica.doc	|EPS								|
		And Configurar Planes Renovacion
		|PlanesCreados				|
		|plan 1 plan 2 plan 3	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*|
		And Buscar tramite renovacion
		And Comunicacion de Cuenta Renovacion
		|Ajuste	|PlanesPotestativos	|Ajustes	|CambiosImportantes	|RutaPlanes				|Planes																						|
		|5%			|Si									|Ajuste 5%|cambios						|//rutacompartida	|Carta_de_Invitación.doc|
		And Migrar Afiliados
		
		
		@RenovacionPersonalizadaAutonomia
			Scenario: Renovacion personalizada con autonomia
		Given Abrir Navegador
		And Autenticar y verificar
			|Correo																				|password		|
			|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*	|
		And Ingreso opcion renovacion
		And Nuevo Tramite Renovacion
			|GrupoEconomico	|Broker															|TipoPLan				|Ruc				|RazonSocial|NroPoliza|FechaVigencia|PersonaContacto|CorreoContacto				|TelefonoContacto	|CargoContacto|ComunicacioElectronica	|CorreoComucicacionElectronica|Poliza	|CorreoPoliza					|Direccion							|Departamento	|provincia|distrito	|																																																																	
			|SIN GRUPO			|CONTACTO CORREDORES DE SEGUROS S.A.|Personalizado	|20271653939|Empresa 1	|E1234		|01/03/2019		|Jhon E Acevedo	|jeacevedo92@gmail.com|3154439086				|Analista			|Si											|jeacevedo92@gmail.com				|si			|jeacevedo92@gmail.com|avnida 123, san isidro	|Lima					|Lima			|Lima			|																											
		And Gestionar Clientes a renovar
		And Solicitar Prospecto
		|PlanVigente									|RangoPrimaProyectada	|SiniestralidadProyectadaConAjuste|CambiosEnCondiciones		|
		|PlanMultiempresa.pdf					|0 - 300							|80																|cambios en condiciones	|
		And Aprobar propuesta personalizada
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$ 	|		
		And Buscar tramite renovacion
		And Registrar Prospecto
		|numeroProspecto|
		|12345					|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*|
		And Buscar tramite renovacion
		And Cargar Planes Propuesta
		|PlanPropuesto				|FichaTecnica					|
		|PlanMultiempresa.pdf	|PlanMultiempresa.pdf	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$ 	|		
		And Buscar tramite renovacion
		And Enviar Propuesta Renovacion	
		And Ingresar respuesta propuesta
		|documento(opcional)					|
		|TRAMA_EPS_(Actualizado).xlsx	|
		And Iniciar Concurso
		|CartaInvitacion				|Comentario				|Documento(opcional)					|Broker															|PersonaContacto|CorreoContacto					|TelefonoContacto	|EjecutivoBroker|CorreoEBorker			|TelefonoEBroker|
		|Carta_de_Invitación.doc|no hay comentario|TRAMA_EPS_(Actualizado).xlsx	|CONTACTO CORREDORES DE SEGUROS S.A.|John E Acevedo	|jeacevedo92@gmail.com	|3154439086				|Jhon Acevedo		|jeacevedo@gmail.com|3154439086			|																																												
		And Enviar Propuesta Formal Renovacion
		|FechaDespacho		|Contacto								|Email										|Telefono		|
		|02/02/2019 12:15	|jhon contacto propuesta|jhonedison92@hotmail.com	|9315443908	|
		And Resultado de concurso Renovacion
		And Requisitos pre-implementación Renovacion
		|ExisteRimacSalud	|AutorizaciónDigital	|AdjuntarAutorizacion		|PolizaElectronica|RutaAjuntarPoliza						|ContactoPoliza	|CorreoContactoPoliza					|ContratoDigital|ContactoContratoDigital|CorreoContratoDigital		|TipoDocumento|DniRepLegal|NombreRepLegal						|DIRECIION																		|DEPARTAMENTO	|PROVINCIA|DISTRITO|PartidaElectronica	|RutaActaEscrutinio			|FechaVotacion|PersonaContacto					|CorreoContacto					|FechaInicioVigencia|Producto						|RutaDocumentacion						|EntidadPredecesora	|		
		|No								|Si										|Carta_de_Invitación.doc|Si								|Comunicación Electrónica.doc	|jhon poliza		|jacevedor@choucairtesting.com|Si							|Jhon Contrato Digital	|jhonedison92@hotmail.com	|DNI					|45423558		|Jhon Representante Legal	|Paseo de la República 3211, San Isidro 15047 |Lima					|Lima			|San Luis|P456664						|SolicitarCotizacion.docx	|05/01/2019		|jhon Contacto Bienvenida	|jeacevedo92@utp.edu.co	|05/01/2019					|Planes médicos EPS	|Comunicación Electrónica.doc	|EPS								|
		And Configurar Planes Renovacion
		|PlanesCreados				|
		|plan 1 plan 2 plan 3	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*|
		And Buscar tramite renovacion
		And Comunicacion de Cuenta Renovacion
		|Ajuste	|PlanesPotestativos	|Ajustes	|CambiosImportantes	|RutaPlanes				|Planes																						|
		|5%			|Si									|Ajuste 5%|cambios						|//rutacompartida	|Carta_de_Invitación.doc|
		And Migrar Afiliados
		
		
		@RPObservarAprobarPropuesta
			Scenario: Observar Aprobar Propuesta
		Given Abrir Navegador
		And Autenticar y verificar
			|Correo																				|password		|
			|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*	|
		And Ingreso opcion renovacion
		And Nuevo Tramite Renovacion
			|GrupoEconomico	|Broker															|TipoPLan				|Ruc				|RazonSocial|NroPoliza|FechaVigencia|PersonaContacto|CorreoContacto				|TelefonoContacto	|CargoContacto|ComunicacioElectronica	|CorreoComucicacionElectronica|Poliza	|CorreoPoliza					|Direccion							|Departamento	|provincia|distrito	|																																																																	
			|SIN GRUPO			|CONTACTO CORREDORES DE SEGUROS S.A.|Personalizado	|20271653939|Empresa 1	|E1234		|01/03/2019		|Jhon E Acevedo	|jeacevedo92@gmail.com|3154439086				|Analista			|Si											|jeacevedo92@gmail.com				|si			|jeacevedo92@gmail.com|avnida 123, san isidro	|Lima					|Lima			|Lima			|																											
		And Gestionar Clientes a renovar
		And Solicitar Prospecto
		|PlanVigente					|RangoPrimaProyectada	|SiniestralidadProyectadaConAjuste|CambiosEnCondiciones		|
		|PlanMultiempresa.pdf	|0 - 300							|80																|cambios en condiciones	|
		And Observar propuesta personalizada
	
	
	@RPObservarCargarPlanes
			Scenario: Observar Cargar Planes
		Given Abrir Navegador
		And Autenticar y verificar
			|Correo																				|password		|
			|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*	|
		And Ingreso opcion renovacion
		And Nuevo Tramite Renovacion
			|GrupoEconomico	|Broker															|TipoPLan				|Ruc				|RazonSocial|NroPoliza|FechaVigencia|PersonaContacto|CorreoContacto				|TelefonoContacto	|CargoContacto|ComunicacioElectronica	|CorreoComucicacionElectronica|Poliza	|CorreoPoliza					|Direccion							|Departamento	|provincia|distrito	|																																																																	
			|SIN GRUPO			|CONTACTO CORREDORES DE SEGUROS S.A.|Personalizado	|20271653939|Empresa 1	|E1234		|01/03/2019		|Jhon E Acevedo	|jeacevedo92@gmail.com|3154439086				|Analista			|Si											|jeacevedo92@gmail.com				|si			|jeacevedo92@gmail.com|avnida 123, san isidro	|Lima					|Lima			|Lima			|																											
		And Gestionar Clientes a renovar
		And Solicitar Prospecto
		|PlanVigente					|RangoPrimaProyectada	|SiniestralidadProyectadaConAjuste|CambiosEnCondiciones		|
		|PlanMultiempresa.pdf	|0 - 300							|80																|cambios en condiciones	|
		And Aprobar propuesta personalizada
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$ 	|		
		And Buscar tramite renovacion
		And Registrar Prospecto
		|numeroProspecto|
		|12345					|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*|
		And Buscar tramite renovacion
		And Observar Prospecto
		
		
		
		@RPSolicitarReajusteEnviarPropuesta
			Scenario: Solicitar reajuste en acitivdad enviar propuesta 
		Given Abrir Navegador
		And Autenticar y verificar
			|Correo																				|password		|
			|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*	|
		And Ingreso opcion renovacion
		And Nuevo Tramite Renovacion
			|GrupoEconomico	|Broker															|TipoPLan				|Ruc				|RazonSocial|NroPoliza|FechaVigencia|PersonaContacto|CorreoContacto				|TelefonoContacto	|CargoContacto|ComunicacioElectronica	|CorreoComucicacionElectronica|Poliza	|CorreoPoliza					|Direccion							|Departamento	|provincia|distrito	|																																																																	
			|SIN GRUPO			|CONTACTO CORREDORES DE SEGUROS S.A.|Personalizado	|20271653939|Empresa 1	|E1234		|01/03/2019		|Jhon E Acevedo	|jeacevedo92@gmail.com|3154439086				|Analista			|Si											|jeacevedo92@gmail.com				|si			|jeacevedo92@gmail.com|avnida 123, san isidro	|Lima					|Lima			|Lima			|																											
		And Gestionar Clientes a renovar
		And Solicitar Prospecto
		|PlanVigente					|RangoPrimaProyectada	|SiniestralidadProyectadaConAjuste|CambiosEnCondiciones		|
		|PlanMultiempresa.pdf	|0 - 300							|50																|cambios en condiciones	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$ 	|		
		And Buscar tramite renovacion
		And Registrar Prospecto
		|numeroProspecto|
		|12345					|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*|
		And Buscar tramite renovacion
		And Cargar Planes Propuesta
		|PlanPropuesto				|FichaTecnica					|
		|PlanMultiempresa.pdf	|PlanMultiempresa.pdf	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$ 	|		
		And Buscar tramite renovacion
		And Solicitar Reajuste Enviar Propuesta
		
		
		@RPEnviarPropuesta
			Scenario: Enviar Propuesta
		Given Abrir Navegador
		And Autenticar y verificar
			|Correo																				|password		|
			|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*	|
		And Ingreso opcion renovacion
		And Nuevo Tramite Renovacion
			|GrupoEconomico	|Broker															|TipoPLan				|Ruc				|RazonSocial|NroPoliza|FechaVigencia|PersonaContacto|CorreoContacto				|TelefonoContacto	|CargoContacto|ComunicacioElectronica	|CorreoComucicacionElectronica|Poliza	|CorreoPoliza					|Direccion							|Departamento	|provincia|distrito	|																																																																	
			|SIN GRUPO			|CONTACTO CORREDORES DE SEGUROS S.A.|Personalizado	|20271653939|Empresa 1	|E1234		|01/03/2019		|Jhon E Acevedo	|jeacevedo92@gmail.com|3154439086				|Analista			|Si											|jeacevedo92@gmail.com				|si			|jeacevedo92@gmail.com|avnida 123, san isidro	|Lima					|Lima			|Lima			|																											
		And Gestionar Clientes a renovar
		And Solicitar Prospecto
		|PlanVigente					|RangoPrimaProyectada	|SiniestralidadProyectadaConAjuste|CambiosEnCondiciones		|
		|PlanMultiempresa.pdf	|0 - 300							|50																|cambios en condiciones	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$ 	|		
		And Buscar tramite renovacion
		And Registrar Prospecto
		|numeroProspecto|
		|12345					|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*|
		And Buscar tramite renovacion
		And Cargar Planes Propuesta
		|PlanPropuesto				|FichaTecnica					|
		|PlanMultiempresa.pdf	|PlanMultiempresa.pdf	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$ 	|		
		And Buscar tramite renovacion
		And Enviar Propuesta Renovacion	
		And Enviar Propuesta - Ingresar Respuesta Propuesta
		
		@RPAceptarPropuestaRenovacion
			Scenario: Aceptar Propuesta Renovacion
		Given Abrir Navegador
		And Autenticar y verificar
			|Correo																				|password		|
			|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*	|
		And Ingreso opcion renovacion
		And Nuevo Tramite Renovacion
			|GrupoEconomico	|Broker															|TipoPLan				|Ruc				|RazonSocial|NroPoliza|FechaVigencia|PersonaContacto|CorreoContacto				|TelefonoContacto	|CargoContacto|ComunicacioElectronica	|CorreoComucicacionElectronica|Poliza	|CorreoPoliza					|Direccion							|Departamento	|provincia|distrito	|																																																																	
			|SIN GRUPO			|CONTACTO CORREDORES DE SEGUROS S.A.|Personalizado	|20271653939|Empresa 1	|E1234		|01/03/2019		|Jhon E Acevedo	|jeacevedo92@gmail.com|3154439086				|Analista			|Si											|jeacevedo92@gmail.com				|si			|jeacevedo92@gmail.com|avnida 123, san isidro	|Lima					|Lima			|Lima			|																											
		And Gestionar Clientes a renovar
		And Solicitar Prospecto
		|PlanVigente					|RangoPrimaProyectada	|SiniestralidadProyectadaConAjuste|CambiosEnCondiciones		|
		|PlanMultiempresa.pdf	|0 - 300							|50																|cambios en condiciones	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$ 	|		
		And Buscar tramite renovacion
		And Registrar Prospecto
		|numeroProspecto|
		|12345					|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*|
		And Buscar tramite renovacion
		And Cargar Planes Propuesta
		|PlanPropuesto				|FichaTecnica					|
		|PlanMultiempresa.pdf	|PlanMultiempresa.pdf	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$ 	|		
		And Buscar tramite renovacion
		And Enviar Propuesta Renovacion	
		And Ingresar respuesta Acepta Propuesta
		#requisitos pre - implementacion ...
		
		@RPSolicitarRespuestaConcurso
			Scenario: Solicitar respuesta en ingresar respuesta concurso
		Given Abrir Navegador
		And Autenticar y verificar
			|Correo																				|password		|
			|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*	|
		And Ingreso opcion renovacion
		And Nuevo Tramite Renovacion
			|GrupoEconomico	|Broker															|TipoPLan				|Ruc				|RazonSocial|NroPoliza|FechaVigencia|PersonaContacto|CorreoContacto				|TelefonoContacto	|CargoContacto|ComunicacioElectronica	|CorreoComucicacionElectronica|Poliza	|CorreoPoliza					|Direccion							|Departamento	|provincia|distrito	|																																																																	
			|SIN GRUPO			|CONTACTO CORREDORES DE SEGUROS S.A.|Personalizado	|20271653939|Empresa 1	|E1234		|01/03/2019		|Jhon E Acevedo	|jeacevedo92@gmail.com|3154439086				|Analista			|Si											|jeacevedo92@gmail.com				|si			|jeacevedo92@gmail.com|avnida 123, san isidro	|Lima					|Lima			|Lima			|																											
		And Gestionar Clientes a renovar
		And Solicitar Prospecto
		|PlanVigente					|RangoPrimaProyectada	|SiniestralidadProyectadaConAjuste|CambiosEnCondiciones		|
		|PlanMultiempresa.pdf |0 - 300							|50																|cambios en condiciones	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$ 	|		
		And Buscar tramite renovacion
		And Registrar Prospecto
		|numeroProspecto|
		|12345					|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*|
		And Buscar tramite renovacion
		And Cargar Planes Propuesta
		|PlanPropuesto				|FichaTecnica					|
		|PlanMultiempresa.pdf	|PlanMultiempresa.pdf	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$ 	|		
		And Buscar tramite renovacion
		And Enviar Propuesta Renovacion	
		And Ingresar respuesta propuesta
		|documento(opcional)																		|
		|TRAMA_EPS_(Actualizado).xlsx	|
		And Iniciar Concurso
		|CartaInvitacion				|Comentario				|Documento(opcional)					|Broker															|PersonaContacto|CorreoContacto					|TelefonoContacto	|EjecutivoBroker|CorreoEBorker			|TelefonoEBroker|
		|Carta_de_Invitación.doc|no hay comentario|TRAMA_EPS_(Actualizado).xlsx	|CONTACTO CORREDORES DE SEGUROS S.A.|John E Acevedo	|jeacevedo92@gmail.com	|3154439086				|Jhon Acevedo		|jeacevedo@gmail.com|3154439086			|																																												
		And Enviar Propuesta Formal Renovacion
		|FechaDespacho		|Contacto								|Email										|Telefono		|
		|02/02/2019 12:15	|jhon contacto propuesta|jhonedison92@hotmail.com	|9315443908	|
		And Solicitar Respuesta Resultado de concurso
		
	

@ejecucion
Feature: Ejecución Venta nueva Multiempresa - Renovación Personalizada  
  	
		@VentaNueva
	Scenario: Venta Nueva Empresa Existente
	Given Abrir Navegador
	And Autenticar y verificar
		|Correo																				|password	|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$|
		And Ingreso opcion nuevo tramite
		When Registro de tramite Empresa Existente
		|TipoTramite	|RUC					|
		|Individual		|20271653940	|
		And Solicitar Cotizacion
		|NumeroTrabajadores	|RangoPotAfiliados|FacturacionEmpresa	|FechaInicioVigencia|TipoPlanAnterior	|FechaRenovacion|DiagnosticoAltoCosto	|CondEspecial	|RutaArchivo							|
		|60									|Entre 25 y 100		|> 1700 UIT					|01/03/2019					|Multiempresa			|05/02/2019			|No										|No						|SolicitarCotizacion.docx	|
		And Ingresar Respuesta de Cotizacion
		|CartaInvitacion							|
		|TRAMA_EPS_(Actualizado).xlsx	|
		And Enviar Propuesta Formal
		|FechaDespacho		|Contacto								|Email										|Telefono		|
		|02/02/2019 12:15	|jhon contacto propuesta|jhonedison92@hotmail.com	|9315443908	|
		And Resultado de concurso
		And Requisitos pre-implementación
		|ExisteRimacSalud	|PolizaElectronica|RutaAjuntarPoliza						|ContactoPoliza	|CorreoContactoPoliza					|ContratoDigital|ContactoContratoDigital|CorreoContratoDigital		|TipoDocumento|DniRepLegal|NombreRepLegal						|DIRECIION																		|DEPARTAMENTO	|PROVINCIA|DISTRITO|PartidaElectronica|RutaActaEscrutinio				|FechaVotacion|PersonaContacto					|CorreoContacto					|FechaInicioVigencia|Producto						|RutaDocumentacion						|EntidadPredecesora	|		
		|No								|Si								|Comunicación Electrónica.doc	|jhon poliza		|jacevedor@choucairtesting.com|Si							|Jhon Contrato Digital	|jhonedison92@hotmail.com	|DNI					|45423558		|Jhon Representante Legal	|Paseo de la República 3211, San Isidro 15047 |Lima					|Lima			|San Luis|P456664						|SolicitarCotizacion.docx	|05/01/2019		|jhon Contacto Bienvenida	|jeacevedo92@utp.edu.co	|05/01/2019					|Planes médicos EPS	|Comunicación Electrónica.doc	|EPS								|
		And Enviar trama afiliados
		|RutaTramaDeafiliados					|				
		|TRAMA_EPS_(Actualizado).xlsx	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086* 	|		
		And Buscar tramite
		And Crea Poliza
		|NumeroPoliza	|
		|P0034654			|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|ruc				|
		|customerela@rimacsegurosperu.onmicrosoft.com	|Rlap3154$|20270653929|	
		And Buscar tramite
		And Configurar Planes
		|PlanesCreados				|Comenarios					|
		|plan 1 plan 2 plan 3	| creacion de planes|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|ruc				|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*|20270653929|
		And Buscar tramite
		And Comunicacion de Cuenta
		|FechaVigencia|ComisionPoliza	|MetodoFacturacion	|PlanesPotestativos	|Revision	|CondicionesEspeciales|RutaPlanes				|RutaArchivoAdjunto		|
		|05/01/2019		|5							|Mensual						|Si									|Semestral|No hay								|\\Rutacompartida	|PlanMultiempresa.pdf	|
		And Registrar afiliados
		|Usuario																			|Password	|
		|customerela@rimacsegurosperu.onmicrosoft.com	|Rlap3154$|
		
		
		
		@RenovacionPersonalizada
			Scenario: Renovacion Personalizada
		Given Abrir Navegador
		And Autenticar y verificar
			|Correo																				|password		|
			|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*	|
		And Ingreso opcion renovacion
		And Nuevo Tramite Renovacion
			|GrupoEconomico	|Broker															|TipoPLan				|Ruc				|RazonSocial|NroPoliza|FechaVigencia|PersonaContacto|CorreoContacto				|TelefonoContacto	|CargoContacto|ComunicacioElectronica	|CorreoComucicacionElectronica|Poliza	|CorreoPoliza					|Direccion							|Departamento	|provincia|distrito	|																																																																	
			|SIN GRUPO			|CONTACTO CORREDORES DE SEGUROS S.A.|Personalizado	|20271653941|Empresa 1	|E1234		|01/03/2019		|Jhon E Acevedo	|jeacevedo92@gmail.com|3154439086				|Analista			|Si											|jeacevedo92@gmail.com				|si			|jeacevedo92@gmail.com|avnida 123, san isidro	|Lima					|Lima			|Lima			|																											
		And Gestionar Clientes a renovar
		And Solicitar Prospecto
		|PlanVigente					|RangoPrimaProyectada	|SiniestralidadProyectadaConAjuste|CambiosEnCondiciones		|
		|PlanMultiempresa.pdf	|0 - 300							|50																|cambios en condiciones	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$ 	|		
		And Buscar tramite renovacion
		And Registrar Prospecto
		|numeroProspecto|
		|12345					|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*|
		And Buscar tramite renovacion
		And Cargar Planes Propuesta
		|PlanPropuesto				|FichaTecnica					|
		|PlanMultiempresa.pdf	|PlanMultiempresa.pdf	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$ 	|		
		And Buscar tramite renovacion
		And Enviar Propuesta Renovacion	
		And Ingresar respuesta propuesta
		|documento(opcional)					|
		|TRAMA_EPS_(Actualizado).xlsx	|
		And Iniciar Concurso
		|CartaInvitacion				|Comentario				|Documento(opcional)					|Broker															|PersonaContacto|CorreoContacto					|TelefonoContacto	|EjecutivoBroker|CorreoEBorker			|TelefonoEBroker|
		|Carta_de_Invitación.doc|no hay comentario|TRAMA_EPS_(Actualizado).xlsx	|CONTACTO CORREDORES DE SEGUROS S.A.|John E Acevedo	|jeacevedo92@gmail.com	|3154439086				|Jhon Acevedo		|jeacevedo@gmail.com|3154439086			|																																												
		And Enviar Propuesta Formal Renovacion
		|FechaDespacho		|Contacto								|Email										|Telefono		|
		|02/02/2019 12:15	|jhon contacto propuesta|jhonedison92@hotmail.com	|9315443908	|
		And Resultado de concurso Renovacion
		And Requisitos pre-implementación Renovacion
		|ExisteRimacSalud	|AutorizaciónDigital	|AdjuntarAutorizacion		|PolizaElectronica|RutaAjuntarPoliza						|ContactoPoliza	|CorreoContactoPoliza					|ContratoDigital|ContactoContratoDigital|CorreoContratoDigital		|TipoDocumento|DniRepLegal|NombreRepLegal						|DIRECIION																		|DEPARTAMENTO	|PROVINCIA|DISTRITO|PartidaElectronica|RutaActaEscrutinio				|FechaVotacion|PersonaContacto					|CorreoContacto					|FechaInicioVigencia|Producto						|RutaDocumentacion						|EntidadPredecesora	|		
		|No								|Si										|Carta_de_Invitación.doc|Si								|Comunicación Electrónica.doc	|jhon poliza		|jacevedor@choucairtesting.com|Si							|Jhon Contrato Digital	|jhonedison92@hotmail.com	|DNI					|45423558		|Jhon Representante Legal	|Paseo de la República 3211, San Isidro 15047 |Lima					|Lima			|San Luis|P456664						|SolicitarCotizacion.docx	|05/01/2019		|jhon Contacto Bienvenida	|jeacevedo92@utp.edu.co	|05/01/2019					|Planes médicos EPS	|Comunicación Electrónica.doc	|EPS								|
		And Configurar Planes Renovacion
		|PlanesCreados				|
		|plan 1 plan 2 plan 3	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*|
		And Buscar tramite renovacion
		And Comunicacion de Cuenta Renovacion
		|Ajuste	|PlanesPotestativos	|Ajustes	|CambiosImportantes	|RutaPlanes				|Planes																						|
		|5%			|Si									|Ajuste 5%|cambios						|//rutacompartida	|Carta_de_Invitación.doc|
		And Migrar Afiliados
		
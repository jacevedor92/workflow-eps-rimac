@tag
Feature: Trámite Venta Nueva - Cotización WorkFlow Eps

	@VentaNueva
	Scenario: Venta Nueva Empresa Existente
	Given Abrir Navegador
	And Autenticar y verificar
		|Correo																				|password	|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$|
		And Ingreso opcion nuevo tramite
		When Registro de tramite Empresa Existente
		|TipoTramite	|RUC					|
		|Individual		|20271653940	|
		And Solicitar Cotizacion
		|NumeroTrabajadores	|RangoPotAfiliados|FacturacionEmpresa	|FechaInicioVigencia|TipoPlanAnterior	|FechaRenovacion|DiagnosticoAltoCosto	|CondEspecial	|RutaArchivo							|
		|60									|Entre 25 y 100		|> 1700 UIT					|01/03/2019					|Multiempresa			|05/02/2019			|No										|No						|SolicitarCotizacion.docx	|
		And Ingresar Respuesta de Cotizacion
		|CartaInvitacion							|
		|TRAMA_EPS_(Actualizado).xlsx	|
		And Enviar Propuesta Formal
		|FechaDespacho		|Contacto								|Email										|Telefono		|
		|02/02/2019 12:15	|jhon contacto propuesta|jhonedison92@hotmail.com	|9315443908	|
		And Resultado de concurso
		And Requisitos pre-implementación
		|ExisteRimacSalud	|PolizaElectronica|RutaAjuntarPoliza						|ContactoPoliza	|CorreoContactoPoliza					|ContratoDigital|ContactoContratoDigital|CorreoContratoDigital		|TipoDocumento|DniRepLegal|NombreRepLegal						|DIRECIION																		|DEPARTAMENTO	|PROVINCIA|DISTRITO|PartidaElectronica|RutaActaEscrutinio				|FechaVotacion|PersonaContacto					|CorreoContacto					|FechaInicioVigencia|Producto						|RutaDocumentacion						|EntidadPredecesora	|		
		|No								|Si								|Comunicación Electrónica.doc	|jhon poliza		|jacevedor@choucairtesting.com|Si							|Jhon Contrato Digital	|jhonedison92@hotmail.com	|DNI					|45423558		|Jhon Representante Legal	|Paseo de la República 3211, San Isidro 15047 |Lima					|Lima			|San Luis|P456664						|SolicitarCotizacion.docx	|05/01/2019		|jhon Contacto Bienvenida	|jeacevedo92@utp.edu.co	|05/01/2019					|Planes médicos EPS	|Comunicación Electrónica.doc	|EPS								|
		And Enviar trama afiliados
		|RutaTramaDeafiliados					|				
		|TRAMA_EPS_(Actualizado).xlsx	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086* 	|		
		And Buscar tramite
		And Crea Poliza
		|NumeroPoliza	|
		|P0034654			|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|ruc				|
		|customerela@rimacsegurosperu.onmicrosoft.com	|Rlap3154$|20270653929|	
		And Buscar tramite
		And Configurar Planes
		|PlanesCreados				|Comenarios					|
		|plan 1 plan 2 plan 3	| creacion de planes|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|ruc				|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*|20270653929|
		And Buscar tramite
		And Comunicacion de Cuenta
		|FechaVigencia|ComisionPoliza	|MetodoFacturacion	|PlanesPotestativos	|Revision	|CondicionesEspeciales|RutaPlanes				|RutaArchivoAdjunto		|
		|05/01/2019		|5							|Mensual						|Si									|Semestral|No hay								|\\Rutacompartida	|PlanMultiempresa.pdf	|
		And Registrar afiliados
		|Usuario																			|Password	|
		|customerela@rimacsegurosperu.onmicrosoft.com	|Rlap3154$|
		
		@VentaNuevaEmpresaNueva
	Scenario: Venta Nueva empresa nueva
	Given Abrir Navegador
	And Autenticar y verificar
		|Correo																				|password	|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$|
		And Ingreso opcion nuevo tramite
		When Realizo el registro del tramite
		|TipoTramite	|RUC					|RazonSocial			|DIRECIION																		|DEPARTAMENTO	|PROVINCIA|DISTRITO|GRUPO_ECONOMICO	|BROKER															|CONTACTO							|CORREO_CONTACTO							|TELEFONO_CONTACTO|CONTACTO_BROKER										|CORREO_BROKER				|TELEFONO_BROKER|PROCEDENCIA|Eps			|	
		|Individual		|20271653453	|Innova School SAC|Paseo de la República 3211, San Isidro 15047 |Lima					|Lima			|San Luis|SIN GRUPO				|JLT PERU CORREDORES DE SEGUROS S.A.|Jhon Edinson Acevedo	|jacevedor@choucairtesting.com|3154439086				|JLT PERU CORREDORES DE SEGUROS S.A	|jeacevedo92@gmail.com|3154439086			|EPS				|Pacifico	|												
		And Solicitar Cotizacion
		|NumeroTrabajadores	|RangoPotAfiliados|FacturacionEmpresa	|FechaInicioVigencia|TipoPlanAnterior	|FechaRenovacion|DiagnosticoAltoCosto	|CondEspecial	|RutaArchivo																				|
		|60									|Entre 25 y 100		|> 1700 UIT					|01/03/2019					|Multiempresa			|05/02/2019			|No										|No						|SolicitarCotizacion.docx	|
		And Ingresar Respuesta de Cotizacion
		|CartaInvitacion					|
		|Carta_de_Invitación.doc	|
		And Enviar Propuesta Formal
		|FechaDespacho		|Contacto								|Email										|Telefono		|
		|02/02/2019 12:15	|jhon contacto propuesta|jhonedison92@hotmail.com	|9315443908	|
		And Resultado de concurso
		And Requisitos pre-implementación
		|ExisteRimacSalud	|PolizaElectronica|RutaAjuntarPoliza						|ContactoPoliza	|CorreoContactoPoliza					|ContratoDigital|ContactoContratoDigital|CorreoContratoDigital		|TipoDocumento|DniRepLegal|NombreRepLegal						|DIRECIION																		|DEPARTAMENTO	|PROVINCIA|DISTRITO|PartidaElectronica|RutaActaEscrutinio				|FechaVotacion|PersonaContacto					|CorreoContacto					|FechaInicioVigencia|Producto						|RutaDocumentacion						|EntidadPredecesora	|		
		|No								|Si								|Comunicación Electrónica.doc	|jhon poliza		|jacevedor@choucairtesting.com|Si							|Jhon Contrato Digital	|jhonedison92@hotmail.com	|DNI					|45423558		|Jhon Representante Legal	|Paseo de la República 3211, San Isidro 15047 |Lima					|Lima			|San Luis|P456664						|SolicitarCotizacion.docx	|05/01/2019		|jhon Contacto Bienvenida	|jeacevedo92@utp.edu.co	|05/01/2019					|Planes médicos EPS	|Comunicación Electrónica.doc	|EPS								|
		And Enviar trama afiliados
		|RutaTramaDeafiliados					|				
		|TRAMA_EPS_(Actualizado).xlsx	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086* 	|		
		And Buscar tramite
		And Crea Poliza
		|NumeroPoliza	|
		|P0034654			|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|ruc				|
		|customerela@rimacsegurosperu.onmicrosoft.com	|Rlap3154$|20270653929|	
		And Buscar tramite
		And Configurar Planes
		|PlanesCreados				|Comenarios					|
		|plan 1 plan 2 plan 3	| creacion de planes|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|ruc				|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*|20270653929|
		And Buscar tramite
		And Comunicacion de Cuenta
		|FechaVigencia|ComisionPoliza	|MetodoFacturacion	|PlanesPotestativos	|Revision	|CondicionesEspeciales|RutaPlanes				|RutaArchivoAdjunto		|
		|05/01/2019		|5							|Mensual						|Si									|Semestral|No hay								|\\Rutacompartida	|PlanMultiempresa.pdf	|
		And Registrar afiliados
		|Usuario																			|Password	|
		|customerela@rimacsegurosperu.onmicrosoft.com	|Rlap3154$|
		
		
	@VnDocIncompleta
	Scenario: Venta Nueva SolicitarCotizacion documentacion incompleta 
	Given Abrir Navegador
	And Autenticar y verificar
		|Correo																				|password	|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$|
		And Ingreso opcion nuevo tramite
		When Registro de tramite Empresa Existente
		|TipoTramite	|RUC					|
		|Individual		|20271653933	|
		And Solicitar Cotizacion Documentacion Incompleta
		|NumeroTrabajadores	|RangoPotAfiliados|FacturacionEmpresa	|FechaInicioVigencia|TipoPlanAnterior	|FechaRenovacion|DiagnosticoAltoCosto	|CondEspecial	|RutaArchivo							|
		|60									|Entre 25 y 100		|> 1700 UIT					|01/02/2019					|Multiempresa			|05/02/2019			|No										|No						|SolicitarCotizacion.docx	|
		
		
		@VnRespCotizacionPyme
	Scenario: Venta Nueva Ingresar respuesta de cotización - Concurso PYME
	Given Abrir Navegador
	And Autenticar y verificar
		|Correo																				|password	|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$|
		And Ingreso opcion nuevo tramite
		When Realizo el registro del tramite
		|TipoTramite	|RUC					|RazonSocial			|DIRECIION																		|DEPARTAMENTO	|PROVINCIA|DISTRITO|GRUPO_ECONOMICO	|BROKER															|CONTACTO							|CORREO_CONTACTO							|TELEFONO_CONTACTO|CONTACTO_BROKER										|CORREO_BROKER				|TELEFONO_BROKER|PROCEDENCIA|Eps			|	
		|Individual		|20271653976	|Innova School SAC|Paseo de la República 3211, San Isidro 15047 |Lima					|Lima			|San Luis|SIN GRUPO				|JLT PERU CORREDORES DE SEGUROS S.A.|Jhon Edinson Acevedo	|jacevedor@choucairtesting.com|3154439086				|JLT PERU CORREDORES DE SEGUROS S.A	|jeacevedo92@gmail.com|3154439086			|EPS				|Pacifico	|												
		And Solicitar Cotizacion
		|NumeroTrabajadores	|RangoPotAfiliados|FacturacionEmpresa	|FechaInicioVigencia|TipoPlanAnterior	|FechaRenovacion|DiagnosticoAltoCosto	|CondEspecial	|RutaArchivo							|
		|60									|Entre 25 y 100		|<= 1700 UIT				|01/02/2019					|Multiempresa			|05/02/2019			|No										|No						|SolicitarCotizacion.docx	|
		And Ingresar Respuesta de Cotizacion Pyme
		|CartaInvitacion					|
		|Carta_de_Invitación.doc	|
				
		
		@VNCotizacionRechazada
	Scenario: Venta Nueva Empresa Existente Ingresar respuesta de cotización - Rechazar cotización
	Given Abrir Navegador
	And Autenticar y verificar
		|Correo																				|password	|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$|
		And Ingreso opcion nuevo tramite
		When Registro de tramite Empresa Existente
		|TipoTramite	|RUC					|
		|Individual		|20271653933	|
		And Solicitar Cotizacion
		|NumeroTrabajadores	|RangoPotAfiliados|FacturacionEmpresa	|FechaInicioVigencia|TipoPlanAnterior	|FechaRenovacion|DiagnosticoAltoCosto	|CondEspecial	|RutaArchivo							|
		|60									|Entre 25 y 100		|> 1700 UIT					|01/02/2019					|Multiempresa			|05/02/2019			|No										|No						|SolicitarCotizacion.docx	|
		And Rechazar Cotizacion
		|CartaMotivoRechazo				|Comentario					|
		|Carta_de_Invitación.doc	|se cancelo esto XD |
		
		
		@VnConcursoNoGanado
	Scenario: Venta Nueva Empresa Existente Concurso no ganado
	Given Abrir Navegador
	And Autenticar y verificar
		|Correo																				|password	|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$|
		And Ingreso opcion nuevo tramite
		When Registro de tramite Empresa Existente
		|TipoTramite	|RUC					|
		|Individual		|20271653933	|
		And Solicitar Cotizacion
		|NumeroTrabajadores	|RangoPotAfiliados|FacturacionEmpresa	|FechaInicioVigencia|TipoPlanAnterior	|FechaRenovacion|DiagnosticoAltoCosto	|CondEspecial	|RutaArchivo							|
		|60									|Entre 25 y 100		|> 1700 UIT					|01/02/2019					|Multiempresa			|05/02/2019			|No										|No						|SolicitarCotizacion.docx	|
		And Ingresar Respuesta de Cotizacion
		|CartaInvitacion							|
		|TRAMA_EPS_(Actualizado).xlsx	|
		And Enviar Propuesta Formal
		|FechaDespacho		|Contacto								|Email										|Telefono		|
		|02/02/2019 12:15	|jhon contacto propuesta|jhonedison92@hotmail.com	|9315443908	|
		And Concurso No Ganado
		|Ruta									|Comentario					|
		|ActadeEscrutinio.doc	|Concurso no ganado	|
		
	@VnSolicitarRespuesta
	Scenario: Venta Nueva Empresa Existente Solicitar Respuesta
	Given Abrir Navegador
	And Autenticar y verificar
		|Correo																				|password	|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$|
		And Ingreso opcion nuevo tramite
		When Registro de tramite Empresa Existente
		|TipoTramite	|RUC					|
		|Individual		|20271653934	|
		And Solicitar Cotizacion
		|NumeroTrabajadores	|RangoPotAfiliados|FacturacionEmpresa	|FechaInicioVigencia|TipoPlanAnterior	|FechaRenovacion|DiagnosticoAltoCosto	|CondEspecial	|RutaArchivo																				|
		|60									|Entre 25 y 100		|> 1700 UIT					|01/02/2019					|Multiempresa			|05/02/2019			|No										|No						|SolicitarCotizacion.docx	|
		And Ingresar Respuesta de Cotizacion
		|CartaInvitacion							|
		|TRAMA_EPS_(Actualizado).xlsx	|
		And Enviar Propuesta Formal
		|FechaDespacho		|Contacto								|Email										|Telefono		|
		|02/02/2019 12:15	|jhon contacto propuesta|jhonedison92@hotmail.com	|9315443908	|
		And Solicitar Respuesta Concurso
		
		
		@VnDevolverTramite
	Scenario: Venta Nueva Enviar planes configurados - Devolver trámite
	Given Abrir Navegador
	And Autenticar y verificar
		|Correo																				|password	|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$|
		And Ingreso opcion nuevo tramite
		When Registro de tramite Empresa Existente
		|TipoTramite	|RUC					|
		|Individual		|20271653934	|
		And Solicitar Cotizacion
		|NumeroTrabajadores	|RangoPotAfiliados|FacturacionEmpresa	|FechaInicioVigencia|TipoPlanAnterior	|FechaRenovacion|DiagnosticoAltoCosto	|CondEspecial	|RutaArchivo							|
		|60									|Entre 25 y 100		|> 1700 UIT					|01/02/2019					|Multiempresa			|05/02/2019			|No										|No						|SolicitarCotizacion.docx	|
		And Ingresar Respuesta de Cotizacion
		|CartaInvitacion							|
		|TRAMA_EPS_(Actualizado).xlsx	|
		And Enviar Propuesta Formal
		|FechaDespacho		|Contacto								|Email										|Telefono		|
		|02/02/2019 12:15	|jhon contacto propuesta|jhonedison92@hotmail.com	|9315443908	|
		And Resultado de concurso
		And Requisitos pre-implementación
		|ExisteRimacSalud	|PolizaElectronica|RutaAjuntarPoliza						|ContactoPoliza	|CorreoContactoPoliza					|ContratoDigital|ContactoContratoDigital|CorreoContratoDigital		|TipoDocumento|DniRepLegal|NombreRepLegal						|DIRECIION																		|DEPARTAMENTO	|PROVINCIA|DISTRITO|PartidaElectronica|RutaActaEscrutinio				|FechaVotacion|PersonaContacto					|CorreoContacto					|FechaInicioVigencia|Producto						|RutaDocumentacion						|EntidadPredecesora	|		
		|No								|Si								|Comunicación Electrónica.doc	|jhon poliza		|jacevedor@choucairtesting.com|Si							|Jhon Contrato Digital	|jhonedison92@hotmail.com	|DNI					|45423558		|Jhon Representante Legal	|Paseo de la República 3211, San Isidro 15047 |Lima					|Lima			|San Luis|P456664						|SolicitarCotizacion.docx	|05/01/2019		|jhon Contacto Bienvenida	|jeacevedo92@utp.edu.co	|05/01/2019					|Planes médicos EPS	|Comunicación Electrónica.doc	|EPS								|
		And Enviar trama afiliados
		|RutaTramaDeafiliados					|				
		|TRAMA_EPS_(Actualizado).xlsx	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086* 	|		
		And Buscar tramite
		And Crea Poliza
		|NumeroPoliza	|
		|P0034654			|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|ruc				|
		|customerela@rimacsegurosperu.onmicrosoft.com	|Rlap3154$|20270653929|	
		And Buscar tramite
		And Planes Configurados Devolver tramite
		|motivoDevolver		|AdjuntarArchivo							|
		|Pues me devuelvo	|TRAMA_EPS_(Actualizado).xlsx	|
		
		@VnAdjudicacionDirecta
		Scenario: Venta Nueva Ingresar respuesta de cotización - Concurso PYME
	Given Abrir Navegador
	And Autenticar y verificar
		|Correo																				|password	|
		|customeradm@rimacsegurosperu.onmicrosoft.com	|Rul42988$|
		And Ingreso opcion nuevo tramite
		When Realizo el registro del tramite
		|TipoTramite	|RUC					|RazonSocial			|DIRECIION																		|DEPARTAMENTO	|PROVINCIA|DISTRITO|GRUPO_ECONOMICO	|BROKER															|CONTACTO							|CORREO_CONTACTO							|TELEFONO_CONTACTO|CONTACTO_BROKER										|CORREO_BROKER				|TELEFONO_BROKER|PROCEDENCIA|Eps			|	
		|Individual		|20271653981	|Innova School SAC|Paseo de la República 3211, San Isidro 15047 |Lima					|Lima			|San Luis|SIN GRUPO				|JLT PERU CORREDORES DE SEGUROS S.A.|Jhon Edinson Acevedo	|jacevedor@choucairtesting.com|3154439086				|JLT PERU CORREDORES DE SEGUROS S.A	|jeacevedo92@gmail.com|3154439086			|EPS				|Pacifico	|												
		And Solicitar Cotizacion
		|NumeroTrabajadores	|RangoPotAfiliados|FacturacionEmpresa	|FechaInicioVigencia|TipoPlanAnterior	|FechaRenovacion|DiagnosticoAltoCosto	|CondEspecial	|RutaArchivo							|
		|60									|Entre 25 y 100		|<= 1700 UIT				|01/03/2019					|Multiempresa			|05/02/2019			|No										|No						|SolicitarCotizacion.docx	|
		And Ingresar Respuesta de Cotizacion Pyme
		|CartaInvitacion					|
		|Carta_de_Invitación.doc	|
		And Requisitos pre-implementación
		|ExisteRimacSalud	|PolizaElectronica|RutaAjuntarPoliza						|ContactoPoliza	|CorreoContactoPoliza					|ContratoDigital|ContactoContratoDigital|CorreoContratoDigital		|TipoDocumento|DniRepLegal|NombreRepLegal						|DIRECIION																		|DEPARTAMENTO	|PROVINCIA|DISTRITO|PartidaElectronica|RutaActaEscrutinio				|FechaVotacion|PersonaContacto					|CorreoContacto					|FechaInicioVigencia|Producto						|RutaDocumentacion						|EntidadPredecesora	|		
		|No								|Si								|Comunicación Electrónica.doc	|jhon poliza		|jacevedor@choucairtesting.com|Si							|Jhon Contrato Digital	|jhonedison92@hotmail.com	|DNI					|45423558		|Jhon Representante Legal	|Paseo de la República 3211, San Isidro 15047 |Lima					|Lima			|San Luis|P456664						|SolicitarCotizacion.docx	|05/01/2019		|jhon Contacto Bienvenida	|jeacevedo92@utp.edu.co	|05/01/2019					|Planes médicos EPS	|Comunicación Electrónica.doc	|EPS								|
		And Enviar trama afiliados
		|RutaTramaDeafiliados					|				
		|TRAMA_EPS_(Actualizado).xlsx	|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password		|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086* 	|		
		And Buscar tramite
		And Crea Poliza
		|NumeroPoliza	|
		|P0034654			|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|ruc				|
		|customerela@rimacsegurosperu.onmicrosoft.com	|Rlap3154$|20270653929|	
		And Buscar tramite
		And Configurar Planes
		|PlanesCreados				|Comenarios					|
		|plan 1 plan 2 plan 3	| creacion de planes|
		And Cerrar sesion
		And Autenticar y verificar
		|Correo																				|password	|ruc				|
		|customerapr@rimacsegurosperu.onmicrosoft.com	|Rimp9086*|20270653929|
		And Buscar tramite
		And Comunicacion de Cuenta
		|FechaVigencia|ComisionPoliza	|MetodoFacturacion	|PlanesPotestativos	|Revision	|CondicionesEspeciales|RutaPlanes				|RutaArchivoAdjunto															|
		|05/01/2019		|5							|Mensual						|Si									|Semestral|No hay								|\\Rutacompartida	|PlanMultiempresa.pdf	|
		And Registrar afiliados
		|Usuario																			|Password	|
		|customerela@rimacsegurosperu.onmicrosoft.com	|Rlap3154$|
		
	
	
	
		
		